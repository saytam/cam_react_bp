const atomic = ['Button', 'Typography'];

/** @type {import('eslint').Linter.Config} */
module.exports = {
  extends: ['react-app', 'prettier', 'plugin:@typescript-eslint/recommended'],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'prettier', 'unicorn'],
  settings: {
    'import/parsers': { '@typescript-eslint/parser': ['.ts', '.tsx'] },
    'import/resolver': { typescript: {} },
    'import/internal-regex': '^@cam/',
  },
  rules: {
    'no-duplicate-imports': 'error',
    'no-unused-vars': 'off',
    'no-restricted-imports': [
      'error',
      {
        paths: [
          {
            name: 'antd',
            importNames: atomic,
            message: 'Use @cam/atomic',
          },
        ],
        patterns: [
          {
            group: ['antd/lib', 'antd/lib/*'],
            message: 'Use antd/es',
          },
        ],
      },
    ],
    'react/jsx-filename-extension': ['warn', { extensions: ['.js', '.jsx', '.ts', '.tsx'] }],
    'react/jsx-curly-brace-presence': 'warn',
    'object-shorthand': 'warn',
    'import/no-anonymous-default-export': 'off',
    'import/no-extraneous-dependencies': 'off',
    'import/no-default-export': 'off',
    'import/no-namespace': 'warn',
    'import/extensions': [
      'error',
      'ignorePackages',
      { js: 'never', jsx: 'never', ts: 'never', tsx: 'never', mjs: 'never' },
    ],
    'import/order': [
      'error',
      {
        groups: ['builtin', 'external', 'internal'],
        alphabetize: { order: 'asc', caseInsensitive: true },
        'newlines-between': 'always',
      },
    ],
    'jsx-a11y/href-no-hash': 'off',
    'jsx-a11y/click-events-have-key-events': 'off',
    'jsx-a11y/no-noninteractive-element-interactions': 'off',
    '@typescript-eslint/ban-types': 'warn',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/no-namespace': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    '@typescript-eslint/no-unused-expressions': 'off',
    '@typescript-eslint/no-redeclare': 'off',
    '@typescript-eslint/no-shadow': 'warn',
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-unused-vars': [
      'warn',
      { vars: 'all', args: 'after-used', ignoreRestSiblings: false },
    ],
    'unicorn/explicit-length-check': 'warn',
    'unicorn/no-useless-length-check': 'warn',
    'unicorn/no-useless-spread': 'warn',
    'unicorn/no-useless-undefined': 'warn',
    'unicorn/prefer-array-find': 'warn',
    'unicorn/prefer-array-flat': 'warn',
    'unicorn/prefer-array-index-of': 'warn',
    'unicorn/prefer-array-some': 'warn',
    'unicorn/prefer-date-now': 'warn',
    'unicorn/prefer-includes': 'warn',
    'unicorn/prefer-negative-index': 'warn',
    'unicorn/prefer-number-properties': 'warn',
    'unicorn/prefer-string-starts-ends-with': 'warn',
    'unicorn/require-array-join-separator': 'warn',
  },
  overrides: [
    {
      files: 'packages/atomic/**',
      rules: {
        'no-restricted-imports': ['error', { patterns: ['antd/lib', 'antd/lib/*'] }],
      },
    },
    {
      files: ['*.tsx'],
      rules: {
        '@typescript-eslint/no-unnecessary-type-constraint': 'off', // enable TS generics in JSX
      },
    },
  ],
};
