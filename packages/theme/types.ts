import { getBaseTheme, createColors } from '@cam/theme/helpers';

export type Theme = ReturnType<typeof getBaseTheme> & {
  colors: ReturnType<typeof createColors> & { charts: Charts };
  buttonRadius: number;
  fontFamily: string;
};

export type BasePalette = {
  blue: string;
  green: string;
  red: string;
  orange: string;
  gold: string;
};

export type Charts = {
  status: Record<StatusColors, string>;
};

export type BackgroundVariant = 'bg' | 'surface';
export type FontSize = 'sm' | 'base' | 'md' | 'lg' | 'xl' | 'xxl';
export type StatusColors = 'success' | 'warning' | 'error' | 'neutral';
export type Colors =
  | 'default'
  | 'success'
  | 'warning'
  | 'error'
  | 'primary'
  | 'white'
  | 'black'
  | 'info'
  | 'neutral'
  | 'secondary';
