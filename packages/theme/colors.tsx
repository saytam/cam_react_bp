import { DefaultTheme, useTheme } from 'styled-components';

import { Colors } from '@cam/theme/types';

export const getThemeColors = (theme: DefaultTheme): Record<Colors, string | undefined> => ({
  black: theme.colors.black,
  default: undefined,
  error: theme.colors.error,
  info: theme.colors.info,
  neutral: theme.colors.neutral.light.disabled,
  primary: theme.colors.primary,
  secondary: theme.colors.neutral.light.secondary,
  success: theme.colors.success,
  warning: theme.colors.warning,
  white: theme.colors.white,
});

export const useThemeColor = (variant: Colors) => {
  const theme = useTheme();
  const colorMap = getThemeColors(theme);
  return colorMap[variant];
};
