import { Radio as AntdRadio } from 'antd';

import { CardRadioButton } from './CardRadioButton';
import { RadioWithIcon } from './RadioWithIcon';
import { VerticalRadio } from './VerticalRadio';

export const Radio = Object.assign({}, AntdRadio, {
  Vertical: VerticalRadio,
  Icon: RadioWithIcon,
  Card: CardRadioButton,
});
