import { Row, Radio } from 'antd';
import { RadioButtonProps } from 'antd/es/radio/radioButton';
import styled from 'styled-components';

const Button = styled(Radio.Button)`
  &&& {
    width: 100%;
    padding: 8px 16px 8px 40px;
    height: auto;

    .ant-radio-button {
      top: auto;
      bottom: auto;
      left: 16px;
      z-index: 1;
    }

    .ant-radio-button-input {
      opacity: 1;
      width: auto;
      height: auto;
    }
  }
`;

const Label = styled.span`
  font-size: ${props => props.theme.fontSizes.base}px;
  font-weight: 500;
`;

export const CardRadioButton = <T extends unknown>({
  value,
  label,
  children,
  ...rest
}: RadioButtonProps & {
  value: T;
  label: React.ReactNode;
  children?: React.ReactNode;
}) => {
  return (
    <Button value={value} {...rest}>
      <Row justify="space-between" wrap>
        <Label>{label}</Label>
        {children}
      </Row>
    </Button>
  );
};
