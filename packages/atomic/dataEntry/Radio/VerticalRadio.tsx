import { Radio } from 'antd';
import styled from 'styled-components';

export const VerticalRadio = styled(Radio)`
  && {
    display: flex;
    align-items: flex-start;
    min-height: 30px;

    span:not([class]) {
      padding-right: 0;
    }
  }
`;
