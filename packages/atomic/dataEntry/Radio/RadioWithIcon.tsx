import { Radio, Space } from 'antd';
import styled from 'styled-components';

const RadioWithIconWrapper = styled(Radio.Button)`
  &.ant-radio-button-wrapper {
    && {
      width: 92px;
      min-height: 80px;
      padding: 14px;
      border-radius: 8px;
      margin-right: 24px;
      border-left-width: 1px;
    }
    && {
      :not(:first-child)::before {
        width: 0;
        background-color: transparent;
        padding: 0;
      }
    }
  }
`;

export const RadioWithIcon: React.FC<{
  icon: React.ReactNode;
  title: string;
  value: string | unknown;
  label?: string;
}> = ({ icon, title, value, label, ...rest }) => {
  return (
    <RadioWithIconWrapper value={value} aria-label={label || title} {...rest}>
      <Space align="center" direction="vertical" size={2}>
        {icon}
        <div style={{ marginTop: '-10px' }}>{title}</div>
      </Space>
    </RadioWithIconWrapper>
  );
};
