import generatePicker from 'antd/es/date-picker/generatePicker';
import dayjsGenerateConfig from 'rc-picker/es/generate/dayjs';
import 'antd/es/date-picker/style/index';

export const DatePicker = generatePicker(dayjsGenerateConfig);
