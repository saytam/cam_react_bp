/* eslint-disable @typescript-eslint/ban-types */
import { Table as AntdTable, TableProps, TablePaginationConfig } from 'antd';
import { ColumnsType } from 'antd/es/table/interface';
import { memo, AriaAttributes } from 'react';
import { useTranslation, TFunction } from 'react-i18next';
import styled from 'styled-components';

import { TypedColumnType } from '@cam/app/src/utils/antd';
import { defaults as paginationDefaults } from '@cam/redux/table/pagination';

const HeaderCell = styled.th.attrs({ scope: 'col' })``;

const addDataTestIdsToTableColumns = <T extends unknown>(
  columns?: ColumnsType<T>
): ColumnsType<T> | undefined =>
  (columns as TypedColumnType<T>[])?.map(column => ({
    ...column,
    onCell: (...args) =>
      ({
        'data-test-id': `table-cell-${column.dataIndex}`,
        'data-key': column.dataIndex,
        ...column.onCell?.(...args),
      } as any), // eslint-disable-line @typescript-eslint/no-explicit-any
    onHeaderCell: (...args) =>
      ({
        'data-test-id': `table-header-${column.dataIndex}`,
        'data-key': column.dataIndex,
        ...column.onHeaderCell?.(...args),
      } as any), // eslint-disable-line @typescript-eslint/no-explicit-any
  }));

const createPaginationConfig = (
  t: TFunction,
  pagination?: TablePaginationConfig
): TablePaginationConfig => {
  const paginationConfig: TablePaginationConfig & AriaAttributes = {
    defaultCurrent: paginationDefaults.page,
    defaultPageSize: paginationDefaults.size,
    pageSizeOptions: paginationDefaults.options.map(size => size.toString()),
    size: 'small',
    position: ['bottomLeft'],
    showQuickJumper: true,
    showSizeChanger: true,
    showTotal: total => t('common:a11y.table.pagination.total', { count: total }),
    role: 'navigation',
  };

  return { ...paginationConfig, ...pagination };
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getDefaultAriaLabel = (item: any) =>
  item.name || item.label || item.title || item.value || item.url;

const TableBase = <RecordType extends object>(
  props: TableProps<RecordType> & {
    'data-test-id'?: string;
    height?: number;
    rowLabel?: (item: RecordType) => string;
  }
) => {
  const { t } = useTranslation();
  const { pagination, components, onRow, rowLabel, columns, ...rest } = props;

  return (
    <AntdTable
      tableLayout="auto"
      scroll={{ x: 'max-content', y: props.height || undefined }}
      showSorterTooltip={props.showSorterTooltip || false}
      pagination={pagination !== false && createPaginationConfig(t, pagination)}
      onRow={item => ({
        ...onRow,
        role: 'row',
        'aria-label': rowLabel?.(item) || getDefaultAriaLabel(item),
      })}
      components={{
        ...components,
        header: {
          cell: HeaderCell,
          ...components?.header,
        },
      }}
      columns={addDataTestIdsToTableColumns(columns)}
      {...rest}
    />
  );
};

export const Table = memo(TableBase) as typeof TableBase;
