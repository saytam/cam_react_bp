import { Tag as AntdTag, TagProps as AntdTagProps } from 'antd';
import { tint } from 'polished';
import { forwardRef, ForwardedRef } from 'react';
import styled, { css } from 'styled-components';

import { useThemeColor } from '@cam/theme/colors';
import { Colors } from '@cam/theme/types';

const StyledTag = styled(AntdTag)<{ $ghost: boolean; color?: string }>`
  ${({ $ghost, color, theme }) =>
    $ghost &&
    color &&
    css`
      && {
        border: 1px solid;
        background-color: ${tint(0.94, color)} !important;
        border-color: ${tint(0.1, color)};
        color: ${theme.colors.neutral.light.primary};
        &:hover {
          background-color: ${tint(0.95, color)} !important;
          border-color: ${tint(0, color)};
        }
      }
    `}
`;

export type TagProps =
  | AntdTagProps &
      ({ variant?: Colors; ghost?: boolean; color?: never } | { variant?: never; ghost?: never });
export const Tag = forwardRef(
  ({ variant = 'default', ghost = false, ...rest }: TagProps, ref: ForwardedRef<HTMLElement>) => {
    const color = useThemeColor(variant);
    return <StyledTag $ghost={ghost} color={color} {...ref} {...rest} />;
  }
);
