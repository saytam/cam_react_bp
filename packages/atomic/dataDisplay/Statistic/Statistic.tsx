import { Statistic as AntdStatistic } from 'antd';
import { ConfigContext } from 'antd/es/config-provider';
import { StatisticProps } from 'antd/es/statistic/Statistic';
import { useContext, useMemo, memo } from 'react';
import styled, { css } from 'styled-components';

type StatisticVariant = 'default' | 'snug';
const StyledStatistic = styled(AntdStatistic)<{ variant?: StatisticVariant }>`
  ${props =>
    props.variant === 'snug' &&
    css`
      && {
        line-height: 1;
      }
    `}
`;

const getSeparators = (locale: string) => {
  const numberWithGroupAndDecimalSeparator = 1000.1;
  const defaults = { group: ',', decimal: '.' };
  try {
    const separators = Intl.NumberFormat(locale).formatToParts(numberWithGroupAndDecimalSeparator);
    return {
      group: separators.find(part => part.type === 'group')?.value || defaults.group,
      decimal: separators.find(part => part.type === 'decimal')?.value || defaults.decimal,
    };
  } catch {
    return defaults;
  }
};

const useSeparators = () => {
  const { locale: antdLocale } = useContext(ConfigContext);
  const locale = antdLocale?.locale || 'en';
  return useMemo(() => getSeparators(locale), [locale]);
};

const StatisticBase: React.FC<
  StatisticProps & {
    variant?: StatisticVariant;
  }
> = props => {
  const separators = useSeparators();

  const content = (
    <StyledStatistic
      groupSeparator={separators.group}
      decimalSeparator={separators.decimal}
      {...props}
    />
  );

  return content;
};

export const Statistic = Object.assign(memo(StatisticBase), {
  Countdown: AntdStatistic.Countdown,
});
