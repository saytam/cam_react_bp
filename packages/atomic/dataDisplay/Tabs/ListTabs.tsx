import { Tabs as AntdTabs, Row, Tooltip } from 'antd';
import { TabsProps } from 'antd/es/tabs';
import { Suspense } from 'react';
import { Disable } from 'react-disable';

import { Loader, TabItem } from '@cam/atomic';

type PaneProps = {
  renderContent?: (content: TabItem['content']) => React.ReactNode;
};

const renderTabPane = ({
  key,
  icon,
  title,
  titleTooltip,
  content,
  enabled = true,
  onClick,
  renderContent = item => item,
  extra,
}: TabItem & PaneProps) => (
  <AntdTabs.TabPane
    key={key}
    tab={
      <Tooltip title={titleTooltip}>
        <Row align="middle" wrap={false} onClick={onClick}>
          {icon}
          <span>{title}</span>
          <Disable disabled={!enabled} disabledOpacity={0.5}>
            {extra}
          </Disable>
        </Row>
      </Tooltip>
    }
    disabled={!enabled}
  >
    <Suspense fallback={<Loader />}>{renderContent(content)}</Suspense>
  </AntdTabs.TabPane>
);

export const ListTabs = <T extends string = string>({
  tabs,
  ...props
}: Omit<TabsProps, 'activeKey' | 'defaultActiveKey' | 'onChange'> &
  PaneProps & {
    tabs: TabItem<T>[];
    activeKey?: T;
    defaultActiveKey?: T;
    onChange?: (activeKey: T) => void;
  }) => (
  <AntdTabs tabBarStyle={{ marginBottom: 0 }} {...(props as TabsProps)}>
    {tabs.map(renderTabPane)}
  </AntdTabs>
);
