import { Tabs as AntdTabs } from 'antd';

import { ListTabs } from '@cam/atomic/dataDisplay/Tabs/ListTabs';

export interface TabItem<T extends string | number = string> {
  icon?: React.ReactNode;
  title: string;
  extra?: React.ReactNode;
  key: T;
  content: React.ReactNode;
  testId?: string;
  enabled?: boolean;
  titleTooltip?: string;
  onClick?: () => void;
}

export const Tabs = Object.assign(AntdTabs, { List: ListTabs });
