import { Row } from 'antd';

import { Spin } from '@cam/atomic';

export const Loader: React.FC = () => (
  <Row justify="center" style={{ paddingTop: 128 }}>
    <Spin />
  </Row>
);
