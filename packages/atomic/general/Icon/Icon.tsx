import { forwardRef } from 'react';
import styled from 'styled-components';

import { useThemeColor } from '@cam/theme/colors';
import { Colors } from '@cam/theme/types';

export type IconWrapperProps = {
  color?: string;
  left?: number;
  right?: number;
  size?: number;
  onClick?: React.MouseEventHandler<HTMLSpanElement>;
};

export type IconProps = IconWrapperProps & {
  color?: Colors;
  icon: React.FunctionComponent;
  className?: string;
};

const IconWrapper = styled.span<IconWrapperProps>`
  .anticon,
  &.anticon {
    && {
      color: ${props => props.color};
      font-size: ${props => typeof props.size !== 'undefined' && props.size + 'px'};
      height: 1em;
      line-height: 1em;
      padding-left: ${props => props.left || 0}px;
      padding-right: ${props => props.right || 0}px;
    }
  }
`;

export const Icon: React.FC<IconProps> = forwardRef(
  (
    { icon: IconComponent, className = '', onClick, color: colorName, ...rest },
    ref: React.Ref<HTMLElement>
  ) => {
    const color = useThemeColor(colorName || 'default');
    return (
      <IconWrapper
        className={'anticon ' + className}
        ref={ref}
        onClick={onClick}
        children={<IconComponent {...rest} />}
        color={color}
        {...rest}
      />
    );
  }
);
