import { Dropdown as AntdDropdown } from 'antd';
import { DropDownProps } from 'antd/es/dropdown';

import { DropdownButton } from './DropdownButton';

const DropdownBase: React.FC<DropDownProps> = ({ children, ...props }) => {
  return typeof children === 'string' ? (
    <AntdDropdown {...props}>
      <span>{children}</span>
    </AntdDropdown>
  ) : (
    <AntdDropdown {...props}>{children}</AntdDropdown>
  );
};

export const Dropdown = Object.assign(DropdownBase, { Button: DropdownButton });
