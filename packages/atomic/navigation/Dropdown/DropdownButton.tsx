import { DownOutlined } from '@ant-design/icons';
import { Dropdown } from 'antd';
import { DropdownButtonProps } from 'antd/es/dropdown/dropdown-button';
import { cloneElement, HTMLAttributes, isValidElement } from 'react';
import styled, { css } from 'styled-components';

export const DropdownButton = styled(Dropdown.Button).attrs<{
  $triggerButtonProps?: HTMLAttributes<HTMLButtonElement>;
}>(({ $triggerButtonProps, icon }) => ({
  icon: icon || <DownOutlined />,
  ...($triggerButtonProps && {
    buttonsRender: enhanceDropdownButton({
      ...$triggerButtonProps,
      onClick: e => ($triggerButtonProps['aria-disabled'] ? e.preventDefault() : undefined),
    }),
  }),
}))<{
  $block?: boolean;
  $triggerButtonProps?: HTMLAttributes<HTMLButtonElement>;
}>`
  ${props =>
    props.$block &&
    css`
      .ant-btn {
        &:first-of-type {
          width: 100%;
        }
      }
    `}

  ${props =>
    props.$block &&
    css`
      width: 100%;
    `}

    ${props =>
    props.$triggerButtonProps?.['aria-disabled'] &&
    css`
      .ant-dropdown-trigger {
        cursor: not-allowed;
        &,
        &:hover,
        &:focus {
          color: ${props.theme.colors.neutral.light.disabled};
          border-color: ${props.theme.colors.neutral.light.border};
          background-color: ${props.theme.colors.neutral.light.surface};
        }
      }
    `}
`;

const enhanceDropdownButton =
  (triggerButtonProps: HTMLAttributes<HTMLButtonElement>): DropdownButtonProps['buttonsRender'] =>
  ([leftButton, rightButton]) =>
    [
      leftButton,
      isValidElement(rightButton)
        ? cloneElement(rightButton as React.ReactElement, triggerButtonProps)
        : rightButton,
    ];
