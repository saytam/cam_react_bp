import { Space } from 'antd';
import { CardSize } from 'antd/es/card';

import { Icon, Card } from '@cam/atomic';

export const Widget: React.FC<{
  title: React.ReactNode;
  icon?: React.FunctionComponent;
  size?: CardSize;
  extra?: React.ReactNode;
}> = ({ size = 'small', icon, title, extra, ...rest }) => (
  <Card
    size={size}
    headStyle={{
      background: 'transparent',
      fontWeight: 400,
      border: 'none',
    }}
    style={{ width: '100%' }}
    title={
      <Space>
        {icon && <Icon icon={icon} size={42} color="neutral" />}
        {title}
      </Space>
    }
    extra={<div>{extra}</div>}
    role="group"
    {...rest}
  />
);
