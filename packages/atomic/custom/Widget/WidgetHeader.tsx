import { StatisticProps } from 'antd/es/statistic';
import styled from 'styled-components';

import { Statistic } from '@cam/atomic';

const StatisticTitle = styled.h2`
  && {
    color: ${props => props.theme.colors.neutral.light.secondary};
    font-size: ${props => props.theme.fontSizes.sm}px;
    font-weight: 400;
    margin-bottom: 4px;
  }
`;

type WidgetHeaderProps = Pick<StatisticProps, 'value' | 'valueRender'> & {
  title: string;
};

export const WidgetHeader: React.FC<WidgetHeaderProps> = ({ title, value, valueRender }) => (
  <Statistic
    variant="snug"
    title={<StatisticTitle>{title}</StatisticTitle>}
    value={value}
    valueRender={valueRender}
  />
);
