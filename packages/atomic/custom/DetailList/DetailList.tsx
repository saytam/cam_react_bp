import { Col, Row, Space } from 'antd';

import { Text, HelpText } from '@cam/atomic';
import { DetailItemProps } from '@cam/atomic/custom/DetailList/types';

export const DetailList: React.FC<{
  items: DetailItemProps[];
}> = ({ items }) => {
  return (
    <Space direction="vertical" size="large">
      {items.map(({ label, content, helpText }, index) => (
        <Row key={index}>
          <Col xs={24} sm={8}>
            <HelpText {...helpText}>
              <Text strong>{label}</Text>
            </HelpText>
          </Col>
          <Col xs={24} sm={14}>
            {content}
          </Col>
        </Row>
      ))}
    </Space>
  );
};
