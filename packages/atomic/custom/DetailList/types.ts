import { HelpTextOptions } from '@cam/atomic/custom/HelpText/HelpText';

export interface DetailItemProps {
  label: string;
  content: React.ReactNode;
  dataTestId?: string;
  helpText?: HelpTextOptions;
}
