import { QuestionCircleOutlined, InfoCircleOutlined } from '@ant-design/icons';
import { Tooltip, Popover, TooltipProps, PopoverProps } from 'antd';
import styled from 'styled-components';

import { Icon } from '@cam/atomic';

const Wrapper = styled.span`
  display: inline-flex;
  align-items: center;
`;

const HelpIcon = styled(Icon).attrs<{ $primary?: boolean }>(props => ({
  left: props.$primary ? 8 : 4,
  color: props.$primary ? 'primary' : 'neutral',
}))<{ $primary?: boolean }>`
  cursor: help;
`;

export type HelpTextOptions = {
  help?: React.ReactNode;
  isInfo?: boolean;
  primary?: boolean;
  width?: number;
  popover?: boolean;
} & Partial<PopoverProps | TooltipProps>;

export const HelpText: React.FC<HelpTextOptions> = ({
  help,
  isInfo,
  primary,
  width,
  popover,
  children,
  ...props
}) => {
  const content = (
    <HelpIcon icon={isInfo ? InfoCircleOutlined : QuestionCircleOutlined} $primary={primary} />
  );

  return (
    <Wrapper>
      {children}
      {help &&
        (popover ? (
          <Popover content={help} overlayInnerStyle={{ width }} {...props}>
            {content}
          </Popover>
        ) : (
          <Tooltip title={help} overlayInnerStyle={{ width }} {...props}>
            {content}
          </Tooltip>
        ))}
    </Wrapper>
  );
};
