import {
  CheckCircleOutlined,
  ExclamationCircleOutlined,
  InfoCircleOutlined,
  WarningOutlined,
} from '@ant-design/icons';
import { Space, SpaceProps } from 'antd';

import { Icon } from '@cam/atomic';

export type StatusTextType = 'info' | 'warning' | 'error' | 'success';
type SizeType = 'small' | 'middle' | 'large';
interface IconPreset {
  icon: typeof InfoCircleOutlined;
  color: StatusTextType;
}

const sizes: Record<SizeType, number> = {
  small: 16,
  middle: 20,
  large: 24,
};

const iconPresets: Record<StatusTextType, IconPreset> = {
  error: { icon: WarningOutlined, color: 'error' },
  info: { icon: InfoCircleOutlined, color: 'info' },
  success: { icon: CheckCircleOutlined, color: 'success' },
  warning: { icon: ExclamationCircleOutlined, color: 'warning' },
};

export const StatusText: React.FC<
  Omit<SpaceProps, 'size'> & {
    type: StatusTextType;
    text?: string;
    size?: SizeType;
    customIcon?: typeof InfoCircleOutlined;
  }
> = ({ type, text, size = 'middle', customIcon, ...rest }) => {
  const { icon, color } = iconPresets[type];
  return (
    <Space size="small" direction="horizontal" align="center" {...rest}>
      <Icon icon={customIcon || icon} size={sizes[size]} color={color} />
      {text}
    </Space>
  );
};
