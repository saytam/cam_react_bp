export interface InvitationMessageResource {
  subject: string;
  html: string;
}

export interface InvitationEmailResource {
  to: string;
  message: InvitationMessageResource;
}

export interface InvitationResource {
  inviteId: string;
  companyId: string;
  userEmail: string;
}
