import { Timestamp } from 'firebase/firestore';

import { DateType } from '@cam/app/src/utils/date';
import { Price } from '@cam/firebase/resource/Common';

export interface Payment {
  paymentId: string;
  date: DateType;
  // category: string;
  description?: string;
  price: Price;
  paid: boolean; // TODO - NOT NEEDED
}
export interface PaymentResource extends Omit<Payment, 'date'> {
  companyId: string;
  date: Timestamp;
}
