import { User } from 'firebase/auth';

export interface EnhancedFirebaseUserResource extends User {
  language?: string;
}

export interface UserResource extends UpdateUserResource {
  userId: string;
}

export interface UpdateUserResource {
  displayName?: string | null;
  email?: string | null;
  language?: string;
}
