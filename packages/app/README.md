# Club Action Manager

## Available scripts

### `yarn start`

Start a development server running at http://localhost:4000. You can easily debug the application using React DevTools, Redux DevTools or the browser debugger.

### `yarn build`

Create a production-ready, minified and optimized build.

## Local development

By default, the BE requests are targeted to the **NoSQL Firebase Firestore database**.

## Tech stack

- [react](https://reactjs.org/)
- [@reduxjs/toolkit](https://redux-toolkit.js.org)
- [@reach/router](http://reach.tech/router)
- [antd](http://ant.design)
- [i18next](http://i18next.com), [react-i18next](https://react.i18next.com)
- [styled-components](http://styled-components.com)
- [chart.js](https://chartjs.org), [react-chartjs-2](https://github.com/reactchartjs/react-chartjs-2)
- [firebase](https://firebase.google.com/)
- [formik](https://formik.org)
- [dayjs](https://day.js.org/)
- [lodash](https://lodash.com/docs)

## Folder structure

- `components` - reusable, feature-agnostic components
- `constants` - definitions like routes or color palette
- `features` - single-purpose components, containing feature-specific logic
- `images` - well, images
- `locales` - automatically generated language files
- `redux` - feature-specific modules, containing the application state
- `utils` - reusable utility functions (e.g. date manipulation)
