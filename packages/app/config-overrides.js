const { override, fixBabelImports, getBabelLoader } = require('customize-cra');

const addLessLoader = require('./less-loader');

const addBabelTranspile = () => config => {
  const babelLoader = getBabelLoader(config);
  babelLoader.include = undefined;
  babelLoader.exclude = /node_modules/;
  return config;
};

module.exports = override(
  fixBabelImports('antd', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true,
  }),
  fixBabelImports('@cam/atomic', {
    libraryName: '@cam/atomic',
    transformToDefaultImport: false,
    camel2DashComponentName: false,
    customName: require('path').resolve(__dirname, '../atomic/import.js'),
  }),
  addBabelTranspile(),
  addLessLoader({
    lessOptions: {
      javascriptEnabled: true,
      modifyVars: {
        hack: `true; @import "${__dirname}/../theme/antd/index.less"; @import "${__dirname}/../theme/brands/cam.less";`,
      },
    },
  })
);
