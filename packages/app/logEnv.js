require('dotenv').config();

console.log(
  Object.fromEntries(
    Object.entries(process.env)
      .filter(([key]) => key.startsWith('REACT_APP'))
      .sort(([keyA], [keyB]) => keyA.localeCompare(keyB))
  )
);
