import { useCallback, useEffect, useRef, useState } from 'react';
import styled, { css } from 'styled-components';

export type KeyboardEvent<T = HTMLElement> = React.KeyboardEvent<T>;

enum KeyCodes {
  ENTER = 'Enter',
  SPACE = 'Space',
}

const isEnabled = (evt: KeyboardEvent) =>
  // eslint-disable-next-line eqeqeq
  evt.target['ariaDisabled'] != 'true' && evt.target['disabled'] != 'true';

export const isActionKey = (evt: KeyboardEvent, pure = false) =>
  pure
    ? KeyCodes.ENTER === evt.code
    : [KeyCodes.ENTER, KeyCodes.SPACE].includes(evt.code as KeyCodes);

export const handleKeyConfirm = (cb: () => void) => (evt: KeyboardEvent) => {
  if (isEnabled(evt) && isActionKey(evt)) cb();
};

export const getA11yHandlers = (cb: () => void) => ({
  onClick: cb,
  onKeyPress: handleKeyConfirm(cb),
  tabIndex: 0 as const,
});

const useClickOutside = (
  ref: React.RefObject<HTMLElement>,
  cb: () => void,
  checkSibling?: boolean // some components render their "children" as siblings
) => {
  useEffect(() => {
    const handleClickOutside = event => {
      if (ref.current && !ref.current.contains(event.target)) {
        if (!checkSibling || !ref.current.nextSibling?.contains(event.target)) {
          cb();
        }
      }
    };
    document.addEventListener('mousedown', handleClickOutside);
    document.addEventListener('touchstart', handleClickOutside);
    document.addEventListener('keypress', handleClickOutside);
    document.addEventListener('focusin', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
      document.removeEventListener('touchstart', handleClickOutside);
      document.removeEventListener('keypress', handleClickOutside);
      document.removeEventListener('focusin', handleClickOutside);
    };
  }, [ref, cb, checkSibling]);
};

export const useControlledMenu = ({ sibling = true } = {}) => {
  const ref = useRef(null);
  const [isOpen, setOpen] = useState(false);

  const close = useCallback(() => {
    setOpen(false);
  }, []);
  const toggle = useCallback(() => {
    setOpen(open => !open);
  }, []);

  useClickOutside(ref, close, sibling);

  const dropdownProps = {
    onVisibleChange: setOpen,
    visible: isOpen,
    getPopupContainer: sibling ? popupToParent : popupToSelf,
  };
  const buttonProps = {
    onKeyPress: handleKeyConfirm(toggle),
    ref,
    'aria-expanded': isOpen,
    'aria-haspopup': true,
  };
  const menuProps = {
    ...getA11yHandlers(toggle),
    tabIndex: -1 as const,
  };

  return { isOpen, close, toggle, set: setOpen, ref, dropdownProps, buttonProps, menuProps };
};

export const useDropdownOverlay = (): HTMLElement =>
  useRef(document.getElementById('overlayDropdowns') || document.body).current;

export const popupToSelf = (node: HTMLElement) => node || document.body;
export const popupToParent = (node: HTMLElement) => node.parentElement || document.body;

// hover doesn't work properly on touch screens -> replace with click
export const hoverTrigger = 'ontouchstart' in window ? 'click' : 'hover';

// hide information visually but keep them available to screen reader and other assistive technology
const hiddenStyle = css`
  border: 0;
  clip: rect(0 0 0 0);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  width: 1px;
`;

export const VisuallyHiddenDiv = styled.div`
  && {
    ${hiddenStyle}
  }
`;

export const VisuallyHiddenLegend = styled.legend`
  && {
    ${hiddenStyle}
  }
`;

export const VisuallyHiddenLabel = styled.label`
  && {
    ${hiddenStyle}
  }
`;

// Constants
export const FORMIK_PREVENTED_TAGNAME = 'INPUT';
export const SIDE_DRAWER_TITLE = 'sideDrawerTitle';
export const PAGE_TITLE_ID = 'pageTitle';
