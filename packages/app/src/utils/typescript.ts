export type StringKeys<T> = Extract<keyof T, string> | 'visibleColumns';

export type RequireProps<T extends object, K extends keyof T> = Omit<T, K> & {
  [MK in K]-?: NonNullable<T[MK]>;
};

export type StyledProp<Prop> = `$${Extract<Prop, string>}`;
export type StyledProps<Props> = {
  [Prop in keyof Props as StyledProp<Prop>]: Props[Prop];
};

export type ExtractProps<Component> = Component extends React.ComponentType<infer Props>
  ? Props
  : never;

export type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;

export type RequiredFields<T, K extends keyof T = keyof T> = Omit<T, K> & Required<Pick<T, K>>;

// recursive object keys
type Join<K, P> = K extends string | number
  ? P extends string | number
    ? `${K}${'' extends P ? '' : '.'}${P}`
    : never
  : never;

type Prev = [never, 0, 1, 2, 3, 4, ...0[]];

export type NestedStringKeys<T, D extends number = 3> = [D] extends [never]
  ? never
  : T extends object
  ? {
      [K in keyof T]-?: K extends string | number
        ? `${K}` | Join<K, NestedStringKeys<T[K], Prev[D]>>
        : never;
    }[keyof T]
  : '';
