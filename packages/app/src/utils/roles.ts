import { TFunction } from 'i18next';

import { RoleResource } from '@cam/firebase/resource/Company';

export const getRoleMap = (t: TFunction): Record<RoleResource, string> => ({
  [RoleResource.ADMIN]: t('common:role.admin'),
  [RoleResource.VIEWER]: t('common:role.viewer'),
  [RoleResource.EDITOR]: t('common:role.editor'),
  [RoleResource.NO_ACCESS]: t('common:role.noAccess'),
});

export const getInfoRolesMap = (): Record<RoleResource, RoleResource> => ({
  [RoleResource.ADMIN]: RoleResource.ADMIN,
  [RoleResource.VIEWER]: RoleResource.VIEWER,
  [RoleResource.EDITOR]: RoleResource.EDITOR,
  [RoleResource.NO_ACCESS]: RoleResource.NO_ACCESS,
});
