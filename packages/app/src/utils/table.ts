import { TableProps } from 'antd';
import {
  CompareFn,
  TableRowSelection,
  TableAction,
  ExpandableConfig,
} from 'antd/es/table/interface';
import { findLastIndex, get } from 'lodash';
import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

import { VisibleColumns } from '@cam/app/src/components/Table/filters/VisibleColumns';
import { TypedColumnProps, TypedSorterResult } from '@cam/app/src/utils/antd';
import { DateUtil } from '@cam/app/src/utils/date';
import {
  StringKeys,
  Optional,
  RequiredFields,
  NestedStringKeys,
} from '@cam/app/src/utils/typescript';
import { TableActions } from '@cam/redux/table';
import { FilterType } from '@cam/redux/table/filter';

export const applySorter =
  <T>(
    sorter: TypedSorterResult<T>,
    sortMap: Partial<Record<StringKeys<T>, string | CompareFn<T>>>
  ) =>
  (columns: TypedColumnProps<T>[]): TypedColumnProps<T>[] =>
    columns.map(column => ({
      ...column,
      sorter:
        sortMap.hasOwnProperty(column.dataIndex) && typeof sortMap[column.dataIndex] === 'string'
          ? true
          : (sortMap[column.dataIndex] as CompareFn<T>),
      sortOrder:
        sortMap.hasOwnProperty(column.dataIndex) && sorter.column?.dataIndex === column.dataIndex
          ? sorter.order
          : null,
    }));

export const applyFilter =
  <T>(filter: FilterType<T>) =>
  (columns: TypedColumnProps<T>[]): TypedColumnProps<T>[] =>
    columns
      .filter(
        column =>
          column.dataIndex === 'visibleColumns' ||
          !filter.visibleColumns ||
          filter.visibleColumns?.includes(column.dataIndex)
      )
      .map(column => ({ ...column, filteredValue: filter[column.dataIndex] || null }));

export const memoizeColumns = <T>(columns: TypedColumnProps<T>[]): TypedColumnProps<T>[] =>
  columns.map(column =>
    column.shouldCellUpdate
      ? column
      : { ...column, shouldCellUpdate: (next, prev) => next !== prev }
  );

export const stretchLastColumn = <T>(columns: TypedColumnProps<T>[]): TypedColumnProps<T>[] => {
  const lastIndex = findLastIndex(columns, c => c.dataIndex !== 'visibleColumns');
  return columns.map((column, idx) =>
    idx === lastIndex ? { ...column, width: undefined } : column
  );
};

export const createActionsColumn = <T>(
  render: TypedColumnProps<T>['render'],
  overrides?: Partial<TypedColumnProps<T>>,
  columns?: TypedColumnProps<T>[]
): TypedColumnProps<T> => ({
  dataIndex: 'visibleColumns',
  fixed: 'right',
  align: 'center',
  width: 64,
  render,
  ...(columns && {
    filters: columns.map(({ title, dataIndex }) => ({ text: title, value: dataIndex })),
    filterIcon: VisibleColumns,
  }),
  ...overrides,
});

export const translateTitle = <T>(
  columns: TypedColumnProps<T>[],
  columnsTranslations: Record<StringKeys<T>, string>
): TypedColumnProps<T>[] =>
  columns.map(column => ({ ...column, title: columnsTranslations[column.dataIndex] }));

export type TableColumnsHook<T> = (
  filter: FilterType<T>,
  sorter: TypedSorterResult<T>,
  ...rest: any[] //eslint-disable-line @typescript-eslint/no-explicit-any
) => TypedColumnProps<T>[];

export const textFilter =
  <T>(field: keyof T) =>
  (searchTerm: string | number | boolean, record: T): boolean => {
    const value = record[field];
    const includesTerm = (str: string) =>
      str.toLowerCase().includes(searchTerm.toString().toLowerCase());

    return (
      !!value &&
      (Array.isArray(value) ? value.some(val => includesTerm(`${val}`)) : includesTerm(`${value}`))
    );
  };

export const textSorter =
  <T>(path: NestedStringKeys<T>, direction: 'asc' | 'desc' = 'asc') =>
  (a: T, b: T): number => {
    const res = String(get(a, path, '')).localeCompare(String(get(b, path, '')), undefined, {
      sensitivity: 'base',
    });
    const dir = direction === 'desc' ? -1 : 1;
    return res * dir;
  };

export const numberSorter =
  <T>(path: NestedStringKeys<T>, direction: 'asc' | 'desc' = 'asc') =>
  (a: T, b: T): number => {
    const res = Number(get(a, path, 0)) - Number(get(b, path, 0));
    const dir = direction === 'desc' ? -1 : 1;
    return res * dir;
  };

export const dateSorter =
  <T>(path: NestedStringKeys<T>, direction: 'asc' | 'desc' = 'asc') =>
  (a: T, b: T): number => {
    const dateA = DateUtil.date(get(a, path));
    const dateB = DateUtil.date(get(b, path));
    const res = DateUtil.isAfter(dateA, dateB) ? 1 : DateUtil.isBefore(dateA, dateB) ? -1 : 0;
    const dir = direction === 'desc' ? -1 : 1;
    return res * dir;
  };

export const useTable = <T>({
  fetchData,
  tableActions: { filterActions, paginationActions, sorterActions, rowsActions },
  onChangeCallback,
  rowSelectionOptions = { selectedRowKeys: [] },
  expandableOptions = { expandedRowKeys: [] },
  resetFirebasePagination,
}: {
  fetchData?: () => void;
  tableActions: Optional<TableActions<T>['actions'], 'dataActions'>;
  onChangeCallback?: (action: TableAction) => void;
  rowSelectionOptions?: RequiredFields<TableRowSelection<T>, 'selectedRowKeys'>;
  expandableOptions?: RequiredFields<ExpandableConfig<T>, 'expandedRowKeys'>;
  resetFirebasePagination?: () => void;
}) => {
  const dispatch = useDispatch();
  const size = 'large';

  const onChange = useCallback<NonNullable<TableProps<T>['onChange']>>(
    (changedPagination, changedFilter, changedSorter, { action }) => {
      action !== 'paginate' && resetFirebasePagination?.();
      dispatch(paginationActions.setPagination(changedPagination));
      dispatch(sorterActions.setSorter(changedSorter));
      dispatch(filterActions.setFilter(changedFilter as FilterType<T>));
      fetchData?.();
      onChangeCallback?.(action);
    },
    [
      filterActions,
      paginationActions,
      sorterActions,
      dispatch,
      fetchData,
      onChangeCallback,
      resetFirebasePagination,
    ]
  );

  const rowSelection: TableRowSelection<T> = {
    type: 'checkbox',
    columnWidth: 32,
    onChange: selectedKeys => {
      dispatch(rowsActions.setSelectedRows(selectedKeys));
    },
    ...rowSelectionOptions,
  };

  const expandable: ExpandableConfig<T> = {
    onExpandedRowsChange: expandedRowKeys => {
      dispatch(rowsActions.setExpandedRows(expandedRowKeys));
    },
    ...expandableOptions,
  };

  return {
    onTableChange: onChange,
    rowSelection,
    expandable,
    size,
  };
};
