export const removeWhiteSpacesFromArray = (items: string[]) =>
  items.map(item => item.replace(/\s/g, ''));
