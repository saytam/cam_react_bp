import { navigate, Router } from '@reach/router';
import { lazy, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { LoadingScreen } from '@cam/app/src/components/LoadingScreen';
import { Route } from '@cam/app/src/components/Route';
import { Routes } from '@cam/app/src/constants/routes';
import { URL_PARAMS } from '@cam/app/src/constants/urlParams';
import { logout } from '@cam/app/src/features/oauth/utils';
import { useChangeLocale } from '@cam/app/src/locales/hooks';
import { getUserId } from '@cam/app/src/redux/Auth/selectors';
import { thunks as companyThunks } from '@cam/app/src/redux/Company';
import {
  getIsFetchingCompanies,
  getIsFetchingCompaniesError,
  getIsInitialized,
  selectAll,
} from '@cam/app/src/redux/Company/selectors';

const Invitation = lazy(
  () => import(/* webpackChunkName: "invitation" */ '@cam/app/src/features/invitation/Invitation')
);
const Signup = lazy(() => import(/* webpackChunkName: "signup" */ '@cam/app/src/features/signup'));
const InAppRouter = lazy(() => import(/* webpackChunkName: "inapp" */ '@cam/app/src/InAppRouter'));

const AppRouter: React.FC = () => {
  useChangeLocale();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const initialCompanyId = new URLSearchParams(window.location.search).get(URL_PARAMS.COMPANY_ID);
  const invitationToken = new URLSearchParams(window.location.search).get(
    URL_PARAMS.INVITATION_TOKEN
  );
  const userId = useSelector(getUserId);

  const isLoading = useSelector(getIsFetchingCompanies);
  const isInitialized = useSelector(getIsInitialized);
  const companies = useSelector(selectAll);
  const hasError = useSelector(getIsFetchingCompaniesError);

  useEffect(() => {
    if (invitationToken) {
      navigate(`${Routes.INVITATION.BASE}?${URL_PARAMS.INVITATION_TOKEN}=${invitationToken}`);
    } else {
      !isInitialized && dispatch(companyThunks.fetchCompaniesList(userId, initialCompanyId));
    }
  }, [dispatch, isInitialized, companies, invitationToken, userId, initialCompanyId]);

  if ((isLoading || !isInitialized) && !invitationToken) {
    return <LoadingScreen isLoading />;
  }

  if (hasError) {
    return (
      <LoadingScreen
        title={t('common:notification.loadingError.description')}
        primaryButton={{
          text: t('common:refresh'),
          onClick: () => dispatch(companyThunks.fetchCompaniesList(userId)),
        }}
        secondaryButton={{
          text: t('common:userMenu.logout'),
          onClick: () => logout(),
        }}
      />
    );
  }

  return (
    <>
      {companies.length > 0 ? (
        <InAppRouter />
      ) : (
        <Router>
          <Route path={Routes.SIGNUP.BASE} component={<Signup />} />
        </Router>
      )}
      {!isInitialized && invitationToken && (
        <Router>
          <Route path={Routes.INVITATION.BASE} component={<Invitation />} />
        </Router>
      )}
    </>
  );
};

// eslint-disable-next-line import/no-default-export
export default AppRouter;
