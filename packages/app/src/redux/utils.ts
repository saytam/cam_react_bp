import { OrderByDirection } from 'firebase/firestore';

import { SortDirection } from '@cam/firebase/resource/Common';

export const orderDirectionMap: Record<SortDirection, OrderByDirection> = {
  [SortDirection.ASC]: 'asc',
  [SortDirection.DESC]: 'desc',
};
