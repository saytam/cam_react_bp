import { createEntityAdapter } from '@reduxjs/toolkit';

import { Customer } from '@cam/firebase/resource/Event';

export const customersAdapter = createEntityAdapter<Customer>({
  selectId: customer => customer.customerId,
});
