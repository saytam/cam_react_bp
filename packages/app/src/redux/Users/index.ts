import { combineReducers } from '@reduxjs/toolkit';

import ProfileReducer from '@cam/app/src/redux/Users/profile';

const usersReducer = combineReducers({
  userProfileVisible: ProfileReducer,
});

export default usersReducer;
