import { navigate } from '@reach/router';
import { doc } from 'firebase/firestore';

import { Routes } from '@cam/app/src/constants/routes';
import { getUser } from '@cam/app/src/redux/Auth/selectors';
import { setSelectedCompanyId, fetchCompaniesList } from '@cam/app/src/redux/Company';
import { destroyModal } from '@cam/app/src/redux/Modals';
import { AppThunk } from '@cam/app/src/redux/store';
import { generatePermissionsMatrix } from '@cam/app/src/utils/permissionsMatrix';
import { db } from '@cam/firebase';
import { CompanyCredentialsResource, RoleResource } from '@cam/firebase/resource/Company';
import { EmployeeStatus } from '@cam/firebase/resource/Employee';

export const createCompany =
  (credentials: CompanyCredentialsResource): AppThunk =>
  async (dispatch, getState, { api, notifications }) => {
    const user = getUser(getState());
    if (!!user) {
      try {
        const { id: companyId } = doc(db.Company.company);
        await api.Company.createCompany(companyId, credentials);
        await api.Employees.createEmployee(
          {
            userId: user.uid,
            email: user.email,
            displayName: user.displayName,
            companyId,
            role: RoleResource.ADMIN,
            status: EmployeeStatus.ACTIVE,
          },
          generatePermissionsMatrix(RoleResource.ADMIN)
        );
        navigate(Routes.DASHBOARD.BASE);
        dispatch(setSelectedCompanyId(companyId));
        dispatch(fetchCompaniesList(user.uid));
      } catch (error) {
        notifications().users.createCompany.failure();
      } finally {
        dispatch(destroyModal());
      }
    }
  };

export const thunks = {
  createCompany,
};
