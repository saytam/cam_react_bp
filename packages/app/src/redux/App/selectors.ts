import { RootState } from '@cam/app/src/redux/reducer';

export const isSiderCollapsed = (state: RootState) => state.App.siderCollapsed;

export const isSidePanelExpanded = (state: RootState) => state.App.sidePanelExpanded;

export const getIsInitialized = true;
