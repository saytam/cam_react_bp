import { RootState } from '@cam/app/src/redux/reducer';

export const getModal = (state: RootState) => state.Modals.modal;
export const getConfirmLoading = (state: RootState) => state.Modals.isConfirmLoading;
