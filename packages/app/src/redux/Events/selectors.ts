import { createSelector } from '@reduxjs/toolkit';

import { eventDetailAdapter } from '@cam/app/src/redux/Events/adapters';
import { RootState } from '@cam/app/src/redux/reducer';

export const eventDetailSelectors = eventDetailAdapter.getSelectors(
  (state: RootState) => state.Events.eventDetail.detail.data
);

export const getIsEventDetailLoading = (state: RootState) =>
  state.Events.eventDetail.detail.isLoading;
export const getIsEventDetailInitialized = (state: RootState) =>
  state.Events.eventDetail.detail.isInitialized;
export const getEventDetailUpdatingIds = (state: RootState) =>
  state.Events.eventDetail.detail.updatingIds;
export const getHasEventDetailError = (state: RootState) =>
  state.Events.eventDetail.detail.hasError;

export const getIsUpdatingEvent = createSelector(
  getEventDetailUpdatingIds,
  (_: RootState, eventId: string) => eventId,
  (ids, eventId) => ids.includes(eventId)
);
