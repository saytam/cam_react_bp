import { RootState } from '@cam/app/src/redux/reducer';

export const getIsCreatingCompany = (state: RootState) => state.Signup.company.isLoading;
