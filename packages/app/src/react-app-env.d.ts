/// <reference types="react-scripts" />

declare namespace NodeJS {
  type DotenvBoolean = 'true' | 'false';

  interface ProcessEnv {
    REACT_APP_DEBUG_ENABLED: DotenvBoolean;
    REACT_APP_BUILD_NUMBER: string | undefined; // provided by CI job
    REACT_APP_FIREBASE_PUBLIC_API_KEY: string;
    REACT_APP_FIREBASE_AUTH_DOMAIN: string;
    REACT_APP_FIREBASE_PROJECT_ID: string;
    REACT_APP_FIREBASE_STORAGE_BUCKET: string;
    REACT_APP_FIREBASE_MESSAGING_SENDER_ID: string;
    REACT_APP_FIREBASE_APP_ID: string;
    REACT_APP_FIREBASE_SERVICE_ID: string;
    REACT_APP_INVITATION_BASE: string;
  }
}

interface Window {
  dataLayer?: {
    push: (event: {
      event: string;
      eventCategory: string;
      eventAction: string;
      eventLabel?: string;
    }) => void;
  };
}
