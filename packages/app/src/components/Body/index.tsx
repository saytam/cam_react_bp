import { Suspense } from 'react';
import styled from 'styled-components';

import { Loader } from '@cam/atomic';

const BodyHeader = styled.div`
  width: 100%;
  padding: 20px 20px 0 20px;
`;

const BodyWrapper = styled.div`
  height: 100%;
  padding: 10px 20px 20px 20px;
`;

export const Body: React.FC<{ header?: React.ReactNode }> = ({ children, header }) => (
  <section>
    {header && <BodyHeader>{header}</BodyHeader>}
    <BodyWrapper>
      <Suspense fallback={<Loader />}>{children}</Suspense>
    </BodyWrapper>
  </section>
);
