import { UserOutlined } from '@ant-design/icons';
import { Avatar, Layout, Menu } from 'antd';
import styled from 'styled-components';

export const StyledHeader = styled(Layout.Header)`
  && {
    line-height: 20px;
    padding: 12px 0 12px 20px;
    display: flex;
    align-items: center;
  }
`;

export const TextWrapper = styled.div`
  padding: 0 20px 0 10px;
  display: flex;
  flex-direction: column;
`;

export const StyledAvatar = styled(Avatar)`
  && {
    background-color: ${props => props.theme.colors.neutral.dark.divider};
    display: inline-flex;
    justify-content: center;
    align-items: center;
  }
`;

export const StyledIcon = styled(UserOutlined)`
  && {
    color: ${props => props.theme.colors.neutral.dark.disabled};
    font-size: 20px;
  }
`;

export const DropdownWrapper = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`;

export const StyledMenuItem = styled(Menu.Item)`
  && {
    padding: 12px;
  }
`;
