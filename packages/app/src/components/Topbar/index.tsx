import { LogoutOutlined } from '@ant-design/icons';
import { Col, Row, Menu } from 'antd';
import { useTranslation } from 'react-i18next';

import {
  DropdownWrapper,
  StyledAvatar,
  StyledHeader,
  StyledIcon,
  StyledMenuItem,
  TextWrapper,
} from '@cam/app/src/components/Topbar/styles';
import { logout } from '@cam/app/src/features/oauth/utils';
import { useControlledMenu, getA11yHandlers } from '@cam/app/src/utils/a11y';
import { Dropdown, Icon, Text } from '@cam/atomic';

interface MenuItemProps {
  onClick: () => void;
  translation: () => void;
  key: string;
  icon: React.FunctionComponent<unknown>;
  disabled?: boolean;
}

export const TopbarMenuItem: React.FC<MenuItemProps> = ({
  key,
  icon,
  onClick,
  translation,
  ...rest
}) => {
  return (
    <StyledMenuItem key={key} {...rest} onClick={onClick}>
      <Row gutter={10} align="middle">
        <Col>
          <Icon icon={icon} />
        </Col>
        <Col>{translation}</Col>
      </Row>
    </StyledMenuItem>
  );
};

export const Topbar = ({
  breadcrumbs,
  menuItems,
}: {
  breadcrumbs: React.ReactNode;
  menuItems: React.ReactNode[];
}) => (
  <StyledHeader>
    {breadcrumbs}
    <Row
      style={{
        marginLeft: 'auto',
        lineHeight: '20px',
        alignItems: 'center',
        flex: '0 0 auto',
      }}
      role="complementary"
    >
      {menuItems.map((item, idx) => (
        <Col key={idx}>{item}</Col>
      ))}
    </Row>
  </StyledHeader>
);

export const UserDropdown: React.FC<{
  userName: string;
  roleText: string;
}> = ({ children, userName, roleText }) => {
  const { t } = useTranslation();
  const { toggle, dropdownProps, buttonProps, menuProps } = useControlledMenu();

  return (
    <Dropdown
      overlay={
        <Menu theme="dark" {...menuProps}>
          {children}

          <TopbarMenuItem
            icon={LogoutOutlined}
            key="logout"
            translation={t('common:userMenu.logout')}
            {...getA11yHandlers(() => logout())}
          />
        </Menu>
      }
      trigger={['click']}
      placement="bottomRight"
      {...dropdownProps}
    >
      <DropdownWrapper {...buttonProps} {...getA11yHandlers(toggle)}>
        <StyledAvatar size={32} icon={<StyledIcon />} />
        <TextWrapper>
          <Text theme="dark" strong noWrap>
            {userName || t('common:userMenu.noName')}
          </Text>
          <Text theme="dark" type="secondary" noWrap>
            {roleText}
          </Text>
        </TextWrapper>
      </DropdownWrapper>
    </Dropdown>
  );
};
