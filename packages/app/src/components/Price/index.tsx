import { InputNumber, Select, Space, Tooltip } from 'antd';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { MAX_SMALL_INPUT_WIDTH } from '@cam/app/src/features/events/components/form/utils';
import { Currency, Price as PriceType } from '@cam/firebase/resource/Common';

interface PriceProps {
  value?: PriceType;
  onChange?: (value: PriceType) => void;
  enableCurrency?: boolean;
  disabled?: boolean;
}

export const Price: React.FC<PriceProps> = ({
  value = { amount: 0, currency: Currency.CZK },
  onChange,
  enableCurrency = false,
  disabled = false,
}) => {
  const { t } = useTranslation();
  const [price, setPrice] = useState<PriceType>(value);

  const triggerChange = (changedPrice?: PriceType) => {
    onChange?.(changedPrice || value);
  };

  const onPriceChange = (amount: number) => {
    const newPrice = { ...value, amount };
    if (amount < 0) {
      return;
    }

    if (!('amount' in value)) {
      setPrice(newPrice);
    }

    triggerChange(newPrice);
  };
  const onCurrencyChange = (currency: Currency) => {
    const newCurrency = { ...value, currency };
    if (!currency) {
      return;
    }

    if (!('currency' in value)) {
      setPrice(newCurrency);
    }

    triggerChange(newCurrency);
  };

  return (
    <Space direction="horizontal">
      <InputNumber
        min={0}
        step={100}
        type="number"
        style={{ width: MAX_SMALL_INPUT_WIDTH }}
        onChange={onPriceChange}
        value={value.amount || price.amount}
        disabled={disabled}
      />
      <Tooltip
        title={!enableCurrency ? t('events:create.form.event.payment.disabledCurrencyTooltip') : ''}
      >
        <Select
          onChange={onCurrencyChange}
          value={value.currency || price.currency}
          style={{ minWidth: 75 }}
          disabled={!enableCurrency || disabled}
        >
          {Object.keys(Currency)?.map(currency => (
            <Select.Option value={currency} key={currency}>
              {currency}
            </Select.Option>
          ))}
        </Select>
      </Tooltip>
    </Space>
  );
};
