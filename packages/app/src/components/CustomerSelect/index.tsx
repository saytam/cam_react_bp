import { Col, Row, Select, Space } from 'antd';
import { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { AccessController } from '@cam/app/src/components/AccessController';
import { ReloadButton } from '@cam/app/src/components/ReloadButton';
import { MAX_INPUT_WIDTH } from '@cam/app/src/features/events/components/form/utils';
import { fetchCustomers } from '@cam/app/src/redux/Customers';
import { getCustomers, getIsCustomersLoading } from '@cam/app/src/redux/Customers/selectors';
import { ModalType, showModal } from '@cam/app/src/redux/Modals';
import { Button, Tag } from '@cam/atomic';
import { Scope } from '@cam/firebase/resource/Company';
import { Customer } from '@cam/firebase/resource/Event';

interface CustomerSelectProps {
  value?: string;
  onChange?: (value: string) => void;
}

export const CustomerRow: React.FC<{ customer?: Customer }> = ({ customer }) => {
  if (!customer) {
    return null;
  }

  return (
    <Row justify="space-between">
      {customer.name || customer.email}
      <div>
        {customer.email && (
          <Tag variant="info" ghost>
            {customer.email}
          </Tag>
        )}
        {customer.phone && (
          <Tag variant="info" ghost>
            {customer.phone}
          </Tag>
        )}
      </div>
    </Row>
  );
};

export const CustomerSelect: React.FC<CustomerSelectProps> = ({ value = '', onChange }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const customers = useSelector(getCustomers);
  const isCustomersLoading = useSelector(getIsCustomersLoading);

  const handleReload = useCallback(() => {
    dispatch(fetchCustomers());
  }, [dispatch]);

  const onCustomerChange = (changedCustomerId: string) => {
    if (!changedCustomerId) {
      return;
    }
    onChange?.(changedCustomerId || value);
  };

  return (
    <Space direction="vertical">
      <Row justify="space-between" gutter={2} style={{ width: '100%', maxWidth: MAX_INPUT_WIDTH }}>
        <Col xs={22} md={22} lg={22}>
          <Select
            filterOption
            showSearch
            allowClear
            optionFilterProp="label"
            defaultValue={!!value ? value : undefined}
            onChange={onCustomerChange}
          >
            {customers.map(customer => (
              <Select.Option
                value={customer.customerId}
                key={customer.customerId}
                label={customer.name}
              >
                <CustomerRow customer={customer} />
              </Select.Option>
            ))}
          </Select>
        </Col>
        <Col xs={2} md={2} lg={2}>
          <ReloadButton isLoading={isCustomersLoading} fetchData={handleReload} />
        </Col>
      </Row>
      <AccessController scope={Scope.CUSTOMER}>
        <Button
          variant="primary"
          onClick={() => dispatch(showModal({ type: ModalType.CreateCustomer, props: {} }))}
        >
          {t('customers:select.createNew')}
        </Button>
      </AccessController>
    </Space>
  );
};
