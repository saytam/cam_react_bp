import { Empty as AntdEmpty } from 'antd';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { Button, Card } from '@cam/atomic';

type Size = 'xs' | 'sm' | 'md' | 'lg';

const padding: Record<Size, [top: number, bottom: number]> = {
  xs: [8, 16],
  sm: [16, 16],
  md: [32, 64],
  lg: [64, 96],
};

const isSmall = (size: Size) => ['xs', 'sm'].includes(size);

const Empty = styled(AntdEmpty)<{ $size: Size }>`
  .ant-empty-description {
    font-size: ${props => (props.$size === 'xs' ? props.theme.fontSizes.md + 'px' : undefined)};
  }

  .ant-empty-image {
    margin-bottom: ${props => (isSmall(props.$size) ? 0 : undefined)};
  }
`;

export const LoadingError: React.FC<{
  onReload: () => void;
  text: React.ReactNode;
  bg?: 'transparent';
  size?: Size;
}> = ({ onReload, text, bg, size = 'md' }) => {
  const { t } = useTranslation();
  const [paddingTop, paddingBottom] = padding[size];
  const backgroundColor = bg === 'transparent' ? 'transparent' : undefined;

  return (
    <Card
      bodyStyle={{ paddingTop, paddingBottom, backgroundColor }}
      style={{ backgroundColor }}
      bordered={bg !== 'transparent'}
    >
      <Empty description={text} $size={size}>
        <Button variant="primary" onClick={onReload}>
          {t('common:reload', { defaultValue: 'Reload' })}
        </Button>
      </Empty>
    </Card>
  );
};
