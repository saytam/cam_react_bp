import { Select, Form, Tooltip, FormInstance } from 'antd';
import { useState } from 'react';

import { Button, ButtonProps, HelpText, Radio } from '@cam/atomic';

export interface FormProps<T> {
  form: FormInstance<T>;
  name: string;
  readonly?: boolean;
}

type Option = { value: string | number; label: string; disabled?: boolean };
export type RadioProps = Option & {
  help?: string;
  isInfo?: boolean;
  isDisabled?: boolean;
  tooltipTitle?: string;
  dataTestId?: string;
};

export const renderOption = ({ value, label, disabled = false }: Option) => (
  <Select.Option key={value} value={value} aria-label={label} disabled={disabled}>
    {label}
  </Select.Option>
);

export const renderRadio = ({
  value,
  label,
  help,
  isInfo,
  isDisabled,
  tooltipTitle,
  dataTestId,
}: RadioProps & { dataTestId?: string }) => (
  <Tooltip title={tooltipTitle} key={value}>
    <Radio.Vertical disabled={isDisabled} value={value} data-test-id={dataTestId}>
      {help ? (
        <HelpText help={help} isInfo={isInfo}>
          {label}
        </HelpText>
      ) : (
        label
      )}
    </Radio.Vertical>
  </Tooltip>
);

export const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 15, push: 1 },
};

const externalFormName = 'externalForm';
export const DirtProvider: React.FC<{
  render?: { ({ dirty }: { dirty: boolean }): React.ReactElement };
}> = ({ children, render }) => {
  const [dirty, setDirty] = useState(false);

  return (
    <Form.Provider
      onFormChange={(name, context) => {
        if (render) {
          !dirty && setDirty(true);
        } else {
          const externalForm = context.forms[externalFormName];
          if (externalForm && name !== externalFormName) {
            externalForm.setFieldsValue({ dirty: true });
          }
        }
      }}
    >
      {render ? render({ dirty }) : children}
    </Form.Provider>
  );
};

export const ExternalSubmitButton: React.FC<ButtonProps & { tooltipTitle?: string }> = ({
  children,
  tooltipTitle,
  ...props
}) => {
  return (
    <Form name={externalFormName}>
      <Form.Item shouldUpdate noStyle>
        {form => (
          <Tooltip title={tooltipTitle}>
            <Button
              variant="primary"
              {...props}
              disabled={!form.getFieldValue('dirty') || props.disabled}
            >
              {children}
            </Button>
          </Tooltip>
        )}
      </Form.Item>
    </Form>
  );
};
