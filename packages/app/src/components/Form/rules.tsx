import { Rule, RuleObject } from 'antd/es/form';
import { useMemo } from 'react';
import { useTranslation, TFunction } from 'react-i18next';
import { isMobilePhone } from 'validator';

export const MAX_LENGTH_STRING = 120;

const required: Rule = { required: true, whitespace: true };

const max = (length = MAX_LENGTH_STRING, message?: string): Rule => ({ max: length, message });

const numberMax = (maxNumb: number, message?: string | React.ReactElement): Rule => ({
  type: 'number',
  max: maxNumb,
  message,
});

const numberMin = (minNumb: number, message?: string | React.ReactElement): Rule => ({
  type: 'number',
  min: minNumb,
  message,
});

const email: Rule = { type: 'email' };

const phone = (errorMessage: string): Rule => ({
  validator: (_, value) => {
    if (value && !isMobilePhone(value)) {
      return Promise.reject(errorMessage);
    }
    return Promise.resolve();
  },
});

const duplicate = (otherValues: string[], errorMessage: string): Rule => ({
  validator: (_, value) => {
    if (value && otherValues.includes(value)) {
      return Promise.reject(errorMessage);
    }
    return Promise.resolve();
  },
});

const array = ({
  type,
  empty,
  invalid,
  required: isRequired,
}: {
  type: RuleObject['type'];
  empty?: string;
  invalid?: string;
  required?: boolean;
}): Rule => ({
  required: isRequired,
  message: empty,
  type: 'array',
  defaultField: { type, message: invalid },
});

const notEqual = (originalValue: string, errorMessage: string) =>
  duplicate([originalValue], errorMessage);

const createRules = (t: TFunction) => ({
  required,
  max,
  numberMax,
  numberMin,
  email,
  phone: phone(t('common:validation.phone')),
  array,
  duplicate,
  notEqual,
});

export const useRules = () => {
  const { t } = useTranslation();
  return useMemo(() => createRules(t), [t]);
};
