import { Space } from 'antd';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { BetaSeparator } from '@cam/app/src/components/BetaLabel';
import { logo } from '@cam/app/src/components/Logo';

const LogoWrapper = styled(Space).attrs({ split: <BetaSeparator /> })<{
  $width: number;
}>`
  width: ${props => props.$width};
  height: 64px;
  padding: 16px 24px;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1;
`;

const LogoImg = styled.img<{ $collapsed: boolean }>`
  height: ${props => (props.$collapsed ? '36px' : '48px')};
  max-width: ${props => (props.$collapsed ? '36px' : '220px')};
  object-fit: contain;
  object-position: left center;
`;

interface SideMenuLogoProps {
  width: number;
  isCollapsed: boolean;
  onClick: () => void;
}

export const SideMenuLogo: React.FC<SideMenuLogoProps> = ({ width, isCollapsed, onClick }) => {
  const { t } = useTranslation();

  return (
    <LogoWrapper $width={width}>
      <LogoImg
        onClick={onClick}
        style={{ cursor: 'pointer' }}
        src={isCollapsed ? logo.color.blob : logo.color.long}
        alt={t('common:a11y.logo')}
        $collapsed={isCollapsed}
      />
    </LogoWrapper>
  );
};
