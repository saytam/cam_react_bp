import styled from 'styled-components';

import { Icon } from '@cam/atomic';

export const SIDER_WIDTH = 280;
export const SIDER_COLLAPSED_WIDTH = 80;

export const TriggerIcon = styled(Icon)<{ 'data-collapsed': boolean }>`
  transition: transform 200ms ease, opacity 300ms ease-in-out;
  transform: ${props => (props['data-collapsed'] ? 'scaleX(-1)' : 'none')};
  opacity: 0.65;

  &:hover {
    opacity: 1;
  }
`;

export const DropdownWrapper = styled.div`
  color: ${props => props.theme.colors.neutral.dark.secondary};
  cursor: pointer;
  padding-right: 16px;
`;
