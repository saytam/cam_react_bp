import { DoubleLeftOutlined } from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import { forwardRef } from 'react';
import { useTranslation } from 'react-i18next';

import { SideMenuLogo } from '@cam/app/src/components/SideMenu/SideMenuLogo';
import {
  TriggerIcon,
  SIDER_WIDTH,
  SIDER_COLLAPSED_WIDTH,
} from '@cam/app/src/components/SideMenu/styles';
import { NavLink } from '@cam/atomic';

export interface MenuItemProps {
  icon: JSX.Element;
  label: string;
  link: string;
  enabled?: boolean;
}

interface SideMenuProps {
  companySelect: React.ReactNode;
  menuItems: MenuItemProps[];
  isCollapsed: boolean;
  onClickLogo: () => void;
  onCollapse: () => void;
  selectedKey: string;
  widthCollapsed?: number;
  widthFull?: number;
}

export const SideMenu: React.FC<SideMenuProps> = forwardRef(
  (
    {
      companySelect,
      menuItems,
      isCollapsed,
      onClickLogo,
      onCollapse,
      selectedKey,
      widthCollapsed = SIDER_COLLAPSED_WIDTH,
      widthFull = SIDER_WIDTH,
    },
    ref: React.Ref<HTMLDivElement>
  ) => {
    const { t } = useTranslation();
    const trigger = (
      <TriggerIcon data-collapsed={isCollapsed} icon={DoubleLeftOutlined} size={24} />
    );
    return (
      <Layout.Sider
        id="sider"
        collapsible
        collapsed={isCollapsed}
        onCollapse={onCollapse}
        width={widthFull}
        collapsedWidth={widthCollapsed}
        breakpoint="xl"
        trigger={trigger}
        ref={ref}
      >
        <SideMenuLogo
          width={isCollapsed ? widthCollapsed : widthFull}
          isCollapsed={isCollapsed}
          onClick={onClickLogo}
        />
        {companySelect}
        <nav
          role="navigation"
          aria-label={t('common:a11y.label.mainMenu')}
          style={{ overflowY: 'auto' }}
        >
          <Menu
            mode="inline"
            theme="dark"
            selectedKeys={[selectedKey]}
            inlineIndent={isCollapsed ? 32 : 24}
          >
            {menuItems.map(item => (
              <Menu.Item key={item.link} icon={item.icon} aria-label={item.label}>
                <NavLink to={item.link}>{item.label}</NavLink>
              </Menu.Item>
            ))}
          </Menu>
        </nav>
      </Layout.Sider>
    );
  }
);
