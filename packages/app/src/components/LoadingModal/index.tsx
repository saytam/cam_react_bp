import { Modal } from 'antd';

import { Spin } from '@cam/atomic';

export const LoadingModal: React.FC = () => (
  <Modal
    closable={false}
    footer={null}
    visible
    bodyStyle={{ justifyContent: 'center' }}
    getContainer={false}
  >
    <Spin spinning />
  </Modal>
);
