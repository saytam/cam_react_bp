import { Tooltip } from 'antd';
import { ReactNode, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { accessSelectors } from '@cam/app/src/redux/Company/selectors';
import { RootState } from '@cam/app/src/redux/reducer';
import { Permission, Scope as AccessScope } from '@cam/firebase/resource/Company';

export { AccessScope };

export const AccessTooltip: React.FC<{ isDisplayed: boolean }> = ({ isDisplayed, children }) => {
  const { t } = useTranslation();
  return (
    <Tooltip title={isDisplayed ? t('common:tooltip.viewerDisabled') : undefined}>
      {children}
    </Tooltip>
  );
};
export const useRBAC = (scope: AccessScope) => {
  const accessRights = useSelector((state: RootState) => accessSelectors.selectById(state, scope));
  const hasWriteAccess = !!accessRights?.permissions?.includes(Permission.WRITE);
  const hasReadAccess = !!accessRights?.permissions?.includes(Permission.READ);

  return useMemo(
    () => ({ hasWriteAccess, hasReadAccess, hasAccessRights: !!accessRights }),
    [accessRights, hasReadAccess, hasWriteAccess]
  );
};

export type UserRights = ReturnType<typeof useRBAC>;

export const AccessController: React.FC<{
  scope: AccessScope;
  readOnly?: ReactNode;
  noPermission?: ReactNode;
}> = ({ scope, children, readOnly, noPermission }) => {
  const { hasWriteAccess, hasReadAccess, hasAccessRights } = useRBAC(scope);

  if (!hasAccessRights && noPermission) return <>{noPermission}</>;

  if (hasReadAccess) {
    return <>{hasWriteAccess ? children : readOnly}</>;
  }

  return null;
};
