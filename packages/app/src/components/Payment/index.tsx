import { Checkbox, Space, Tooltip } from 'antd';
import { CheckboxChangeEvent } from 'antd/es/checkbox';
import { Dayjs } from 'dayjs';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Price } from '@cam/app/src/components/Price';
import { MAX_SMALL_INPUT_WIDTH } from '@cam/app/src/features/events/components/form/utils';
import { DateType, DateUtil } from '@cam/app/src/utils/date';
import { DatePicker } from '@cam/atomic';
import { Currency, Price as PriceType } from '@cam/firebase/resource/Common';
import { Payment as PaymentType } from '@cam/firebase/resource/Payment';

interface PaymentProps {
  value?: PaymentType;
  onChange?: (value: PaymentType) => void;
  enableCurrency?: boolean;
}

export const Payment: React.FC<PaymentProps> = ({
  value = {
    paid: false,
    date: DateUtil.date().toISOString(),
    price: { amount: 0, currency: Currency.CZK },
    paymentId: '',
  },
  onChange,
}) => {
  const { t } = useTranslation();
  const [paid, setPaid] = useState<boolean>(value.paid);
  const [date, setDate] = useState<DateType>(value.date);

  const triggerChange = (changedValue: { paid?: boolean; date?: DateType; price?: PriceType }) => {
    onChange?.({ ...value, ...changedValue });
  };

  const onPaidChange = (e: CheckboxChangeEvent) => {
    const newPaid = e.target.checked;
    if (!e) {
      return;
    }

    if (!('paid' in value)) {
      setPaid(newPaid);
    }

    triggerChange({ paid: newPaid });
  };

  const onDateChange = (localDate: Dayjs | null) => {
    const newDate = localDate?.toISOString();

    if (!newDate) {
      return;
    }
    if (!('date' in value)) {
      setDate(newDate);
    }

    triggerChange({ date: newDate });
  };

  return (
    <Space direction="vertical">
      <Tooltip title={t('events:create.form.event.payment.paidTooltip')}>
        <Checkbox checked={value.paid || paid} value={value.paid || paid} onChange={onPaidChange}>
          {t('events:create.form.event.payment.paid')}
        </Checkbox>
      </Tooltip>
      <Price onChange={e => triggerChange({ price: e })} value={value.price} />
      <DatePicker
        format="MMM D, YYYY"
        style={{ width: MAX_SMALL_INPUT_WIDTH }}
        onChange={onDateChange}
        value={DateUtil.date(value.date || date)}
      />
    </Space>
  );
};
