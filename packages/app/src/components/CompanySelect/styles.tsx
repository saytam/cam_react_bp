import { Select } from 'antd';
import styled, { css } from 'styled-components';

const companyIcon = css`
  &:before {
    position: absolute;
    content: '';
    width: ${props => props.theme.fontSizes.md}px;
    height: ${props => props.theme.fontSizes.md}px;
    top: 0;
    margin-top: 10px;
    color: ${props => props.theme.colors.neutral.dark.primary};
  }
`;

export const StyledSelectDropdown = styled.div`
  && {
    background-color: ${props => props.theme.colors.neutral.dark.surface};
    color: ${props => props.theme.colors.neutral.dark.primary};

    .ant-select-item-option {
      color: ${props => props.theme.colors.neutral.dark.primary};

      &:hover {
        background-color: ${props => props.theme.colors.neutral.dark.hover};
      }
    }

    .ant-select-item-option-selected,
    .ant-select-item-option-active {
      background-color: ${props => props.theme.colors.neutral.dark.hover};
    }

    .ant-empty {
      color: ${props => props.theme.colors.neutral.dark.secondary};
    }

    .ant-select-item-group {
      color: ${props => props.theme.colors.neutral.dark.secondary};

      &:not(:first-of-type) {
        border-top: 1px solid ${props => props.theme.colors.neutral.dark.divider};
      }
    }
  }
`;

export const StyledSelect = styled(Select)<{
  'data-collapsed': boolean;
}>`
  && {
    width: calc(100% - 32px);
    margin: 16px;
    font-size: ${props => props.theme.fontSizes.base}px;
    color: ${props => props.theme.colors.neutral.dark.primary};

    .ant-select-arrow {
      color: ${props => props.theme.colors.neutral.dark.primary};
      right: 8px;
    }
  }

  &&&&,
  &&&&.ant-select-focused {
    .ant-select-selector {
      padding: 0 8px;
      background-color: transparent;
      border-color: ${props => props.theme.colors.neutral.dark.secondary};
      color: ${props => props.theme.colors.neutral.dark.primary};
      border-radius: ${props => props.theme.buttonRadius}px;
      font-weight: 600;
      cursor: pointer;
      position: relative;

      &:hover {
        border-color: ${props => props.theme.colors.neutral.dark.primary};
      }

      .ant-select-selection-item {
        padding-left: 24px;
        display: ${props => (props['data-collapsed'] ? 'none' : 'inline')};
      }
      .ant-select-selection-search {
        padding-left: 24px;
        left: 8px;
      }
    }
  }
`;
