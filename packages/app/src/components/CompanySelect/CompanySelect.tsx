import { PlusOutlined } from '@ant-design/icons';
import { Select, Tooltip, Row } from 'antd';
import { truncate } from 'lodash';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { StyledSelect, StyledSelectDropdown } from '@cam/app/src/components/CompanySelect/styles';
import { LoadingError } from '@cam/app/src/components/LoadingError';
import { popupToParent } from '@cam/app/src/utils/a11y';
import { Spin, Button } from '@cam/atomic';

const OptGroup = Select.OptGroup;
const Option = Select.Option;

const NAME_MAX_LENGTH = 27;
const TOOLTIP_DELAY = 0.75;
const MIN_HEIGHT = 128;
const MAX_HEIGHT = 512;

const getListHeight = (height: number) => Math.min(MAX_HEIGHT, Math.max(MIN_HEIGHT, height));

interface CompanySelectProps {
  createCompanyRoute: string;
  companyOptions: React.ReactNode[];
  isCollapsed: boolean;
  isFetchingError: boolean;
  isLoading: boolean;
  onClickReload: () => void;
  onCollapseChange: (isCollapsed: boolean) => void;
  onSelect: (value: string) => void;
  selectedCompanyId: string;
  maxHeight?: number;
}

export const CompanySelect: React.FC<CompanySelectProps> = ({
  companyOptions,
  createCompanyRoute,
  isCollapsed,
  isFetchingError,
  isLoading,
  onClickReload,
  onCollapseChange,
  onSelect,
  selectedCompanyId,
  maxHeight = 0,
}) => {
  const { t } = useTranslation();
  const [isActive, setActive] = useState(false);
  const [isOpened, setOpen] = useState(false);

  return (
    <StyledSelect
      open={isOpened}
      getPopupContainer={popupToParent}
      loading={isLoading}
      showSearch
      listHeight={getListHeight(maxHeight)}
      filterOption={true}
      optionFilterProp="data-search-value"
      value={selectedCompanyId}
      onSelect={value => onSelect(value as string)}
      data-collapsed={isCollapsed}
      size="large"
      onDropdownVisibleChange={visible => {
        setOpen(visible);
        if (visible && isCollapsed) {
          onCollapseChange(false);
          setActive(true);
        }
        if (!visible && isActive) {
          onCollapseChange(true);
          setActive(false);
        }
      }}
      dropdownMatchSelectWidth={false}
      dropdownStyle={{
        backgroundColor: 'transparent',
        padding: 0,
        margin: 0,
        width: 'calc(100% - 32px)',
      }}
      dropdownRender={menu => (
        <StyledSelectDropdown>
          <Spin spinning={isLoading}>
            {isFetchingError ? (
              <LoadingError
                onReload={onClickReload}
                text={t('company:companiesTable.loadingError')}
                bg="transparent"
                size="xs"
              />
            ) : companyOptions.length > 0 ? (
              menu
            ) : (
              <Row justify="center" align="middle" style={{ padding: 32 }}>
                <Button
                  variant="primary"
                  icon={<PlusOutlined />}
                  router={{ to: createCompanyRoute }}
                >
                  {t('company:createCompany.addCompany')}
                </Button>
              </Row>
            )}
          </Spin>
        </StyledSelectDropdown>
      )}
    >
      <OptGroup label={t('company:companyTypes.company').toUpperCase()}>{companyOptions}</OptGroup>
    </StyledSelect>
  );
};

interface CustomerSelectOptionProps {
  name: string;
  id: string;
  key?: string;
}

export const renderCompanySelectOption = ({ key, name, id }: CustomerSelectOptionProps) => {
  return (
    <Option key={key} data-search-value={name} aria-label={name} value={id}>
      <Tooltip
        mouseEnterDelay={TOOLTIP_DELAY}
        placement="right"
        title={name.length > NAME_MAX_LENGTH && name}
      >
        <span>{truncate(name, { length: NAME_MAX_LENGTH })}</span>
      </Tooltip>
    </Option>
  );
};
