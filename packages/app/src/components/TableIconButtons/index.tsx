import { CloseOutlined, MoreOutlined } from '@ant-design/icons';
import styled from 'styled-components';

import { Button, Icon } from '@cam/atomic';

export const MoreButton = styled(Button).attrs({ variant: 'text', icon: <MoreOutlined /> })`
  && {
    height: 24px;
    padding: 0;
  }
`;

export const CloseButton = styled(Button).attrs({
  variant: 'text',
  icon: <Icon icon={CloseOutlined} size={14} />,
})``;
