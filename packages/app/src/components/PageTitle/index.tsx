import { Skeleton } from 'antd';
import styled from 'styled-components';

import { Title } from '@cam/atomic';

export const DrawerTitle = styled(Title).attrs({
  level: 2,
  size: 'lg',
  tabIndex: -1,
})`
  && {
    margin: 0;
    line-height: inherit;
  }
`;

const StyledPageTitle = styled(Title).attrs({
  level: 1,
  size: 'xl',
  tabIndex: -1,
})`
  && {
    display: inline-block;
    font-weight: 700;
    margin-bottom: 0;
    line-height: 1.3;
  }
`;

export const PageTitle: React.FC<{ isLoading?: boolean; 'data-test-id'?: string }> = ({
  isLoading,
  children,
  ...rest
}) => {
  if (isLoading) return <Skeleton title={{ width: 300 }} paragraph={false} active />;

  return <StyledPageTitle {...rest}>{children}</StyledPageTitle>;
};
