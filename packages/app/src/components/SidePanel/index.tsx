import { CloseOutlined } from '@ant-design/icons';
import { Space } from 'antd';
import { useState, useEffect, ReactNode } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { DrawerTitle } from '@cam/app/src/components/PageTitle';
import { ExpandIcon, StyledDrawer, TitleWrapper } from '@cam/app/src/components/SidePanel/styles';
import { toggleSidePanelExpanded, setSidePanelExpanded } from '@cam/app/src/redux/App';
import { isSidePanelExpanded } from '@cam/app/src/redux/App/selectors';
import { Button } from '@cam/atomic';
import { useThemeColor } from '@cam/theme/colors';
import { StatusColors } from '@cam/theme/types';

export const SidePanel: React.FC<{
  title: ReactNode;
  shadow?: StatusColors;
  isExpandable?: boolean;
  headerActions?: ReactNode;
  footer?: ReactNode;
  hasMask?: boolean;
  onClose?: () => void;
  isDefaultExpanded?: boolean;
  customOnClose?: () => void;
}> = ({
  children,
  title,
  shadow = 'neutral',
  isExpandable = false,
  headerActions,
  footer,
  hasMask = true,
  onClose,
  isDefaultExpanded = false,
  customOnClose,
}) => {
  const dispatch = useDispatch();
  const isExpanded = useSelector(isSidePanelExpanded);
  const [isVisible, setVisible] = useState(true);
  const shadowColor = useThemeColor(shadow);

  useEffect(() => {
    dispatch(setSidePanelExpanded(isDefaultExpanded));

    return () => {
      dispatch(setSidePanelExpanded(false));
    };
  }, [dispatch, isDefaultExpanded]);

  const handleClose = () => (customOnClose ? customOnClose() : setVisible(false));
  const toggleExpand = () => dispatch(toggleSidePanelExpanded());

  const extra = (
    <Space>
      {headerActions}
      {isExpandable && (
        <Button
          onClick={toggleExpand}
          icon={<ExpandIcon $collapsed={isExpanded} />}
          variant="text"
        />
      )}
      <Button variant="text" onClick={handleClose} icon={<CloseOutlined />} />
    </Space>
  );

  return (
    <StyledDrawer
      shadow={shadowColor}
      title={
        <TitleWrapper>
          <DrawerTitle>{title}</DrawerTitle>
        </TitleWrapper>
      }
      extra={extra}
      placement="right"
      mask={hasMask}
      onClose={handleClose}
      visible={isVisible}
      closable={false}
      afterVisibleChange={visible => !visible && onClose && onClose()}
      footer={footer}
      getContainer={false}
      width={hasMask ? 'auto' : '100%'}
      $collapsed={isExpanded}
    >
      {children}
    </StyledDrawer>
  );
};
