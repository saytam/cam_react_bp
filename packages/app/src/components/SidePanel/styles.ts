import { DoubleLeftOutlined } from '@ant-design/icons';
import { Drawer, Space } from 'antd';
import { rgba } from 'polished';
import styled, { css } from 'styled-components';

import { SIDER_WIDTH, SIDER_COLLAPSED_WIDTH } from '@cam/app/src/components/SideMenu/styles';
import { Icon } from '@cam/atomic';

export const ExpandIcon = styled(Icon).attrs({ icon: DoubleLeftOutlined })<{ $collapsed: boolean }>`
  transition: transform 200ms ease;
  transform: ${props => (props.$collapsed ? 'scaleX(-1)' : 'none')};
`;

export const StyledDrawer = styled(Drawer)<{ $collapsed: boolean; shadow?: string }>`
  &&& {
    top: 64px;
    height: calc(100% - 64px);

    &.no-mask {
      transition: max-width 300ms ease-out, width 300ms ease-out;
      max-width: calc(100vw);

      @media only screen and (min-width: 768px) {
        max-width: ${props =>
          props.$collapsed
            ? 'calc(100vw)'
            : `calc(100vw - ${SIDER_WIDTH + SIDER_COLLAPSED_WIDTH - 40}px)`};
      }

      @media only screen and (min-width: 1300px) {
        max-width: ${props =>
          props.$collapsed ? `calc(100vw - ${2 * SIDER_WIDTH - 40}px)` : '780px'};
      }

      .ant-drawer-content {
        ${props =>
          props.shadow &&
          css`
            box-shadow: -10px 0 10px 0 ${rgba(props.shadow, 0.2)};
          `}
      }

      .ant-drawer-header,
      .ant-drawer-body,
      .ant-drawer-footer {
        padding-left: 0;
        padding-right: 0;
        margin: 0 auto;
      }
    }

    .ant-drawer-header,
    .ant-drawer-body,
    .ant-drawer-footer {
      width: calc(100vw - 10px);
      transition: width 300ms ease-out;

      @media only screen and (min-width: 768px) {
        width: ${props =>
          props.$collapsed ? 'calc(100vw - 40px)' : `calc(100vw - ${SIDER_COLLAPSED_WIDTH}px)`};
      }

      @media only screen and (min-width: 1200px) {
        width: ${props =>
          props.$collapsed ? 'calc(100vw - 40px)' : `calc(100vw - ${SIDER_WIDTH}px)`};
      }

      @media only screen and (min-width: 1300px) {
        width: ${props => (props.$collapsed ? `calc(100vw - ${2 * SIDER_WIDTH}px)` : '740px')};
      }
    }

    .ant-drawer-body,
    .ant-drawer-footer {
      padding: 16px 20px;
    }

    .ant-drawer-header {
      border-bottom: 0;
    }

    .ant-drawer-body {
      height: 100%;
    }

    .ant-drawer-footer {
      text-align: right;
    }
  }
`;

export const TitleWrapper = styled(Space)`
  display: flex;
  flex-wrap: nowrap;
  align-items: flex-start;
  justify-content: space-between;
  width: 100%;

  > :first-child {
    display: inline-flex;
    flex-wrap: wrap;
    word-break: break-word;
    flex: 1 1 auto;

    > div {
      flex: 1 1 auto;
    }
  }
`;
