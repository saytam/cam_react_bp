import { DownloadOutlined, MinusSquareOutlined, PlusSquareOutlined } from '@ant-design/icons';
import { Menu, Row, Tooltip } from 'antd';
import { useCallback } from 'react';
import { useTranslation } from 'react-i18next';

import { ReloadButton } from '@cam/app/src/components/ReloadButton';
import { hoverTrigger, getA11yHandlers, useControlledMenu } from '@cam/app/src/utils/a11y';
import { Dropdown, Button } from '@cam/atomic';

export const TableActionButtons: React.FC<{
  fetchData: () => void;
  exportCsv?: () => void;
  exportPdf?: () => void;
}> = ({ children, fetchData, exportCsv, exportPdf }) => {
  const { t } = useTranslation();
  const exportMenu = useControlledMenu({ sibling: false });

  const handleExportCsv = useCallback(() => {
    exportCsv?.();
  }, [exportCsv]);

  const handleExportPdf = useCallback(() => {
    exportPdf?.();
  }, [exportPdf]);

  const handleReload = useCallback(() => {
    fetchData();
  }, [fetchData]);

  const exportMenuItems = [
    exportCsv && (
      <Menu.Item key="csv" {...getA11yHandlers(handleExportCsv)}>
        {t('reports:actions.exportCsv')}
      </Menu.Item>
    ),
    exportPdf && (
      <Menu.Item key="pdf" {...getA11yHandlers(handleExportPdf)}>
        {t('reports:actions.exportPdf')}
      </Menu.Item>
    ),
  ].filter(Boolean);

  return (
    <Row align="middle" justify="end">
      {children}

      {exportMenuItems.length > 0 && (
        <Dropdown
          overlay={<Menu {...exportMenu.menuProps}>{exportMenuItems}</Menu>}
          trigger={[hoverTrigger]}
          {...exportMenu.dropdownProps}
        >
          <Tooltip placement="topRight" arrowPointAtCenter title={t('reports:actions.export')}>
            <Button variant="text" icon={<DownloadOutlined />} {...exportMenu.buttonProps} />
          </Tooltip>
        </Dropdown>
      )}

      <ReloadButton fetchData={handleReload} />
    </Row>
  );
};
