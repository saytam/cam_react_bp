import { Col, Row, Space } from 'antd';

export const TableHeader: React.FC<{ actions: React.ReactNode; filters: React.ReactNode }> = ({
  actions,
  filters,
}) => {
  return (
    <Row align="bottom" justify="space-between">
      <Col xs={24} md={12} lg={14} xxl={16}>
        {filters}
      </Col>
      <Col xs={24} md={12} lg={10} xxl={8}>
        <Space direction="vertical" size={4}>
          <Row align="middle" justify="end">
            {actions}
          </Row>
        </Space>
      </Col>
    </Row>
  );
};
