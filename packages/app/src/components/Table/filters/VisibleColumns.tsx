import { EllipsisOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';
import { useTranslation } from 'react-i18next';

import { Icon } from '@cam/atomic';

export const VisibleColumns: React.FC = () => {
  const { t } = useTranslation();
  return (
    <Tooltip title={t('common:table.header.columns')} placement="topRight" arrowPointAtCenter>
      <Icon icon={EllipsisOutlined} size={16} color="neutral" />
    </Tooltip>
  );
};
