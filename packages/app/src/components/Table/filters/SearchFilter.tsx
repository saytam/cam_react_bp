import { SearchOutlined, FilterFilled } from '@ant-design/icons';
import { Input, Row } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { FilterDropdownProps } from 'antd/es/table/interface';
import { useTranslation } from 'react-i18next';

import { Button } from '@cam/atomic';

export const FilterButtons: React.FC<
  Pick<FilterDropdownProps, 'confirm' | 'selectedKeys' | 'clearFilters'> & { confirmText?: string }
> = ({ selectedKeys, clearFilters, confirm, confirmText }) => {
  const { t } = useTranslation();
  const handleConfirm = () => confirm({ closeDropdown: true });
  const handleClear = () => {
    clearFilters?.();
    handleConfirm();
  };

  return (
    <Row
      justify="space-between"
      style={{ minWidth: 200 }}
      className="ant-table-filter-dropdown-btns"
    >
      <Button
        variant="link"
        size="small"
        disabled={selectedKeys.length === 0}
        onClick={handleClear}
      >
        {t('common:reset')}
      </Button>
      <Button variant="primary" size="small" onClick={handleConfirm}>
        {confirmText || t('common:ok')}
      </Button>
    </Row>
  );
};

export const SearchFilter: React.FC<FilterDropdownProps & { numberFilter?: boolean }> = ({
  confirm,
  clearFilters,
  setSelectedKeys,
  selectedKeys,
  numberFilter,
}) => {
  const { t } = useTranslation();
  const handleConfirm = () => confirm({ closeDropdown: true });

  return (
    <>
      <Row style={{ padding: 8 }}>
        {numberFilter && <label htmlFor="searchFilterInput">{t('common:largerThan')}</label>}
        <Input
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          value={selectedKeys[0]}
          onPressEnter={handleConfirm}
          id="searchFilterInput"
        />
      </Row>
      <FilterButtons
        clearFilters={clearFilters}
        selectedKeys={selectedKeys}
        confirm={handleConfirm}
        confirmText={!numberFilter ? t('common:search') : undefined}
      />
    </>
  );
};

export const searchFilterProps: Pick<ColumnProps<unknown>, 'filterDropdown' | 'filterIcon'> = {
  filterDropdown: SearchFilter,
  filterIcon: () => <SearchOutlined />,
};

export const searchNumberFilterProps: Pick<
  ColumnProps<unknown>,
  'filterDropdown' | 'filterIcon'
> = {
  filterDropdown: props => <SearchFilter {...props} numberFilter />,
  filterIcon: () => <FilterFilled />,
};
