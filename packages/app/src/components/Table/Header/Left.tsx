/* eslint-disable @typescript-eslint/no-explicit-any */
import { Menu, Row } from 'antd';
import { useCallback, ReactText } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { FilterSorterTags } from '@cam/app/src/components/Table/Header/FilterSorterTags';
import { SelectedCounts } from '@cam/app/src/components/Table/Header/SelectedCounts';
import { getA11yHandlers, hoverTrigger, useControlledMenu } from '@cam/app/src/utils/a11y';
import { StringKeys } from '@cam/app/src/utils/typescript';
import { Button, Dropdown } from '@cam/atomic';
import { TableActions } from '@cam/redux/table';
import { FilterSelectors, SorterSelectors } from '@cam/redux/table/selectors';

export type TranslationHook<T extends string = string, U = unknown> = (
  args?: U
) => [(column: T) => string, (column: T, filter: ReactText) => ReactText];

const defaultTranslationHook: TranslationHook = () => [
  column => column,
  (_, filter) => `"${filter}"`,
];

type Props<FilterIn, FilterOut, SorterIn, SorterOut, State, Data> = {
  filterSelectors: FilterSelectors<FilterIn, FilterOut, State>;
  sorterSelectors: SorterSelectors<SorterIn, SorterOut, State>;
  enableSelectAll?: boolean;
  selectedIds?: string[] | 'All';
  total?: number;
  fetchData?: () => void;
  filterActions: TableActions<Data>['actions']['filterActions'];
  sorterActions: TableActions<Data>['actions']['sorterActions'];
  paginationActions: TableActions<Data>['actions']['paginationActions'];
  rowsActions: TableActions<Data>['actions']['rowsActions'];
  useFilterTranslations?: TranslationHook<StringKeys<FilterIn>>;
};

export const LeftHeader = <FilterIn, FilterOut, SorterIn, SorterOut, State, Data>({
  total,
  selectedIds,
  enableSelectAll = false,
  fetchData,
  filterSelectors,
  filterActions,
  sorterSelectors,
  sorterActions,
  paginationActions,
  rowsActions,
  useFilterTranslations = defaultTranslationHook,
}: Props<FilterIn, FilterOut, SorterIn, SorterOut, State, Data>) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const menu = useControlledMenu();
  const selectedCount = selectedIds === 'All' ? 'All' : selectedIds?.length;
  const isSelectionActive = !!selectedCount && (selectedCount === 'All' || selectedCount > 0);
  const filter = useSelector(filterSelectors.getFilterWithoutVisibleColumns) as Record<
    string,
    ReactText[]
  >;
  const sorter = useSelector(sorterSelectors.getSorter);
  const isTableSorterActive = useSelector(sorterSelectors.isActive);
  const isTableFilterActive = useSelector(filterSelectors.isActive);
  const [translateColumn, translateFilter] = useFilterTranslations();
  const translatedFilters = Object.entries(filter)
    .filter(([, value]) => Boolean(value))
    .map(([dataIndex, filterValues]) => {
      return {
        column: { dataIndex, title: translateColumn(dataIndex as StringKeys<FilterIn>) },
        filtered: filterValues.map(item => ({
          value: item,
          text: translateFilter(dataIndex as StringKeys<FilterIn>, item),
        })),
      };
    });

  const onResetAll = useCallback(() => {
    dispatch(filterActions.reset());
    dispatch(sorterActions.reset());
    dispatch(paginationActions.reset());
    fetchData?.();
  }, [dispatch, fetchData, filterActions, sorterActions, paginationActions]);

  const onResetFilters = useCallback(() => {
    dispatch(filterActions.reset());
    dispatch(paginationActions.reset());
    fetchData?.();
  }, [dispatch, fetchData, filterActions, paginationActions]);

  const onResetSorters = useCallback(() => {
    dispatch(sorterActions.reset());
    fetchData?.();
  }, [dispatch, fetchData, sorterActions]);

  const onResetSelection = useCallback(() => {
    dispatch(rowsActions.reset());
  }, [dispatch, rowsActions]);

  if (!isTableSorterActive && !isTableFilterActive && !isSelectionActive) {
    return null;
  }

  return (
    <Row align="middle" style={{ flex: 1 }}>
      {isSelectionActive && !!selectedCount && !!total && (
        <SelectedCounts
          selectedCount={selectedCount}
          totalCount={total}
          selectAllAction={() => rowsActions.setSelectedRows('All')}
          enableSelectAll={enableSelectAll}
        />
      )}

      {(isTableSorterActive || isTableFilterActive) && (
        <FilterSorterTags
          refetchData={fetchData}
          filter={translatedFilters}
          sorter={sorter}
          resetFilter={filterActions.resetFilter}
          resetSorter={sorterActions.reset}
        />
      )}

      {(isTableSorterActive || isTableFilterActive || isSelectionActive) && (
        <Dropdown
          trigger={[hoverTrigger]}
          {...menu.dropdownProps}
          overlay={
            <Menu {...menu.menuProps}>
              <Menu.Item
                key="clearAll"
                disabled={!isTableFilterActive || !isTableSorterActive}
                {...getA11yHandlers(onResetAll)}
              >
                {t('common:clearMenu.clearAll')}
              </Menu.Item>
              <Menu.Item
                key="clearSorters"
                disabled={!isTableSorterActive}
                {...getA11yHandlers(onResetSorters)}
              >
                {t('common:clearMenu.clearSorters')}
              </Menu.Item>
              <Menu.Item
                key="clearFilters"
                disabled={!isTableFilterActive}
                {...getA11yHandlers(onResetFilters)}
              >
                {t('common:clearMenu.clearFilters')}
              </Menu.Item>
              <Menu.Item
                key="clearSelection"
                disabled={!isSelectionActive}
                {...getA11yHandlers(onResetSelection)}
              >
                {t('common:clearSelection.clearSelection')}
              </Menu.Item>
            </Menu>
          }
        >
          <Button variant="link" {...menu.buttonProps}>
            {t('common:clear')}
          </Button>
        </Dropdown>
      )}
    </Row>
  );
};
