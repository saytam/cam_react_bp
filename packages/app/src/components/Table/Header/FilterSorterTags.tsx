import { Tooltip } from 'antd';
import { SortOrder, SorterResult } from 'antd/es/table/interface';
import { TFunction } from 'i18next';
import truncate from 'lodash/truncate';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';

import { Tag } from '@cam/atomic';
import { TableActions } from '@cam/redux/table';

const MAX_FILTER_LENGTH = 75;

type SorterTagProps = {
  column: string;
  sortDir: NonNullable<SortOrder>;
  onReset: () => void;
};

// move this to some common place
const sortDirectionTranslation = (t: TFunction): Record<NonNullable<SortOrder>, string> => ({
  ascend: t('common:sortDirection.asc'),
  descend: t('common:sortDirection.desc'),
});

const Spacer = styled.div`
  margin-bottom: 2px;
`;

const SorterTag: React.FC<SorterTagProps> = ({ column, sortDir, onReset }) => {
  const { t } = useTranslation();
  const direction = sortDirectionTranslation(t)[sortDir];

  const title = `${column}: ${direction}`;

  return (
    <Tooltip title={title}>
      <Spacer>
        <Tag variant="primary" closable onClose={onReset}>
          {title}
        </Tag>
      </Spacer>
    </Tooltip>
  );
};

type FilterTagProps = {
  tooltip: string;
  filterName: React.ReactText;
  onReset: () => void;
};

const FilterTag: React.FC<FilterTagProps> = ({ tooltip, filterName, onReset }) => {
  return (
    <Tooltip title={tooltip}>
      <Spacer>
        <Tag variant="primary" closable onClose={onReset}>
          {truncate(String(filterName), { length: MAX_FILTER_LENGTH })}
        </Tag>
      </Spacer>
    </Tooltip>
  );
};

type Filter = {
  column: {
    dataIndex: string;
    title: string;
  };
  filtered: {
    value: React.ReactText;
    text: React.ReactText;
  }[];
};

type Props<T> = {
  filter: Filter[];
  sorter: SorterResult<T>;
  resetFilter: TableActions<T>['actions']['filterActions']['resetFilter'];
  resetSorter: TableActions<T>['actions']['sorterActions']['reset'];
  refetchData?: () => void;
};

export const FilterSorterTags = <T extends unknown>({
  filter,
  sorter,
  resetSorter,
  resetFilter,
  refetchData,
}: Props<T>) => {
  const dispatch = useDispatch();

  return (
    <>
      {sorter.column && sorter.order && (
        <SorterTag
          onReset={() => {
            dispatch(resetSorter());
            refetchData?.();
          }}
          column={sorter.column.title as string}
          sortDir={sorter.order}
        />
      )}
      {filter.map(item =>
        item.filtered.map(({ value, text }) => (
          <FilterTag
            key={`${item.column.dataIndex}.${value}`}
            tooltip={item.column.title}
            filterName={text}
            onReset={() => {
              dispatch(resetFilter({ key: item.column.dataIndex, value }));
              refetchData?.();
            }}
          />
        ))
      )}
    </>
  );
};
