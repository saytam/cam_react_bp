import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { Button, Text } from '@cam/atomic';

type Props = {
  selectedCount: number | 'All';
  totalCount: number;
  selectAllAction: () => void;
  enableSelectAll: boolean;
};

export const SelectedCounts: React.FC<Props> = ({
  selectedCount,
  totalCount,
  selectAllAction,
  enableSelectAll,
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  if (selectedCount === 'All' || selectedCount === totalCount) {
    return (
      <Text style={{ marginRight: 8 }}>
        {t('common:header.selected.all', { count: totalCount })}
      </Text>
    );
  }

  return (
    <>
      <Text style={{ marginRight: 8 }}>
        {t('common:header.selected.some', { count: selectedCount, total: totalCount })}
      </Text>

      {enableSelectAll && (
        <Button
          variant="link"
          onClick={() => dispatch(selectAllAction())}
          style={{ marginRight: 8 }}
        >
          {t('common:header.selectAll', { count: totalCount })}
        </Button>
      )}
    </>
  );
};
