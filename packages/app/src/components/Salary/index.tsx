import { Select, Space } from 'antd';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Price } from '@cam/app/src/components/Price';
import { getTimePerUnitTranslations } from '@cam/app/src/components/Salary/translations';
import { Currency, Price as PriceType } from '@cam/firebase/resource/Common';
import { SalaryResource, TimeUnit } from '@cam/firebase/resource/Company';

interface SalaryProps {
  value?: SalaryResource;
  onChange?: (value: SalaryResource) => void;
  disabled?: boolean;
}

const initialValues = {
  salary: { amount: { amount: 0, currency: Currency.CZK }, perTimeUnit: TimeUnit.HOUR },
  salaryId: '',
} as SalaryResource;

export const Salary: React.FC<SalaryProps> = ({
  value = initialValues,
  onChange,
  disabled = false,
}) => {
  const { t } = useTranslation();
  const [perTimeUnit, setTimePerUnit] = useState<TimeUnit>(value.salary.perTimeUnit);
  const timePerUnitTranslations = getTimePerUnitTranslations(t);

  const triggerChange = (changedSalary: { amount?: PriceType; perTimeUnit?: TimeUnit }) => {
    onChange?.({ ...value, salary: { ...value.salary, ...changedSalary } });
  };

  const onTimePerUnitChange = (newPerTimeUnit: TimeUnit) => {
    if (!Object.keys(TimeUnit).includes(newPerTimeUnit)) {
      return;
    }
    if (!('perTimeUnit' in value)) {
      setTimePerUnit(newPerTimeUnit);
    }

    triggerChange({ ...value.salary, perTimeUnit: newPerTimeUnit });
  };

  return (
    <Space direction="horizontal">
      <Price
        onChange={e => triggerChange({ amount: e })}
        value={value.salary.amount}
        disabled={disabled}
      />
      <Select
        onChange={onTimePerUnitChange}
        value={value.salary.perTimeUnit || perTimeUnit}
        style={{ minWidth: 120 }}
        disabled={disabled}
      >
        {Object.keys(TimeUnit)?.map(timeUnit => (
          <Select.Option value={timeUnit} key={timeUnit}>
            {timePerUnitTranslations[timeUnit]}
          </Select.Option>
        ))}
      </Select>
    </Space>
  );
};
