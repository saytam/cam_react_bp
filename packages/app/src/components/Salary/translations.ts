import { TFunction } from 'i18next';

import { TimeUnit } from '@cam/firebase/resource/Company';

export const getTimePerUnitTranslations = (t: TFunction): Record<TimeUnit, string> => ({
  [TimeUnit.MD]: t('common:timeUnit.MD'),
  [TimeUnit.HOUR]: t('common:timeUnit.hour'),
});
