import { useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  convertDateRangeToDayjs,
  DateRange,
  getFutureRangeFromPeriod,
  getRangeFromPeriod,
  getTimePeriodTranslations,
  TimePeriod,
  TimePeriodOptions,
} from '@cam/app/src/components/RangePicker/util';
import { DatePicker } from '@cam/atomic';

const options = [TimePeriod['7days'], TimePeriod['30days'], TimePeriod['90days']] as const;

export const useDateRange = (defaultOption: TimePeriodOptions = TimePeriod['30days']) => {
  return useState(() => getRangeFromPeriod(defaultOption));
};

const useDateRangePresets = (past?: boolean) => {
  const { t } = useTranslation();
  return useMemo(() => {
    const translations = getTimePeriodTranslations(t, past);
    return Object.fromEntries(
      options.map(opt => [
        translations[opt],
        convertDateRangeToDayjs(past ? getRangeFromPeriod(opt) : getFutureRangeFromPeriod(opt)),
      ])
    );
  }, [t, past]);
};

export const RangePicker: React.FC<{
  value: DateRange;
  onChange: ([from, to]: DateRange) => void;
  disabledDate?: (date: Date) => boolean;
  past?: boolean;
}> = ({ value, onChange, disabledDate, past }) => {
  const presets = useDateRangePresets(past);

  return (
    <DatePicker.RangePicker
      value={convertDateRangeToDayjs(value)}
      onChange={newRange => {
        if (newRange && newRange[0] && newRange[1]) {
          onChange([newRange[0].startOf('day').toDate(), newRange[1].endOf('day').toDate()]);
        }
      }}
      disabledDate={disabledDate ? date => disabledDate(date.toDate()) : undefined}
      format="ll"
      ranges={presets}
    />
  );
};
