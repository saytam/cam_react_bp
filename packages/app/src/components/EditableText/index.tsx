import styled from 'styled-components';

import { Text } from '@cam/atomic';

export const EditableText = styled(Text)`
  &&& {
    left: 0;
    margin-top: 0;

    &.ant-typography-edit-content {
      margin-bottom: 0;
    }
  }

  textarea {
    min-width: 300px;
    resize: none;
    :empty {
      border-color: ${props => props.theme.colors.error};
    }
  }
`;
