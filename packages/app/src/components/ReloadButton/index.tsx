import { ReloadOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';
import { TooltipProps } from 'antd/es/tooltip';
import { useTranslation } from 'react-i18next';

import { Button, ButtonProps } from '@cam/atomic';

export const ReloadButton: React.FC<
  ButtonProps & {
    fetchData: () => void;
    isLoading?: boolean;
    tooltipProps?: TooltipProps;
  }
> = ({ fetchData, isLoading, tooltipProps, ...props }) => {
  const { t } = useTranslation();
  return (
    <Tooltip
      placement="topRight"
      arrowPointAtCenter
      title={t('common:actions.reload')}
      {...tooltipProps}
    >
      <Button
        variant="text"
        icon={<ReloadOutlined spin={isLoading} />}
        onClick={fetchData}
        aria-label={t('common:actions.reload')}
        {...props}
      />
    </Tooltip>
  );
};
