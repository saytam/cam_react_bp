import { Col, Row, Select, SelectProps } from 'antd';
import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { ReloadButton } from '@cam/app/src/components/ReloadButton';
import { MAX_INPUT_WIDTH } from '@cam/app/src/features/events/components/form/utils';
import { fetchEmployees } from '@cam/app/src/redux/Employees';
import { getEmployees, getIsEmployeesLoading } from '@cam/app/src/redux/Employees/selectors';
import { Tag } from '@cam/atomic';
import { Employee } from '@cam/firebase/resource/Employee';

const EmployeeSelectRow: React.FC<{ employee: Employee }> = ({ employee }) => {
  return (
    <Row justify="space-between">
      {employee.displayName}
      {!!employee.job && (
        <div>
          <Tag variant="info" ghost style={{ marginLeft: 2 }}>
            {employee.job}
          </Tag>
        </div>
      )}
    </Row>
  );
};

interface EmployeeSelectProps extends SelectProps {
  value?: string | string[];
  onChange?: (value: string | string[]) => void;
  differentOptions?: Employee[];
  reload?: boolean;
}

export const EmployeeSelect: React.FC<EmployeeSelectProps> = ({
  value = '',
  onChange,
  differentOptions,
  reload = true,
  ...rest
}) => {
  const dispatch = useDispatch();
  const allEmployees = useSelector(getEmployees);
  const employees = differentOptions || allEmployees;
  const isEmployeesLoading = useSelector(getIsEmployeesLoading);

  const handleReload = useCallback(() => {
    dispatch(fetchEmployees());
  }, [dispatch]);

  const onEmployeeChange = (employeeIds: string | string[]) => {
    if (!employeeIds) {
      return;
    }
    onChange?.(employeeIds || value);
  };

  return (
    <Row justify="space-between" gutter={2} style={{ width: '100%', maxWidth: MAX_INPUT_WIDTH }}>
      <Col xs={22} md={22} lg={22}>
        <Select
          filterOption
          optionFilterProp="label"
          onChange={onEmployeeChange}
          value={value}
          {...rest}
        >
          {employees.map(employee => (
            <Select.Option
              value={employee.userId}
              key={employee.userId}
              label={`${employee.displayName || employee.email}+${employee.job}`}
            >
              <EmployeeSelectRow employee={employee} />
            </Select.Option>
          ))}
        </Select>
      </Col>
      {reload && (
        <Col xs={2} md={2} lg={2}>
          <ReloadButton isLoading={isEmployeesLoading} fetchData={handleReload} />
        </Col>
      )}
    </Row>
  );
};
