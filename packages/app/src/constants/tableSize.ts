import { SizeType } from 'antd/es/config-provider/SizeContext';

export const TABLE_SIZE: SizeType = 'large';
