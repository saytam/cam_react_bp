export enum URL_PARAMS {
  INVITATION_TOKEN = 'invitationToken',
  COMPANY_ID = 'companyId',
}
