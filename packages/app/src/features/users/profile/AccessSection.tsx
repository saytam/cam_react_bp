import { DeleteOutlined } from '@ant-design/icons';
import { Space } from 'antd';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { setSelectedCompanyId } from '@cam/app/src/redux/Company';
import { selectAll, getIsFetchingCompanies } from '@cam/app/src/redux/Company/selectors';
import { showModal, ModalType } from '@cam/app/src/redux/Modals';
import { TypedColumnProps } from '@cam/app/src/utils/antd';
import { getRoleMap } from '@cam/app/src/utils/roles';
import { createActionsColumn } from '@cam/app/src/utils/table';
import { Button, Card, Text, Icon, TextLink, Table } from '@cam/atomic';
import { CompanyList } from '@cam/firebase/resource/Company';

const useTableColumns = (): TypedColumnProps<CompanyList>[] => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const roleMap = getRoleMap(t);

  return [
    {
      title: t('account:userProfile.accessSection.table.company'),
      dataIndex: 'companyName',
      render: (_, company) => (
        <TextLink onClick={() => dispatch(setSelectedCompanyId(company.companyId))}>
          {company.companyName}
        </TextLink>
      ),
    },
    {
      title: t('account:userProfile.accessSection.table.role'),
      dataIndex: 'companyId',
      render: (_, company) => company.role && roleMap[company.role],
      width: '45%',
    },
    createActionsColumn((_, company) => <RemoveCompany company={company} />, {
      title: t('account:userProfile.accessSection.table.action'),
      width: '10%',
    }),
  ];
};

const RemoveCompany: React.FC<{ company: CompanyList }> = ({ company }) => {
  const dispatch = useDispatch();
  const handleRemove = () =>
    dispatch(
      showModal({
        type: ModalType.RemoveCompanyAccess,
        props: {
          company,
        },
      })
    );
  return (
    <Button
      variant="text"
      inline
      onClick={handleRemove}
      icon={<Icon icon={DeleteOutlined} color="error" />}
    />
  );
};

export const AccessSection: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const companies = useSelector(selectAll);
  const isLoading = useSelector(getIsFetchingCompanies);
  const tableColumns = useTableColumns();

  return (
    <Card type="inner" title={t('account:userProfile.accessSection.title')}>
      <Space direction="vertical" size="large">
        <Text>{t('account:userProfile.accessSection.description')}</Text>

        <Table
          size="small"
          pagination={false}
          dataSource={companies}
          columns={tableColumns}
          loading={isLoading}
          bordered
          rowKey={company => company.companyId}
        />
        <Button
          onClick={() =>
            dispatch(
              showModal({
                type: ModalType.CreateCompany,
                props: {},
              })
            )
          }
        >
          {t('users:createCompany.create.button')}
        </Button>
      </Space>
    </Card>
  );
};
