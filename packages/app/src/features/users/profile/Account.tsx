import { Space } from 'antd';
import { TFunction, useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { getUserInfo } from '@cam/app/src/redux/Auth/selectors';
import { Card, DetailList } from '@cam/atomic';
import { DetailItemProps } from '@cam/atomic/custom/DetailList/types';

const getListItems = (t: TFunction, name: string, email?: string | null): DetailItemProps[] => [
  {
    label: t('account:userProfile.accountSection.name'),
    content: name,
  },
  {
    label: t('account:userProfile.accountSection.email'),
    content: email,
  },
];

export const AccountSection: React.FC = () => {
  const { t } = useTranslation();
  const name = useSelector(getUserInfo).displayName;
  const { email } = useSelector(getUserInfo);

  const items = getListItems(t, name || t('common:userMenu.noName'), email);
  return (
    <Card title={t('account:userProfile.accountSection.title')} type="inner">
      <Space direction="vertical" size="middle">
        <DetailList items={items.filter(item => Object.keys(item).length)} />
      </Space>
    </Card>
  );
};
