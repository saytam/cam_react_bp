import { Form, Select } from 'antd';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { ExternalSubmitButton, DirtProvider } from '@cam/app/src/components/Form';
import { MAX_SMALL_INPUT_WIDTH } from '@cam/app/src/features/events/components/form/utils';
import { useLanguageOptions, langMap, Lang } from '@cam/app/src/locales';
import { updateCurrentUser } from '@cam/app/src/redux/Auth';
import { getIsUpdatingUser } from '@cam/app/src/redux/Auth/selectors';
import { Card } from '@cam/atomic';

enum FORM_FIELDS {
  LANGUAGE = 'language',
}

interface FormValues {
  [FORM_FIELDS.LANGUAGE]: string;
}

export const LanguageSection: React.FC = () => {
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();
  const languageOptions = useLanguageOptions(t);
  const isUpdatingUser = useSelector(getIsUpdatingUser);
  const [form] = Form.useForm<FormValues>();

  const initialValues: FormValues = {
    [FORM_FIELDS.LANGUAGE]: langMap[i18n.language as Lang],
  };

  const handleSave = async () => {
    try {
      const values = await form.validateFields();
      dispatch(updateCurrentUser({ language: values.language }));
    } catch {}
  };

  return (
    <DirtProvider>
      <Card title={t('account:userProfile.langSection.title')} type="inner">
        <Form
          initialValues={initialValues}
          form={form}
          name="userProfileForm"
          colon={false}
          labelCol={{ span: 5 }}
          wrapperCol={{ span: 19 }}
          labelAlign="left"
        >
          <Form.Item
            name={FORM_FIELDS.LANGUAGE}
            label={t('account:userProfile.langSection.language')}
          >
            <Select
              options={languageOptions}
              defaultOpen={false}
              style={{ maxWidth: MAX_SMALL_INPUT_WIDTH }}
            />
          </Form.Item>
        </Form>

        <ExternalSubmitButton onClick={handleSave} loading={isUpdatingUser}>
          {t('common:saveBtn')}
        </ExternalSubmitButton>
      </Card>
    </DirtProvider>
  );
};
