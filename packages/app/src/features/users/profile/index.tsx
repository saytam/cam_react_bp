import { UserOutlined } from '@ant-design/icons';
import { Space } from 'antd';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { SidePanel } from '@cam/app/src/components/SidePanel';
import { AccessSection } from '@cam/app/src/features/users/profile/AccessSection';
import { AccountSection } from '@cam/app/src/features/users/profile/Account';
import { DetailSection } from '@cam/app/src/features/users/profile/Detail';
import { LanguageSection } from '@cam/app/src/features/users/profile/Language';
import { getUserInfo } from '@cam/app/src/redux/Auth/selectors';
import { getSelectedCompany } from '@cam/app/src/redux/Company/selectors';
import { isUserProfileVisible, setUserProfileVisible } from '@cam/app/src/redux/Users/profile';
import { getRoleMap } from '@cam/app/src/utils/roles';
import { Icon } from '@cam/atomic';

export const UserProfile: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const isPanelVisible = useSelector(isUserProfileVisible);
  const name = useSelector(getUserInfo).displayName;
  const companyRole = useSelector(getSelectedCompany)?.role;
  const pageTitle = [
    name || t('common:userMenu.noName'),
    companyRole ? getRoleMap(t)[companyRole] : t('common:role.admin'),
  ]
    .filter(Boolean)
    .join(' - ');

  const handleClose = () => {
    dispatch(setUserProfileVisible(false));
  };

  if (!isPanelVisible) return null;

  return (
    <SidePanel
      title={
        <Space>
          <Icon icon={UserOutlined} />
          {pageTitle}
        </Space>
      }
      onClose={handleClose}
    >
      <Space direction="vertical">
        <AccountSection />
        <LanguageSection />
        <DetailSection />
        <AccessSection />
      </Space>
    </SidePanel>
  );
};
