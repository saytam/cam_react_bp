import { Space, Form } from 'antd';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { AccessController, AccessScope } from '@cam/app/src/components/AccessController';
import { FormProps, DirtProvider, ExternalSubmitButton } from '@cam/app/src/components/Form';
import { RoleType } from '@cam/app/src/features/employees/detail/form';
import { Job, Salary } from '@cam/app/src/features/employees/detail/form/components';
import { EmployeeProps } from '@cam/app/src/features/employees/detail/form/types';
import { getUserId } from '@cam/app/src/redux/Auth/selectors';
import { fetchEmployeeDetail, updateEmployee } from '@cam/app/src/redux/Employees/detail';
import {
  getEmployeeDetailFormData,
  getIsLoadingDetail,
  getIsUpdatingDetail,
} from '@cam/app/src/redux/Employees/selectors';
import { RootState } from '@cam/app/src/redux/reducer';
import { Card, Spin } from '@cam/atomic';

const DetailForm: React.FC<FormProps<EmployeeProps>> = ({ form, name: formName }) => {
  const userId = useSelector(getUserId);

  const { job, salary, role } = useSelector((state: RootState) =>
    getEmployeeDetailFormData(state, userId)
  );

  const initialValues: EmployeeProps = {
    role,
    job,
    salary,
  };

  useEffect(() => {
    form.resetFields();
  }, [form, role, job, salary]);

  return (
    <Form
      name={formName}
      form={form}
      initialValues={initialValues}
      colon={false}
      labelCol={{ span: 5 }}
      wrapperCol={{ span: 19 }}
      labelAlign="left"
    >
      <Space direction="vertical" style={{ width: '100%' }}>
        <RoleType disabled />
        <AccessController scope={AccessScope.EMPLOYEE} readOnly={<Job disabled />}>
          <Job />
        </AccessController>
        <AccessController scope={AccessScope.EMPLOYEE} readOnly={<Salary disabled />}>
          <Salary />
        </AccessController>
      </Space>
    </Form>
  );
};

export const DetailSection: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userId = useSelector(getUserId);

  const isUpdating = useSelector(getIsUpdatingDetail);
  const isLoading = useSelector(getIsLoadingDetail);
  const [form] = Form.useForm<EmployeeProps>();

  useEffect(() => {
    dispatch(fetchEmployeeDetail(userId));
  }, [dispatch, userId]);

  const handleSave = async () => {
    try {
      const values = await form.validateFields();
      dispatch(updateEmployee(userId, values));
    } catch {}
  };

  useEffect(() => {
    form.resetFields();
  }, [form]);

  return (
    <DirtProvider>
      <Card title={t('account:userProfile.detailSection.title')} type="inner">
        <Spin spinning={isLoading || isUpdating}>
          <DetailForm name="currentUserDetailForm" form={form} />
        </Spin>
        <AccessController scope={AccessScope.EMPLOYEE}>
          <ExternalSubmitButton onClick={handleSave} loading={isUpdating}>
            {t('common:saveBtn')}
          </ExternalSubmitButton>
        </AccessController>
      </Card>
    </DirtProvider>
  );
};
