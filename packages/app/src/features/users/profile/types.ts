import { RoleResource, SalaryResource } from '@cam/firebase/resource/Company';

export enum FORM_FIELDS {
  JOB = 'job',
  SALARY = 'salary',
  ROLE = 'role',
}

export interface CurrentUserDetailProps {
  [FORM_FIELDS.JOB]?: string;
  [FORM_FIELDS.SALARY]?: SalaryResource;
  [FORM_FIELDS.ROLE]: RoleResource;
}
