import { Modal } from 'antd';
import { useState, ReactNode } from 'react';
import { useDispatch } from 'react-redux';

import { destroyModal } from '@cam/app/src/redux/Modals';
import { StatusText } from '@cam/atomic';

interface InfoProps {
  title: string;
  footer?: ReactNode;
  width?: string | number;
  bodyStyle?: React.CSSProperties;
}

export const InfoModal: React.FC<InfoProps> = ({ title, children, footer, width, bodyStyle }) => {
  const dispatch = useDispatch();
  const [isVisible, setVisible] = useState(true);
  const onCancel = () => setVisible(false);

  return (
    <Modal
      title={<StatusText text={title} type="info" style={{ overflowWrap: 'anywhere' }} />}
      visible={isVisible}
      getContainer={false}
      afterClose={() => dispatch(destroyModal())}
      onCancel={onCancel}
      destroyOnClose={true}
      footer={footer || null}
      width={width}
      bodyStyle={bodyStyle}
    >
      {children}
    </Modal>
  );
};
