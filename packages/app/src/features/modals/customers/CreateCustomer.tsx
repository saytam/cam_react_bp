import { Form, Input } from 'antd';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { useRules } from '@cam/app/src/components/Form/rules';
import { ConfirmationType, Confirmation } from '@cam/app/src/features/modals/Confirmation';
import { createCustomer } from '@cam/app/src/redux/Customers';
import { AllModalProps } from '@cam/app/src/redux/Modals/types';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

enum CreateCustomerField {
  NAME = 'name',
  EMAIL = 'email',
  PHONE = 'phone',
}
type CreateCustomerForm = {
  [CreateCustomerField.NAME]: string;
  [CreateCustomerField.EMAIL]?: string;
  [CreateCustomerField.PHONE]?: string;
};

export const CreateCustomer: React.FC<AllModalProps> = ({ modalType }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const rules = useRules();
  const [form] = Form.useForm<CreateCustomerForm>();

  const initialValues: CreateCustomerForm = {
    name: '',
    email: '',
    phone: '',
  };

  const handleSubmit = async () => {
    try {
      const values = await form.validateFields();
      const { email, name, phone } = values;
      dispatch(
        createCustomer({
          email,
          name,
          phone,
        })
      );
    } catch {}
  };

  return (
    <Confirmation
      type={ConfirmationType.Info}
      title={t('customers:createCustomer.title')}
      actionText={t('customers:createCustomer.create.button')}
      formName={modalType}
    >
      <Form
        name={modalType}
        form={form}
        initialValues={initialValues}
        {...layout}
        onFinish={handleSubmit}
      >
        <Form.Item
          label={t('customers:createCustomer.form.name')}
          name={CreateCustomerField.NAME}
          rules={[rules.required, rules.max(40)]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={t('customers:createCustomer.form.email')}
          name={CreateCustomerField.EMAIL}
          rules={[rules.email]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={t('customers:createCustomer.form.phone')}
          name={CreateCustomerField.PHONE}
          rules={[rules.phone]}
        >
          <Input />
        </Form.Item>
      </Form>
    </Confirmation>
  );
};
