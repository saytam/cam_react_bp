import { CreateCustomer } from '@cam/app/src/features/modals/customers/CreateCustomer';
import { ModalType } from '@cam/app/src/redux/Modals/types';

export const customersModalMap = {
  [ModalType.CreateCustomer]: CreateCustomer,
};
