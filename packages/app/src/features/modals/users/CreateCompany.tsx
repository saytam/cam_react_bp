import { Form, Input, Select } from 'antd';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { useRules } from '@cam/app/src/components/Form/rules';
import { ConfirmationType, Confirmation } from '@cam/app/src/features/modals/Confirmation';
import { categoriesTranslations } from '@cam/app/src/features/signup/translations';
import { AllModalProps } from '@cam/app/src/redux/Modals/types';
import { createCompany } from '@cam/app/src/redux/Users/create';
import { HelpText, Paragraph } from '@cam/atomic';
import { Currency as CurrencyEnum } from '@cam/firebase/resource/Common';
import { BusinessType } from '@cam/firebase/resource/Company';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

enum CreateCompanyField {
  Name = 'companyName',
  Category = 'category',
  Currency = 'currency',
}

type CreateCompanyForm = {
  [CreateCompanyField.Name]: string;
  [CreateCompanyField.Category]: BusinessType;
  [CreateCompanyField.Currency]: CurrencyEnum;
};

export const CreateCompany: React.FC<AllModalProps> = ({ modalType }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const rules = useRules();
  const [form] = Form.useForm<CreateCompanyForm>();
  const categories = categoriesTranslations(t);

  const handleSubmit = async () => {
    try {
      const values = await form.validateFields();
      dispatch(
        createCompany({
          businessType: values[CreateCompanyField.Category],
          name: values[CreateCompanyField.Name],
          currency: values[CreateCompanyField.Currency],
        })
      );
    } catch {}
  };

  return (
    <Confirmation
      type={ConfirmationType.Info}
      title={t('users:createCompany.title')}
      actionText={t('users:createCompany.create.button')}
      formName={modalType}
    >
      <Paragraph>{t('users:createCompany.description')}</Paragraph>
      <Form
        name={modalType}
        form={form}
        initialValues={{ currency: CurrencyEnum.CZK }}
        {...layout}
        onFinish={handleSubmit}
      >
        <Form.Item
          label={t('users:createCompany.form.label.name')}
          name="companyName"
          rules={[rules.required, rules.max()]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label={t('signup:credentials.businessType.label')}
          name="category"
          rules={[rules.required]}
        >
          <Select
            filterOption
            showSearch
            optionFilterProp="children"
            placeholder={t('signup:credentials.businessType.placeholder')}
          >
            {Object.values(BusinessType).map(category => (
              <Select.Option value={category} key={category}>
                {categories[category]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label={
            <HelpText help={t('signup:credentials.currency.help')}>
              {t('signup:credentials.currency.label')}
            </HelpText>
          }
          name="currency"
          rules={[rules.required]}
        >
          <Select>
            {Object.keys(CurrencyEnum)?.map(currency => (
              <Select.Option value={currency} key={currency}>
                {currency}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Confirmation>
  );
};
