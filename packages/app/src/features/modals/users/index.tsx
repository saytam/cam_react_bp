import { CreateCompany } from '@cam/app/src/features/modals/users/CreateCompany';
import { RemoveCompanyAccess } from '@cam/app/src/features/modals/users/RemoveCompanyAccess';
import { ModalType } from '@cam/app/src/redux/Modals/types';

export const usersModalMap = {
  [ModalType.CreateCompany]: CreateCompany,
  [ModalType.RemoveCompanyAccess]: RemoveCompanyAccess,
};
