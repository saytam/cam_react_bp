import { Trans, useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { ConfirmationType, Confirmation } from '@cam/app/src/features/modals/Confirmation';
import { getUserId } from '@cam/app/src/redux/Auth/selectors';
import { thunks as companyThunks } from '@cam/app/src/redux/Company';
import { getSelectedCompanyId } from '@cam/app/src/redux/Company/selectors';
import { Paragraph } from '@cam/atomic';
import { CompanyList } from '@cam/firebase/resource/Company';

type CompanyModalProps = {
  company: CompanyList;
};

export const RemoveCompanyAccess: React.FC<CompanyModalProps> = ({ company }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const companyId = useSelector(getSelectedCompanyId);
  const userId = useSelector(getUserId);
  const isRemovingCurrentCompany = companyId === company.companyId;

  const handleRemove = () => {
    dispatch(companyThunks.removeEmployee(userId, company));
  };

  return (
    <Confirmation
      type={ConfirmationType.Warning}
      title={t('users:modal.removeCompany.title')}
      action={handleRemove}
      actionText={t('users:modal.removeCompany.button')}
    >
      <Paragraph>
        <Trans
          t={t}
          i18nKey="users:modal.removeCompany.description"
          values={{ name: company.companyName }}
        >
          Your access to <strong>{company.companyName}</strong>
          will be removed. <strong>You cannot undo this action</strong>.
        </Trans>
      </Paragraph>
      {isRemovingCurrentCompany && (
        <Paragraph>{t('users:modal.removeCompany.logOutAlert')}</Paragraph>
      )}
      <Paragraph>{t('users:modal.warning')}</Paragraph>
    </Confirmation>
  );
};
