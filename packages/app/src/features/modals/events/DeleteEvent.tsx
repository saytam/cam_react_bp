import { Trans, useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { ConfirmationType, Confirmation } from '@cam/app/src/features/modals/Confirmation';
import { deleteEvent } from '@cam/app/src/redux/Events/eventDetail';
import { eventDetailSelectors } from '@cam/app/src/redux/Events/selectors';
import { RootState } from '@cam/app/src/redux/reducer';
import { Paragraph, Text } from '@cam/atomic';

export const DeleteEvent: React.FC<{ eventId: string }> = ({ eventId }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const event = useSelector((state: RootState) => eventDetailSelectors.selectById(state, eventId));
  const eventName = event?.name;
  const handleRemove = () => {
    dispatch(deleteEvent(eventId));
  };

  return (
    <Confirmation
      type={ConfirmationType.Warning}
      title={t('events:detail.event.delete.title')}
      action={handleRemove}
      actionText={t('events:detail.event.deleteButton')}
    >
      <Paragraph>
        <Text>
          <Trans t={t} i18nKey="events:detail.event.delete.description" values={{ eventName }}>
            Event <strong>{{ eventName }}</strong> and all transactions connected to this event will
            be deleted.
          </Trans>
          &nbsp;
          <Text strong>{t('events:modal.delete.warning')}</Text>
        </Text>
      </Paragraph>
      <Paragraph>{t('events:modal.warning')}</Paragraph>
    </Confirmation>
  );
};
