import { FormikProps } from 'formik';

import { AllModalProps } from '@cam/app/src/redux/Modals/types';
import { EventDetailResource } from '@cam/firebase/resource/Event';

export type EventsModalProps = AllModalProps & {
  eventId: string;
  data?: FormikProps<EventDetailResource>;
};
