import { navigate } from '@reach/router';
import { Space, Row, message, MessageArgsProps } from 'antd'; // eslint-disable-line no-restricted-imports
import { TFunction } from 'i18next';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { renderRadio } from '@cam/app/src/components/Form';
import { Routes } from '@cam/app/src/constants/routes';
import { useDiscardFormikChanges } from '@cam/app/src/features/events/hooks';
import { getDataFromObjects, saveEventIntoLocalStorage } from '@cam/app/src/features/events/utils';
import { ConfirmationType, Confirmation } from '@cam/app/src/features/modals/Confirmation';
import { EventsModalProps } from '@cam/app/src/features/modals/events/types';
import { updateEvent } from '@cam/app/src/redux/Events/eventDetail';
import { eventDetailSelectors } from '@cam/app/src/redux/Events/selectors';
import { destroyModal } from '@cam/app/src/redux/Modals';
import { RootState } from '@cam/app/src/redux/reducer';
import { Text, Radio } from '@cam/atomic';

enum SaveType {
  DISCARD = 'discard',
  KEEP = 'keep',
  APPLY = 'apply',
}

const getAuthTypesMap = (t: TFunction) => [
  {
    label: t('events:detail.save.discardOption'),
    value: SaveType.DISCARD,
  },
  {
    label: t('events:detail.save.keepOption'),
    value: SaveType.KEEP,
  },
  {
    label: t('events:detail.save.applyOption'),
    value: SaveType.APPLY,
  },
];

export const EventSave: React.FC<EventsModalProps> = ({ eventId, data }) => {
  const { t } = useTranslation();
  const [closeAction, setCloseAction] = useState(SaveType.KEEP);
  const { discardChanges } = useDiscardFormikChanges(eventId);
  const dispatch = useDispatch();
  const event = useSelector((state: RootState) => eventDetailSelectors.selectById(state, eventId));

  const showEventError = (error: MessageArgsProps) => {
    message.open(error);
    dispatch(destroyModal());
  };

  const handleSubmit = () => {
    if (closeAction === SaveType.DISCARD) {
      discardChanges();
      dispatch(destroyModal());
    } else if (closeAction === SaveType.KEEP) {
      saveEventIntoLocalStorage({
        errMessage: t('events:detail.save.maximumQuota.error'),
        eventId,
        data: JSON.stringify(data),
      });
      dispatch(destroyModal());
      navigate(Routes.EVENTS.BASE);
    } else {
      const { error } = getDataFromObjects(t, event, data?.values, data?.errors);

      error
        ? showEventError(error)
        : data &&
          dispatch(
            updateEvent(data.values, () => {
              data?.resetForm();
              navigate(Routes.EVENTS.BASE);
            })
          );
    }
  };

  return (
    <Confirmation
      type={ConfirmationType.Info}
      title={t('events:detail.save.modal.title')}
      actionText={t('common:confirm')}
      action={handleSubmit}
    >
      <Space direction="vertical">
        <Text>{t('events:detail.save.modal.description')}</Text>

        <Row>
          <Radio.Group
            value={closeAction}
            onChange={val => setCloseAction(val.target.value)}
            name="authTypes"
          >
            {getAuthTypesMap(t).map(renderRadio)}
          </Radio.Group>
        </Row>
      </Space>
    </Confirmation>
  );
};
