import { DeleteEvent } from '@cam/app/src/features/modals/events/DeleteEvent';
import { EventSave } from '@cam/app/src/features/modals/events/EventSave';
import { ModalType } from '@cam/app/src/redux/Modals/types';

export const eventsModalMap = {
  [ModalType.EventSave]: EventSave,
  [ModalType.DeleteEvent]: DeleteEvent,
};
