/* eslint-disable @typescript-eslint/no-explicit-any */
import { Suspense } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { LoadingModal } from '@cam/app/src/components/LoadingModal';
import { showModal } from '@cam/app/src/redux/Modals';
import { getModal } from '@cam/app/src/redux/Modals/selectors';

export const usePreviousModal = () => {
  const dispatch = useDispatch();
  const modal = useSelector(getModal);
  return modal ? () => dispatch(showModal(modal)) : undefined;
};

export const ModalManager: React.FC<{
  modalMap: Record<string, React.ComponentType<any>>;
}> = ({ modalMap }) => {
  const modal = useSelector(getModal);

  if (!modal) return null;

  const Component = modalMap[modal.type];

  return (
    <div style={{ height: 'auto' }}>
      <Suspense fallback={<LoadingModal />}>
        <Component {...modal.props} modalType={modal.type} />
      </Suspense>
    </div>
  );
};
