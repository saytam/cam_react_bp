import { Modal, Form } from 'antd';
import { FormProviderProps } from 'rc-field-form/es/FormContext';
import { cloneElement, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { destroyModal, startConfirmLoading } from '@cam/app/src/redux/Modals';
import { getConfirmLoading } from '@cam/app/src/redux/Modals/selectors';
import { Button, StatusText, StatusTextType } from '@cam/atomic';

export enum ConfirmationType {
  Warning = 'warning',
  Info = 'info',
}

type CommonConfirmationProps = {
  type: ConfirmationType;
  title: string;
  actionText: string;
  footerEl?: React.ReactElement;
  width?: string | number;
  isSubmitDisabledDefault?: boolean;
  isDisabled?: boolean;
  onCancel?: () => void;
};

type ActionConfirmationProps = {
  action: () => void;
  formName?: never;
};

type FormConfirmationProps = {
  action?: never;
  formName: string;
};

type ConfirmationProps = CommonConfirmationProps &
  (ActionConfirmationProps | FormConfirmationProps);

const confirmationTypeToStatusTextType: Record<ConfirmationType, StatusTextType> = {
  [ConfirmationType.Info]: 'info',
  [ConfirmationType.Warning]: 'error',
};

const Footer: React.FC<{
  confirmationProps: ConfirmationProps;
  onCancel: () => void;
  isDisabled: boolean;
  footerEl?: React.ReactElement;
  formName?: string;
}> = ({
  confirmationProps: { type, action, actionText },
  onCancel,
  isDisabled,
  footerEl,
  formName,
}) => {
  const { t } = useTranslation();
  const isConfirmLoading = useSelector(getConfirmLoading);

  const footerComponent = footerEl && cloneElement(footerEl, { key: 'footerEl' });
  const cancelButton = (
    <Button key="back" onClick={onCancel}>
      {t('common:cancel')}
    </Button>
  );
  const submitButton = (
    <Button
      key="submit"
      variant="primary"
      danger={type === ConfirmationType.Warning}
      onClick={action}
      loading={isConfirmLoading}
      disabled={isDisabled}
      {...(formName && { htmlType: 'submit', form: formName })}
    >
      {actionText}
    </Button>
  );

  return (
    <>
      {type === ConfirmationType.Warning
        ? [footerComponent, submitButton, cancelButton]
        : [footerComponent, cancelButton, submitButton]}
    </>
  );
};

export const Confirmation: React.FC<ConfirmationProps> = ({
  title,
  actionText,
  action,
  type,
  children,
  footerEl,
  width,
  formName = '',
  isSubmitDisabledDefault = true,
  isDisabled = false,
  onCancel,
  ...props
}) => {
  const dispatch = useDispatch();
  const [isVisible, setVisible] = useState(true);
  const [isSubmitDisabled, setSubmitDisabled] = useState(!!formName && isSubmitDisabledDefault);

  const handleCancel = () => {
    setVisible(false);
    onCancel?.();
  };

  const handleConfirm = () => {
    !formName && dispatch(startConfirmLoading());
    action?.();
  };

  const handleFormFinish: FormProviderProps['onFormFinish'] = submittedForm => {
    // trigger modal loading after form.submit
    if (submittedForm === formName) {
      dispatch(startConfirmLoading());
    }
  };

  const handleFormChange: FormProviderProps['onFormChange'] = (_, { forms }) => {
    // using validateFields here causes infinite loop
    const errors = forms[formName]?.getFieldsError?.();
    const hasErrors = errors?.some(field => field.errors.length > 0);
    const dirty = forms[formName]?.isFieldsTouched?.();
    const isValid = dirty && !hasErrors;
    isSubmitDisabledDefault ? setSubmitDisabled(!isValid) : setSubmitDisabled(hasErrors);
  };

  return (
    <Modal
      {...props}
      visible={isVisible}
      afterClose={() => dispatch(destroyModal())}
      onCancel={handleCancel}
      title={<StatusText text={title} type={confirmationTypeToStatusTextType[type]} />}
      getContainer={false}
      footer={
        <Footer
          confirmationProps={{ title, actionText, action: handleConfirm, type }}
          onCancel={handleCancel}
          isDisabled={isDisabled || isSubmitDisabled}
          footerEl={footerEl}
          formName={formName}
        />
      }
      destroyOnClose={true}
      width={width}
    >
      {formName ? (
        <Form.Provider onFormChange={handleFormChange} onFormFinish={handleFormFinish}>
          {children}
        </Form.Provider>
      ) : (
        children
      )}
    </Modal>
  );
};
