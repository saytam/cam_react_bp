import { Trans, useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { ConfirmationType, Confirmation } from '@cam/app/src/features/modals/Confirmation';
import { EmployeesModalProps } from '@cam/app/src/features/modals/employees/types';
import { removeInvitation } from '@cam/app/src/redux/Employees';
import { Paragraph } from '@cam/atomic';

export const RemoveInvitedEmployee: React.FC<EmployeesModalProps> = ({ name, id }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handleRemove = () => {
    dispatch(removeInvitation(id));
  };
  return (
    <Confirmation
      type={ConfirmationType.Warning}
      title={t('employees:modal.removeInvite.title')}
      action={handleRemove}
      actionText={t('employees:modal.removeInvite.button')}
    >
      <Paragraph>
        <Trans t={t} i18nKey="employees:modal.removeInvite.description" values={{ name }}>
          Invite for <strong>{{ name }}</strong> will be removed.
        </Trans>
      </Paragraph>
      <Paragraph>{t('employees:modal.warning')}</Paragraph>
    </Confirmation>
  );
};
