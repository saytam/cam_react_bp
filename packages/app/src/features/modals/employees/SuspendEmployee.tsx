import { Trans, useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { ConfirmationType, Confirmation } from '@cam/app/src/features/modals/Confirmation';
import { EmployeesModalProps } from '@cam/app/src/features/modals/employees/types';
import { suspendEmployee } from '@cam/app/src/redux/Employees/detail';
import { Paragraph } from '@cam/atomic';

export const SuspendEmployee: React.FC<EmployeesModalProps> = ({ name, id }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  return (
    <Confirmation
      type={ConfirmationType.Warning}
      title={t('employees:modal.suspend.title')}
      action={() => dispatch(suspendEmployee(id))}
      actionText={t('employees:modal.suspend.button')}
    >
      <Paragraph>
        <Trans t={t} i18nKey="employees:modal.suspend.description" values={{ name }}>
          Your employee <strong>{{ name }}</strong> will be suspended and his login access will be
          suspended and permissions will be revoked.
        </Trans>
      </Paragraph>

      <Paragraph>{t('employees:modal.warning')}</Paragraph>
    </Confirmation>
  );
};
