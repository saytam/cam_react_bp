import { CheckCircleFilled, CloseCircleFilled } from '@ant-design/icons';
import { TFunction } from 'i18next';
import { TdHTMLAttributes } from 'react';
import { useTranslation } from 'react-i18next';

import { InfoModal } from '@cam/app/src/features/modals/InfoModal';
import { TypedColumnProps } from '@cam/app/src/utils/antd';
import { Icon, Table } from '@cam/atomic';
import { RoleResource } from '@cam/firebase/resource/Company';

interface PermisionProp {
  title: string;
  [RoleResource.ADMIN]: boolean;
  [RoleResource.EDITOR]: boolean;
  [RoleResource.VIEWER]: boolean;
}

const getPermisionTitles = (t: TFunction) => [
  t('employees:modal.permissions.1'),
  t('employees:modal.permissions.2'),
  t('employees:modal.permissions.3'),
  t('employees:modal.permissions.4'),
  t('employees:modal.permissions.5'),
];

const getPermisions = (t: TFunction): PermisionProp[] => {
  return getPermisionTitles(t).map((permision, i) => ({
    title: permision,
    [RoleResource.ADMIN]: true,
    [RoleResource.EDITOR]: [0, 2].includes(i),
    [RoleResource.VIEWER]: false,
  }));
};

const getColumns = (t: TFunction): TypedColumnProps<PermisionProp>[] => [
  {
    title: t('employees:modal.table.header.permisions'),
    dataIndex: 'title',
    onCell: () => ({ scope: 'row' } as TdHTMLAttributes<HTMLTableCellElement>),
  },
  {
    title: t('employees:modal.table.header.admin'),
    dataIndex: RoleResource.ADMIN,
    render: () => <Icon icon={CheckCircleFilled} color="success" size={20} />,
    align: 'center',
    width: 80,
  },
  {
    title: t('employees:modal.table.header.editor'),
    dataIndex: RoleResource.EDITOR,
    render: (_, permission) => (
      <Icon icon={CheckCircleFilled} color={permission.EDITOR ? 'success' : 'error'} size={20} />
    ),
    align: 'center',
    width: 80,
  },
  {
    title: t('employees:modal.table.header.viewer'),
    dataIndex: RoleResource.VIEWER,
    render: () => <Icon icon={CloseCircleFilled} color="error" size={20} />,
    align: 'center',
    width: 80,
  },
];

export const EmployeeRoles: React.FC = () => {
  const { t } = useTranslation();

  return (
    <InfoModal title={t('employees:modal.permisions.title')}>
      <Table
        bordered
        pagination={false}
        dataSource={getPermisions(t)}
        columns={getColumns(t)}
        rowKey={permision => permision.title}
        rowLabel={permision => permision.title}
        scroll={{ y: 300 }}
        size="small"
      />
    </InfoModal>
  );
};
