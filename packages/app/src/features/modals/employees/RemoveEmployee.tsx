import { Trans, useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { ConfirmationType, Confirmation } from '@cam/app/src/features/modals/Confirmation';
import { EmployeesModalProps } from '@cam/app/src/features/modals/employees/types';
import { removeEmployee } from '@cam/app/src/redux/Employees';
import { Paragraph, Text } from '@cam/atomic';

export const RemoveEmployee: React.FC<EmployeesModalProps> = ({ id, name }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handleRemove = () => {
    dispatch(removeEmployee(id));
  };

  return (
    <Confirmation
      type={ConfirmationType.Warning}
      title={t('employees:modal.remove.title')}
      action={handleRemove}
      actionText={t('employees:modal.remove.button')}
    >
      <Paragraph>
        <Text>
          <Trans t={t} i18nKey="employees:modal.remove.description" values={{ name }}>
            Your employee <strong>{{ name }}</strong> will be deleted and all employee data and
            permissions will be revoked.
          </Trans>
          &nbsp;
          <Text strong>{t('employees:modal.remove.warning')}</Text>
        </Text>
      </Paragraph>
      <Paragraph>{t('employees:modal.warning')}</Paragraph>
    </Confirmation>
  );
};
