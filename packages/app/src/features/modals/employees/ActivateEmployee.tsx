import { Trans, useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { ConfirmationType, Confirmation } from '@cam/app/src/features/modals/Confirmation';
import { EmployeesModalProps } from '@cam/app/src/features/modals/employees/types';
import { activateEmployee } from '@cam/app/src/redux/Employees/detail';
import { Paragraph } from '@cam/atomic';

export const ActivateEmployee: React.FC<EmployeesModalProps> = ({ name, id }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  return (
    <Confirmation
      type={ConfirmationType.Info}
      title={t('employees:modal.activate.title')}
      action={() => {
        dispatch(activateEmployee(id));
      }}
      actionText={t('employees:modal.activate.button')}
    >
      <Paragraph>
        <Trans t={t} i18nKey="employees:modal.activate.description" values={{ name }}>
          Your employee <strong>{{ name }}</strong> will be re-activated.
        </Trans>
      </Paragraph>
      <Paragraph>{t('employees:modal.warning')}</Paragraph>
    </Confirmation>
  );
};
