import { ActivateEmployee } from '@cam/app/src/features/modals/employees/ActivateEmployee';
import { EmployeeRoles } from '@cam/app/src/features/modals/employees/EmployeeRoles';
import { RemoveEmployee } from '@cam/app/src/features/modals/employees/RemoveEmployee';
import { RemoveInvitedEmployee } from '@cam/app/src/features/modals/employees/RemoveInvitedEmployee';
import { SuspendEmployee } from '@cam/app/src/features/modals/employees/SuspendEmployee';
import { ModalType } from '@cam/app/src/redux/Modals/types';

export const emplyoeesModalMap = {
  [ModalType.ActivateEmployee]: ActivateEmployee,
  [ModalType.RemoveInvitedEmployee]: RemoveInvitedEmployee,
  [ModalType.RemoveEmployee]: RemoveEmployee,
  [ModalType.SuspendEmployee]: SuspendEmployee,
  [ModalType.EmployeeRoles]: EmployeeRoles,
};
