import { ModalManager } from '@cam/app/src/features/modals/ModalManager';
import { modalMap } from '@cam/app/src/features/modals/modalMap';

export const Modals: React.FC = () => <ModalManager modalMap={modalMap} />;

// eslint-disable-next-line import/no-default-export
export default Modals;
