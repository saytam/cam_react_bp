export type CategoryModalProps = {
  name: string;
  id: string;
};
