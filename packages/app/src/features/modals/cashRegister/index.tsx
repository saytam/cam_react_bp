import { DeleteCategory } from '@cam/app/src/features/modals/cashRegister/DeleteCategory';
import { ModalType } from '@cam/app/src/redux/Modals/types';

export const cashRegisterModalMap = {
  [ModalType.DeleteCategory]: DeleteCategory,
};
