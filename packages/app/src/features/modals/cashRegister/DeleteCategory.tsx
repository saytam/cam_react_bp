import { Select, Space } from 'antd';
import { useState } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { MAX_SMALL_INPUT_WIDTH } from '@cam/app/src/features/events/components/form/utils';
import { CategoryModalProps } from '@cam/app/src/features/modals/cashRegister/types';
import { ConfirmationType, Confirmation } from '@cam/app/src/features/modals/Confirmation';
import { deleteCategory, getCategories } from '@cam/app/src/redux/CashRegister/categories';
import { Paragraph, Text } from '@cam/atomic';
import { DefaultCategories } from '@cam/firebase/resource/CashRegister';

export const DeleteCategory: React.FC<CategoryModalProps> = ({ id, name }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [categoryKey, setCategoryKey] = useState<string>(DefaultCategories.OTHER);
  const categories = useSelector(getCategories);

  const handleRemove = () => {
    dispatch(deleteCategory(id, categoryKey));
  };

  return (
    <Confirmation
      type={ConfirmationType.Warning}
      title={t('cashRegister:modal.deleteCategory.title')}
      action={handleRemove}
      actionText={t('cashRegister:modal.deleteCategory.button')}
    >
      <Paragraph>
        <Text>
          <Trans t={t} i18nKey="cashRegister:modal.deleteCategory.description" values={{ name }}>
            Your category <strong>{{ name }}</strong> will be deleted and all cash registry records
            having this category will be changed to the following:
          </Trans>
        </Text>
      </Paragraph>
      <Space direction="vertical">
        <Select
          onChange={value => setCategoryKey(value)}
          value={categoryKey}
          style={{ width: MAX_SMALL_INPUT_WIDTH }}
        >
          {categories.map(category => {
            if (category.key === id) {
              return null;
            }
            return (
              <Select.Option value={category.key} key={category.key} label={category.name}>
                {category.name}
              </Select.Option>
            );
          })}
        </Select>
        <Paragraph>{t('cashRegister:modal.warning')}</Paragraph>
      </Space>
    </Confirmation>
  );
};
