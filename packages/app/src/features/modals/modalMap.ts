import { cashRegisterModalMap } from '@cam/app/src/features/modals/cashRegister';
import { companyModalMap } from '@cam/app/src/features/modals/company';
import { customersModalMap } from '@cam/app/src/features/modals/customers';
import { emplyoeesModalMap } from '@cam/app/src/features/modals/employees';
import { eventsModalMap } from '@cam/app/src/features/modals/events';
import { usersModalMap } from '@cam/app/src/features/modals/users';

export const modalMap = {
  ...cashRegisterModalMap,
  ...companyModalMap,
  ...customersModalMap,
  ...eventsModalMap,
  ...usersModalMap,
  ...emplyoeesModalMap,
} as const;
