import { DeleteCompany } from '@cam/app/src/features/modals/company/DeleteCompany';
import { ModalType } from '@cam/app/src/redux/Modals/types';

export const companyModalMap = {
  [ModalType.DeleteCompany]: DeleteCompany,
};
