import { Trans, useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { CompanyModalProps } from '@cam/app/src/features/modals/company/types';
import { ConfirmationType, Confirmation } from '@cam/app/src/features/modals/Confirmation';
import { deleteCompany } from '@cam/app/src/redux/Company';
import { Paragraph, Text } from '@cam/atomic';

export const DeleteCompany: React.FC<CompanyModalProps> = ({ companyId, companyName }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handleRemove = () => {
    dispatch(deleteCompany(companyId));
  };

  return (
    <Confirmation
      type={ConfirmationType.Warning}
      title={t('company:modal.delete.title')}
      action={handleRemove}
      actionText={t('company:modal.delete.button')}
    >
      <Paragraph>
        <Text>
          <Trans t={t} i18nKey="company:modal.delete.description" values={{ companyName }}>
            Your company <strong>{{ companyName }}</strong> will be deleted. All employees will be
            dismissed and their data regarding this company will also perish. All events data will
            be deleted too.
          </Trans>
          &nbsp;
          <Text strong>{t('company:modal.delete.warning')}</Text>
        </Text>
      </Paragraph>
      <Paragraph>{t('company:modal.warning')}</Paragraph>
    </Confirmation>
  );
};
