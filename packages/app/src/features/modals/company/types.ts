export type CompanyModalProps = {
  companyName?: string;
  companyId: string;
};
