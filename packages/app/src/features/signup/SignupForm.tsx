import { Form } from 'antd';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { Layout } from '@cam/app/src/features/signup/components/Layout';
import { SignUpStep } from '@cam/app/src/features/signup/components/StepCard';
import { CredentialsForm } from '@cam/app/src/features/signup/CredentialsForm';
import { SignupForm, initialValues } from '@cam/app/src/features/signup/types';
import { createCompany } from '@cam/app/src/redux/Signup';
import { getIsCreatingCompany } from '@cam/app/src/redux/Signup/selectors';

export const SignUpForm: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const isCreatingCompany = useSelector(getIsCreatingCompany);
  const [form] = Form.useForm<SignupForm>();

  const handleSubmit = async () => {
    const { companyName, businessType, currency } = await form.validateFields();

    if (!!companyName && !!businessType) {
      dispatch(
        createCompany({
          name: companyName,
          businessType,
          currency,
        })
      );
    }
  };

  return (
    <Layout>
      <Form
        form={form}
        initialValues={initialValues}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        labelAlign="left"
        onFinish={handleSubmit}
      >
        <SignUpStep
          title={t('signup:credentials.card.title')}
          description={t('signup:credentials.card.description')}
          buttonText={t('signup:credentials.card.createCompany')}
          children={<CredentialsForm />}
          isLoading={isCreatingCompany}
        />
      </Form>
    </Layout>
  );
};
