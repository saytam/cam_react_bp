import { TFunction } from 'i18next';

import { BusinessType } from '@cam/firebase/resource/Company';

export const categoriesTranslations = (t: TFunction): Record<BusinessType, string> => ({
  BAR: t('signup:categories.bar'),
  CLUB: t('signup:categories.club'),
  OTHER: t('signup:categories.other'),
});
