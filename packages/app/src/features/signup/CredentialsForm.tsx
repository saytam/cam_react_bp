import { Space, Tooltip, Form, Input, Select } from 'antd';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { useRules } from '@cam/app/src/components/Form/rules';
import { logout } from '@cam/app/src/features/oauth/utils';
import { categoriesTranslations } from '@cam/app/src/features/signup/translations';
import { FORM_FIELDS } from '@cam/app/src/features/signup/types';
import { getUserInfo } from '@cam/app/src/redux/Auth/selectors';
import { HelpText, Text, TextLink } from '@cam/atomic';
import { Currency } from '@cam/firebase/resource/Common';
import { BusinessType } from '@cam/firebase/resource/Company';

export const CredentialsForm: React.FC = () => {
  const { t } = useTranslation();
  const categories = categoriesTranslations(t);
  const userInfo = useSelector(getUserInfo);
  const rules = useRules();
  return (
    <Space direction="vertical" size="large" style={{ padding: '0 32px' }}>
      <Space wrap>
        <Tooltip title={t('signup:credentials.tooltip.email')}>
          <Text size="lg" strong>
            {userInfo.email}
          </Text>
        </Tooltip>

        <TextLink onClick={logout}>{t('signup:credentials.cta.switchAccount')}</TextLink>
      </Space>

      <div style={{ textAlign: 'left' }}>
        <Form.Item
          label={
            <Tooltip placement="left" title={t('signup:credentials.tooltip.name')}>
              {t('signup:credentials.companyName.label')}
            </Tooltip>
          }
          rules={[rules.required, rules.max()]}
          name={FORM_FIELDS.COMPANY_NAME}
        >
          <Input placeholder={t('signup:credentials.companyName.placeholder')} />
        </Form.Item>

        <Form.Item
          label={
            <Tooltip placement="left" title={t('signup:credentials.tooltip.category')}>
              {t('signup:credentials.businessType.label')}
            </Tooltip>
          }
          name={FORM_FIELDS.CATEGORY}
          rules={[rules.required]}
        >
          <Select
            filterOption
            showSearch
            optionFilterProp="children"
            placeholder={t('signup:credentials.businessType.placeholder')}
          >
            {Object.values(BusinessType).map(category => (
              <Select.Option value={category} key={category}>
                {categories[category]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label={
            <HelpText help={t('signup:credentials.currency.label')}>
              {t('signup:credentials.currency.label')}
            </HelpText>
          }
          name="currency"
          rules={[rules.required]}
        >
          <Select>
            {Object.keys(Currency)?.map(currency => (
              <Select.Option value={currency} key={currency}>
                {currency}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </div>
    </Space>
  );
};
