import { Form, Space } from 'antd';
import styled from 'styled-components';

import { Title, Card, Button, Paragraph } from '@cam/atomic';

const StyledCard = styled(Card)`
  && {
    @media only screen and (min-width: 768px) {
      padding: 32px;
    }

    @media only screen and (min-width: 1024px) {
      padding: 32px 128px;
      width: 800px;
      .ant-card-body {
        padding: 0;
      }
    }

    border-radius: 0 24px 24px 24px;
  }
`;

export interface SignupStepProps {
  title: string;
  buttonText: string;
  children: React.ReactNode;
  description?: string;
  isLoading?: boolean;
  isVisible?: boolean;
}

export const SignUpStep: React.FC<SignupStepProps> = ({
  title,
  buttonText,
  description,
  isLoading,
  children,
}) => {
  return (
    <StyledCard>
      <Space direction="vertical" size="large">
        <div>
          <Title level={2} size="xxl" id="chooseYourConsole">
            {title}
          </Title>
          <Space direction="vertical" size="large">
            <Paragraph size="md">{description}</Paragraph>
            {children}
          </Space>
        </div>
        <Form.Item shouldUpdate noStyle>
          {({ isFieldsTouched }) => {
            return (
              <Button
                variant="primary"
                size="large"
                loading={isLoading}
                disabled={!isFieldsTouched() || isLoading}
                htmlType="submit"
              >
                {buttonText}
              </Button>
            );
          }}
        </Form.Item>
      </Space>
    </StyledCard>
  );
};
