import { Currency } from '@cam/firebase/resource/Common';
import { BusinessType } from '@cam/firebase/resource/Company';

export enum FORM_FIELDS {
  CATEGORY = 'businessType',
  COMPANY_NAME = 'companyName',
  CURRENCY = 'currency',
}

export interface SignupForm {
  [FORM_FIELDS.CATEGORY]?: BusinessType;
  [FORM_FIELDS.COMPANY_NAME]: string;
  [FORM_FIELDS.CURRENCY]: Currency;
}

export const initialValues: SignupForm = {
  businessType: BusinessType.BAR,
  companyName: '',
  currency: Currency.CZK,
};
