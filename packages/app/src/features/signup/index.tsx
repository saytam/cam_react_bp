import { SignUpForm } from '@cam/app/src/features/signup/SignupForm';

const Signup: React.FC = () => {
  return <SignUpForm />;
};

// eslint-disable-next-line import/no-default-export
export default Signup;
