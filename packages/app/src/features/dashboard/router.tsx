// import { useBreadcrumb } from '@fransvilhelm/react-breadcrumbs';
import { Router, Redirect } from '@reach/router';

import { Route } from '@cam/app/src/components/Route';
import { Dashboard } from '@cam/app/src/features/dashboard';
import { Loader } from '@cam/atomic';

export const DashboardRouter: React.FC = () => {
  //   const { t } = useTranslation();
  //   const dispatch = useDispatch();
  //   const { pathname: url } = useLocation();
  //   useBreadcrumb({ label: t('common:dashboard.title'), url });

  // TODO @psenicka: company initialized?
  const isInitialized = true;

  if (!isInitialized) {
    return <Loader />;
  }

  return (
    <Router primary={false}>
      <Route key="dashboard" path="/" component={<Dashboard />} />
      <Redirect key="redirect" from="*" to="/" noThrow />
    </Router>
  );
};
