import { Col, Row } from 'antd';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { PageTitle } from '@cam/app/src/components/PageTitle';
import { ReloadButton } from '@cam/app/src/components/ReloadButton';
import { getSelectedCompanyId } from '@cam/app/src/redux/Company/selectors';
import { fetchEmployees } from '@cam/app/src/redux/Employees';
import { getIsEmployeesLoading } from '@cam/app/src/redux/Employees/selectors';
import { fetchEventWidget, getIsLoading } from '@cam/app/src/redux/Events/eventWidget';

export const DashboardHeader: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const companyId = useSelector(getSelectedCompanyId);
  const isLoadingEvents = useSelector(getIsLoading);
  const isLoadingEmployees = useSelector(getIsEmployeesLoading);
  const isLoading = isLoadingEvents || isLoadingEmployees;

  const reloadDashboard = () => {
    dispatch(fetchEventWidget(companyId));
    dispatch(fetchEmployees());
  };

  return (
    <Row justify="space-between" align="middle" style={{ marginBottom: '20px' }}>
      <Col>
        <PageTitle>{t('dashboard:header.title')}</PageTitle>
      </Col>
      <Col>
        <ReloadButton isLoading={isLoading} fetchData={reloadDashboard} />
      </Col>
    </Row>
  );
};
