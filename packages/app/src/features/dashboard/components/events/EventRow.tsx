import { Row, Col } from 'antd';

import { Routes } from '@cam/app/src/constants/routes';
import { Name } from '@cam/app/src/features/events/components/Name';
import { formatDate } from '@cam/app/src/utils/date';
import { Tag } from '@cam/atomic';
import { EventList } from '@cam/firebase/resource/Event';

export const EventRow: React.FC<{
  event: EventList;
}> = ({ event }) => {
  const { date, name, productionManager, eventType, hosts, eventId } = event;

  return (
    <Row gutter={2} align="top" style={{ marginBottom: 8 }} key={eventId}>
      <Col xs={8}>
        <Name name={name} eventId={eventId} route={[Routes.EVENTS.BASE, eventId].join('/')} />
      </Col>
      <Col xs={5}>{formatDate(date)}</Col>
      <Col xs={3}>{eventType}</Col>
      <Col xs={3}>
        <Tag variant="default">{productionManager.displayName}</Tag>
      </Col>
      <Col xs={3}>{hosts}</Col>
      <Col xs={2} />
    </Row>
  );
};
