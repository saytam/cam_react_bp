import { OrderedListOutlined } from '@ant-design/icons';
import { Row, Col, Space } from 'antd';
import { memo, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { Routes } from '@cam/app/src/constants/routes';
import { EventRow } from '@cam/app/src/features/dashboard/components/events/EventRow';
import { getSelectedCompanyId } from '@cam/app/src/redux/Company/selectors';
import { eventWidgetSelectors, fetchEventWidget } from '@cam/app/src/redux/Events/eventWidget';
import { Button, WidgetHeader, Widget, Text } from '@cam/atomic';

export const EventsWidget: React.FC = memo(() => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const companyId = useSelector(getSelectedCompanyId);

  const events = useSelector(eventWidgetSelectors.selectAll);
  const isEmpty = events.length === 0;

  useEffect(() => {
    dispatch(fetchEventWidget(companyId));
  }, [dispatch, companyId]);

  return (
    <Widget
      icon={OrderedListOutlined}
      title={<WidgetHeader title={t('dashboard:widget.events.title')} value={events.length} />}
    >
      {isEmpty ? (
        <Row align="middle" justify="center" gutter={16} style={{ margin: '32px 0' }}>
          <Space direction="vertical">
            <Row align="middle" justify="center">
              <Text>{t('dashboard:widget.events.empty.text')}</Text>
            </Row>
            <Row align="middle" justify="center">
              <Button variant="primary" router={{ to: Routes.EVENTS.BASE }}>
                {t('dashboard:widget.events.empty.button')}
              </Button>
            </Row>
          </Space>
        </Row>
      ) : (
        <Row>
          <Col xs={24} style={{ marginBottom: 4 }}>
            {events.map(event => (
              <EventRow key={event.eventId} event={event} />
            ))}
          </Col>
        </Row>
      )}
    </Widget>
  );
});
