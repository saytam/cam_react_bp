import { PlusOutlined, UserOutlined } from '@ant-design/icons';
import { navigate } from '@reach/router';
import { Row, Col, Space } from 'antd';
import { ChartOptions } from 'chart.js';
import { memo, useEffect } from 'react';
import { Doughnut } from 'react-chartjs-2';
import { TFunction, useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { useTheme } from 'styled-components';

import { useRBAC, AccessScope } from '@cam/app/src/components/AccessController';
import { Routes } from '@cam/app/src/constants/routes';
import { fetchEmployees } from '@cam/app/src/redux/Employees';
import { getEmployees, getIsEmployeesInitialized } from '@cam/app/src/redux/Employees/selectors';
import { Button, WidgetHeader, Widget } from '@cam/atomic';
import { Employee, EmployeeStatus } from '@cam/firebase/resource/Employee';
import { StatusColors } from '@cam/theme/types';

const useOptions = (onClick: (key?: number) => void): ChartOptions<'doughnut'> => {
  const theme = useTheme();

  return {
    plugins: {
      legend: {
        position: 'left',
        align: 'start',
        labels: {
          usePointStyle: true,
          boxWidth: 6,
          padding: 12,
          font: { size: 14 },
          color: theme.colors.primary,
        },
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        onClick: (_, item) => onClick((item as any).index),
        onHover: event => {
          if (event.native?.target) {
            (event.native.target as HTMLElement).style.cursor = 'pointer';
          }
        },
        onLeave: event => {
          if (event.native?.target) {
            (event.native.target as HTMLElement).style.cursor = 'default';
          }
        },
      },
      tooltip: {
        callbacks: {
          label: item => item.label,
        },
      },
    },
    maintainAspectRatio: false,
    cutout: '70%',
  };
};

const prepareEmployeesData = (employees: Employee[]): Record<EmployeeStatus, number> => {
  const counts = employees.reduce(
    (acc, employee) => ({
      ...acc,
      [employee.status]: (acc[employee.status] || 0) + 1,
    }),
    {
      [EmployeeStatus.ACTIVE]: 0,
      [EmployeeStatus.INVITED]: 0,
      [EmployeeStatus.SUSPENDED]: 0,
    }
  );
  return counts;
};

const StatusColorMap: Record<EmployeeStatus, StatusColors> = {
  [EmployeeStatus.ACTIVE]: 'success',
  [EmployeeStatus.INVITED]: 'neutral',
  [EmployeeStatus.SUSPENDED]: 'warning',
};

const getStatusTranslations = (t: TFunction): Record<EmployeeStatus, string> => ({
  [EmployeeStatus.ACTIVE]: t('common:status.active'),
  [EmployeeStatus.INVITED]: t('common:status.invited'),
  [EmployeeStatus.SUSPENDED]: t('common:status.suspended'),
});

export const EmployeesWidget: React.FC = memo(() => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { hasWriteAccess: canAddDevices } = useRBAC(AccessScope.COMPANY);
  const theme = useTheme();
  const isEmployeesInitialized = useSelector(getIsEmployeesInitialized);

  useEffect(() => {
    !isEmployeesInitialized && dispatch(fetchEmployees());
  }, [dispatch, isEmployeesInitialized]);

  const employees = useSelector(getEmployees);
  const employeesData = prepareEmployeesData(employees);
  const allEmployeesCount = employees.length;
  const allStatuses = Object.keys(employeesData).map(key => getStatusTranslations(t)[key]);
  const graphData = Object.values(employeesData);
  const colors = Object.keys(employeesData).map(
    status => theme.colors.charts.status[StatusColorMap[status]]
  );
  const labels = graphData.map((count, i) => `${allStatuses[i]} ${count}`);

  const handleNavigate = (key = 0) => {
    navigate(Routes.EMPLOYEES.BASE);
  };

  const options = useOptions(handleNavigate);

  return (
    <Widget
      size="small"
      icon={UserOutlined}
      title={
        <WidgetHeader title={t('dashboard:widget.employees.title')} value={allEmployeesCount} />
      }
      extra={
        <Space align="start" size="small">
          {canAddDevices && (
            <Button
              variant="primary"
              ghost
              icon={<PlusOutlined />}
              size="small"
              router={{ to: [Routes.EMPLOYEES.BASE, Routes.EMPLOYEES.INVITE].join('/') }}
            >
              {t('dashboard:widget.employees.addButton')}
            </Button>
          )}
        </Space>
      }
    >
      <Row gutter={16}>
        <Col xs={24} style={{ maxHeight: '200px' }}>
          <Doughnut
            data={{
              labels,
              datasets: [
                {
                  data: graphData,
                  backgroundColor: colors,
                  hoverBackgroundColor: colors,
                  hoverBorderWidth: 6,
                  hoverBorderColor: theme.colors.neutral.light.hover,
                },
              ],
            }}
            options={options}
          />
        </Col>
      </Row>
    </Widget>
  );
});
