import { Col, Row } from 'antd';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { Body } from '@cam/app/src/components/Body';
import { EmployeesWidget } from '@cam/app/src/features/dashboard/components/EmployeesWidget';
import { EventsWidget } from '@cam/app/src/features/dashboard/components/events/EventsWidget';
import { DashboardHeader } from '@cam/app/src/features/dashboard/components/Header';
import { getIsLoading } from '@cam/app/src/redux/Events/eventWidget';
import { Spin } from '@cam/atomic';

export const Dashboard: React.FC = () => {
  const isLoadingEvents = useSelector(getIsLoading);
  const isLoading = isLoadingEvents;
  return (
    <Body header={<DashboardHeader />}>
      <Spin spinning={isLoading}>
        <Row gutter={[16, 16]}>
          <Col xs={24} md={14} xl={14} style={{ display: 'flex' }}>
            <EventsWidget />
          </Col>

          <Col xs={24} md={10} xl={10} style={{ display: 'flex' }}>
            <EmployeesWidget />
          </Col>
        </Row>
      </Spin>
    </Body>
  );
};
