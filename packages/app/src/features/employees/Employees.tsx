import { Body } from '@cam/app/src/components/Body';
import { EmployeesHeader } from '@cam/app/src/features/employees/components/Header';
import { EmployeesTable } from '@cam/app/src/features/employees/table/table';

export const Employees: React.FC = ({ children }) => {
  return (
    <>
      <Body header={<EmployeesHeader />}>
        <EmployeesTable />
      </Body>
      {children}
    </>
  );
};
