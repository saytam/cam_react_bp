import { PlusOutlined } from '@ant-design/icons';
import { Col, Row } from 'antd';
import { useTranslation } from 'react-i18next';

import { AccessController, AccessScope } from '@cam/app/src/components/AccessController';
import { PageTitle } from '@cam/app/src/components/PageTitle';
import { Routes } from '@cam/app/src/constants/routes';
import { Button } from '@cam/atomic';

export const EmployeesHeader: React.FC = () => {
  const { t } = useTranslation();

  return (
    <Row justify="space-between" align="middle" style={{ marginBottom: '20px' }}>
      <Col>
        <PageTitle>{t('employees:header.title')}</PageTitle>
      </Col>
      <AccessController scope={AccessScope.EMPLOYEE}>
        <Col>
          <Button
            size="small"
            icon={<PlusOutlined />}
            router={{ to: [Routes.EMPLOYEES.BASE, Routes.EMPLOYEES.INVITE].join('/') }}
          >
            {t('employees:ctaInvite')}
          </Button>
        </Col>
      </AccessController>
    </Row>
  );
};
