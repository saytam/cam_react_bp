import { useParams } from '@reach/router';
import { Switch, Tooltip } from 'antd';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { AccessController, AccessScope } from '@cam/app/src/components/AccessController';
import { showModal, ModalType } from '@cam/app/src/redux/Modals';

export const SwitchStatus: React.FC<{ isSuspended?: boolean; displayName?: string }> = ({
  isSuspended,
  displayName,
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { userId } = useParams<{ userId: string }>();

  const handleClick = () => {
    dispatch(
      showModal({
        type: isSuspended ? ModalType.ActivateEmployee : ModalType.SuspendEmployee,
        props: {
          name: displayName || '',
          id: userId,
        },
      })
    );
  };
  const title = isSuspended
    ? t('employees:detail.switch.activateTitle')
    : t('employees:detail.switch.suspendTitle');

  return (
    <AccessController scope={AccessScope.EMPLOYEE}>
      <Tooltip title={title}>
        <Switch size="small" checked={!isSuspended} onClick={handleClick} />
      </Tooltip>
    </AccessController>
  );
};
