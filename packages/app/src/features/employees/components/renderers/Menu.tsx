import { Menu } from 'antd';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { AccessController, AccessScope } from '@cam/app/src/components/AccessController';
import { MoreButton } from '@cam/app/src/components/TableIconButtons';
import { getUserId } from '@cam/app/src/redux/Auth/selectors';
import { showModal, ModalType } from '@cam/app/src/redux/Modals';
import { getA11yHandlers } from '@cam/app/src/utils/a11y';
import { Dropdown, Text } from '@cam/atomic';
import { EmployeeStatus } from '@cam/firebase/resource/Employee';

interface Props {
  name: string;
  id: string;
  isLocalTable?: boolean;
  status: EmployeeStatus;
}

export const MenuComponent: React.FC<Props> = ({ name, id, status, isLocalTable = false }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userId = useSelector(getUserId);
  const isUserInvited = status === EmployeeStatus.INVITED;
  const isSuspended = status === EmployeeStatus.SUSPENDED;

  const isDisabled = userId === id;

  const toggleSuspendActivate = () => {
    dispatch(
      showModal({
        type: isSuspended ? ModalType.ActivateEmployee : ModalType.SuspendEmployee,
        props: {
          name,
          id,
        },
      })
    );
  };

  const handleDelete = () => {
    dispatch(
      showModal({
        type: ModalType.RemoveEmployee,
        props: {
          name,
          id,
        },
      })
    );
  };

  const handleDeleteInvite = () => {
    dispatch(
      showModal({
        type: ModalType.RemoveInvitedEmployee,
        props: {
          name,
          id,
        },
      })
    );
  };

  const menu = () => (
    <Menu>
      {!isUserInvited && !isLocalTable && (
        <Menu.Item disabled={isDisabled} key="suspend" {...getA11yHandlers(toggleSuspendActivate)}>
          {isSuspended ? t('employees:table.menu.activate') : t('employees:table.menu.suspend')}
        </Menu.Item>
      )}
      {isUserInvited ? (
        <Menu.Item disabled={isDisabled} key="delete" {...getA11yHandlers(handleDeleteInvite)}>
          <Text type="danger" disabled={isDisabled}>
            {t('employees:table.menu.deleteInvite')}
          </Text>
        </Menu.Item>
      ) : (
        <Menu.Item disabled={isDisabled} key="delete" {...getA11yHandlers(handleDelete)}>
          <Text type="danger" disabled={isDisabled}>
            {t('employees:table.menu.delete')}
          </Text>
        </Menu.Item>
      )}
    </Menu>
  );

  return (
    <AccessController scope={AccessScope.EMPLOYEE}>
      <Dropdown overlay={menu()} trigger={['click']}>
        <MoreButton />
      </Dropdown>
    </AccessController>
  );
};
