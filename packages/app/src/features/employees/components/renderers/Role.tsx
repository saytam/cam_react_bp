import { UserOutlined } from '@ant-design/icons';
import { Space } from 'antd';
import { useTranslation } from 'react-i18next';

import { getRoleMap } from '@cam/app/src/utils/roles';
import { Icon } from '@cam/atomic';
import { RoleResource } from '@cam/firebase/resource/Company';

export const Role: React.FC<{ value: RoleResource }> = ({ value }) => {
  const { t } = useTranslation();

  return (
    <Space>
      <Icon icon={UserOutlined} />
      {getRoleMap(t)[value]}
    </Space>
  );
};
