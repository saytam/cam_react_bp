import { Badge } from 'antd';
import { PresetStatusColorType } from 'antd/es/_util/colors';
import { TFunction } from 'i18next';
import { useTranslation } from 'react-i18next';

import { Text } from '@cam/atomic';
import { EmployeeStatus } from '@cam/firebase/resource/Employee';

interface Props {
  value: EmployeeStatus;
}

const getStatusMap = (
  t: TFunction
): Record<EmployeeStatus, { color: PresetStatusColorType; text: string }> => ({
  ACTIVE: { color: 'success', text: t('common:status.active') },
  INVITED: { color: 'default', text: t('common:status.invited') },
  SUSPENDED: { color: 'error', text: t('common:status.suspended') },
});

export const Status: React.FC<Props> = ({ value }) => {
  const { t } = useTranslation();
  const { color, text } = getStatusMap(t)[value];
  return (
    <Text>
      <Badge status={color} text={text} />
    </Text>
  );
};
