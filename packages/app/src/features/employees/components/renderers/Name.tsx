import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { getUserId } from '@cam/app/src/redux/Auth/selectors';
import { setUserProfileVisible } from '@cam/app/src/redux/Users/profile';
import { TextLink, NavLink } from '@cam/atomic';
import { EmployeeStatus } from '@cam/firebase/resource/Employee';

interface Props {
  userId: string;
  value: string;
  status: EmployeeStatus;
}

const DisabledLink = styled(NavLink).attrs({ to: '' })`
  cursor: not-allowed;
`;

export const Name: React.FC<Props> = ({ userId, value, status }) => {
  const dispatch = useDispatch();
  const loggedUserId = useSelector(getUserId);
  const isLoggedUserSelected = loggedUserId === userId;

  const isDisabled = status === EmployeeStatus.INVITED;

  if (isLoggedUserSelected) {
    return <TextLink onClick={() => dispatch(setUserProfileVisible(true))}>{value}</TextLink>;
  }
  return isDisabled ? <DisabledLink>{value}</DisabledLink> : <NavLink to={userId}>{value}</NavLink>;
};
