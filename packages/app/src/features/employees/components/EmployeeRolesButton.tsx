import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { showModal, ModalType } from '@cam/app/src/redux/Modals';
import { TextLink } from '@cam/atomic';

export const EmployeeRolesButton: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const onClick = () => {
    dispatch(showModal({ type: ModalType.EmployeeRoles, props: {} }));
  };

  return <TextLink onClick={onClick}>{t('employees:invite.form.ctaButton')}</TextLink>;
};
