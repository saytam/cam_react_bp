import { useNavigate } from '@reach/router';
import { Form } from 'antd';
import { useDispatch, useSelector } from 'react-redux';

import { AccessController, AccessScope } from '@cam/app/src/components/AccessController';
import { DirtProvider } from '@cam/app/src/components/Form';
import { SidePanel } from '@cam/app/src/components/SidePanel';
import { Footer } from '@cam/app/src/features/employees/invite/Footer';
import { InviteForm } from '@cam/app/src/features/employees/invite/form';
import { InviteFormProps } from '@cam/app/src/features/employees/invite/form/types';
import { InviteTitle } from '@cam/app/src/features/employees/invite/Title';
import { inviteEmployee } from '@cam/app/src/redux/Employees/invites';
import { getIsInvitingCompany } from '@cam/app/src/redux/Employees/selectors';
import { removeWhiteSpacesFromArray } from '@cam/app/src/utils/string';
import { Spin } from '@cam/atomic';

export const EmployeeInvite: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const isInviting = useSelector(getIsInvitingCompany);

  const [form] = Form.useForm<InviteFormProps>();

  const handleInviteCompany = async () => {
    try {
      const { email, role, job } = await form.validateFields();
      const emails = removeWhiteSpacesFromArray(email);
      dispatch(inviteEmployee({ emails, role, job }));
    } catch {}
  };

  const handleClose = () => navigate('./');

  return (
    <DirtProvider>
      <AccessController scope={AccessScope.EMPLOYEE}>
        <SidePanel
          onClose={handleClose}
          title={<InviteTitle />}
          footer={<Footer onClick={handleInviteCompany} isLoading={isInviting} />}
        >
          <Spin spinning={isInviting}>
            <InviteForm form={form} name="companyInviteForm" />
          </Spin>
        </SidePanel>
      </AccessController>
    </DirtProvider>
  );
};
