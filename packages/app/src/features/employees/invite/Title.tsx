import { UserAddOutlined } from '@ant-design/icons';
import { Space } from 'antd';
import { useTranslation } from 'react-i18next';

import { Icon } from '@cam/atomic';

export const InviteTitle: React.FC = () => {
  const { t } = useTranslation();

  return (
    <Space>
      <Icon icon={UserAddOutlined} />
      {t('employees:invite.title')}
    </Space>
  );
};
