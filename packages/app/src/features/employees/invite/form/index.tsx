import { Space, Form, Select, Row, Col, Input } from 'antd';
import { useTranslation } from 'react-i18next';

import { FormProps } from '@cam/app/src/components/Form';
import { useRules } from '@cam/app/src/components/Form/rules';
import { EmployeeRolesButton } from '@cam/app/src/features/employees/components/EmployeeRolesButton';
import { FORM_FIELDS, InviteFormProps } from '@cam/app/src/features/employees/invite/form/types';
import { Card, Radio } from '@cam/atomic';
import { RoleResource } from '@cam/firebase/resource/Company';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

export const InviteForm: React.FC<FormProps<InviteFormProps>> = ({ form, name: formName }) => {
  const { t } = useTranslation();
  const rules = useRules();

  const initialValues: InviteFormProps = {
    email: [],
    role: RoleResource.EDITOR,
  };

  return (
    <Form
      initialValues={initialValues}
      colon={false}
      form={form}
      {...layout}
      labelAlign="left"
      name={formName}
    >
      <Space direction="vertical" style={{ width: '100%' }}>
        <Card title={t('employees:invite.form.detail.title')} type="inner">
          <Form.Item
            style={{ margin: '10px 0' }}
            label={t('employees:invite.form.email.label')}
            name={FORM_FIELDS.EMAIL}
            rules={[
              rules.array({
                required: true,
                type: 'email',
                empty: t('common:validation.email'),
                invalid: t('common:validation.email'),
              }),
            ]}
          >
            <Select
              mode="tags"
              tokenSeparators={[',']}
              dropdownStyle={{ visibility: 'hidden' }}
              aria-required="true"
            />
          </Form.Item>
          <Form.Item
            label={t('employees:invite.form.job.label')}
            name={FORM_FIELDS.JOB}
            rules={[rules.max(20)]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            style={{ margin: '10px 0' }}
            name={FORM_FIELDS.ROLE}
            label={t('employees:invite.table.header.role')}
            rules={[rules.required]}
          >
            <Radio.Group name={FORM_FIELDS.ROLE}>
              <Radio.Vertical value={RoleResource.ADMIN}>
                {t('employees:table.role.admin')}
              </Radio.Vertical>
              <Radio.Vertical value={RoleResource.EDITOR}>
                {t('employees:table.role.editor')}
              </Radio.Vertical>
              <Radio.Vertical value={RoleResource.VIEWER}>
                {t('employees:table.role.viewer')}
              </Radio.Vertical>
            </Radio.Group>
          </Form.Item>
          <Row>
            <Col offset={6}>
              <EmployeeRolesButton />
            </Col>
          </Row>
        </Card>
      </Space>
    </Form>
  );
};
