import { RoleResource } from '@cam/firebase/resource/Company';

export enum FORM_FIELDS {
  EMAIL = 'email',
  ROLE = 'role',
  JOB = 'job',
}

export interface InviteFormProps {
  [FORM_FIELDS.EMAIL]: string[];
  [FORM_FIELDS.ROLE]: RoleResource;
  [FORM_FIELDS.JOB]?: string;
}
