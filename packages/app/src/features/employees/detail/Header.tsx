import { UserOutlined } from '@ant-design/icons';
import { useParams } from '@reach/router';
import { Space } from 'antd';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { getEmployeeDetailFormData } from '@cam/app/src/redux/Employees/selectors';
import { RootState } from '@cam/app/src/redux/reducer';
import { getRoleMap } from '@cam/app/src/utils/roles';
import { Icon } from '@cam/atomic';

export const DetailHeader: React.FC = () => {
  const { t } = useTranslation();
  const { userId } = useParams<{ userId: string }>();
  const { displayName, role } = useSelector((state: RootState) =>
    getEmployeeDetailFormData(state, userId)
  );
  const secondPart = role && getRoleMap(t)[role];

  return (
    <Space>
      <Icon icon={UserOutlined} />
      <span>
        {[displayName || t('common:userMenu.noName'), secondPart].filter(Boolean).join(' - ')}
      </span>
    </Space>
  );
};
