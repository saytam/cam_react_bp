import { useNavigate, useParams } from '@reach/router';
import { Form } from 'antd';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { AccessController, AccessScope } from '@cam/app/src/components/AccessController';
import { ExternalSubmitButton, DirtProvider } from '@cam/app/src/components/Form';
import { SidePanel } from '@cam/app/src/components/SidePanel';
import { EmployeeForm } from '@cam/app/src/features/employees/detail/form';
import { EmployeeProps } from '@cam/app/src/features/employees/detail/form/types';
import { DetailHeader } from '@cam/app/src/features/employees/detail/Header';
import { fetchEmployeeDetail, updateEmployee } from '@cam/app/src/redux/Employees/detail';
import { getIsLoadingDetail, getIsUpdatingDetail } from '@cam/app/src/redux/Employees/selectors';
import { Spin } from '@cam/atomic';

export const EmployeeDetail: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { userId } = useParams<{ userId: string }>();
  const navigate = useNavigate();

  const isLoading = useSelector(getIsLoadingDetail);
  const isUpdating = useSelector(getIsUpdatingDetail);

  const [form] = Form.useForm<EmployeeProps>();

  const handleClose = () => navigate('./');

  useEffect(() => {
    dispatch(fetchEmployeeDetail(userId));
  }, [dispatch, userId]);

  const handleSave = async () => {
    try {
      const values = await form.validateFields();
      dispatch(updateEmployee(userId, values));
    } catch {}
  };

  return (
    <DirtProvider>
      <SidePanel
        title={<DetailHeader />}
        onClose={handleClose}
        footer={
          <AccessController scope={AccessScope.EMPLOYEE}>
            <ExternalSubmitButton loading={isUpdating} onClick={handleSave}>
              {t('common:saveBtn')}
            </ExternalSubmitButton>
          </AccessController>
        }
      >
        <Spin spinning={isLoading || isUpdating}>
          <EmployeeForm name="employeeForm" form={form} />
        </Spin>
      </SidePanel>
    </DirtProvider>
  );
};
