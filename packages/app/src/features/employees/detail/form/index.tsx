import { useParams } from '@reach/router';
import { Space, Form, Input, Row, Col } from 'antd';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { AccessController, AccessScope } from '@cam/app/src/components/AccessController';
import { FormProps } from '@cam/app/src/components/Form';
import { EmployeeRolesButton } from '@cam/app/src/features/employees/components/EmployeeRolesButton';
import { Status } from '@cam/app/src/features/employees/components/renderers/Status';
import { SwitchStatus } from '@cam/app/src/features/employees/components/SwitchStatus';
import { DeleteEmployeeButton } from '@cam/app/src/features/employees/detail/DeleteButton';
import { Job, Salary } from '@cam/app/src/features/employees/detail/form/components';
import { FORM_FIELDS, EmployeeProps } from '@cam/app/src/features/employees/detail/form/types';
import { StyledFormItem } from '@cam/app/src/features/employees/styles';
import { getEmployeeDetailFormData } from '@cam/app/src/redux/Employees/selectors';
import { showModal, ModalType } from '@cam/app/src/redux/Modals';
import { RootState } from '@cam/app/src/redux/reducer';
import { getInfoRolesMap } from '@cam/app/src/utils/roles';
import { Card, Radio } from '@cam/atomic';
import { RoleResource } from '@cam/firebase/resource/Company';
import { EmployeeStatus } from '@cam/firebase/resource/Employee';

export const RoleType: React.FC<{ disabled?: boolean }> = ({ disabled = false }) => {
  const { t } = useTranslation();

  return (
    <StyledFormItem label={t('employees:detail.employee.typeLabel')} name={FORM_FIELDS.ROLE}>
      <Radio.Group disabled={disabled} name={FORM_FIELDS.ROLE}>
        <Radio.Vertical value={RoleResource.ADMIN}>
          {t('employees:table.role.admin')}
        </Radio.Vertical>
        <Radio.Vertical value={RoleResource.EDITOR}>
          {t('employees:table.role.editor')}
        </Radio.Vertical>
        <Radio.Vertical value={RoleResource.VIEWER}>
          {t('employees:table.role.viewer')}
        </Radio.Vertical>
      </Radio.Group>
    </StyledFormItem>
  );
};

export const EmployeeForm: React.FC<FormProps<EmployeeProps>> = ({ form, name: formName }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { userId } = useParams<{ userId: string }>();

  const { displayName, role, email, isSuspended, job, salary } = useSelector((state: RootState) =>
    getEmployeeDetailFormData(state, userId)
  );

  const initialValues: EmployeeProps = {
    displayName: displayName || '',
    role: role ? getInfoRolesMap()[role] : RoleResource.ADMIN,
    job,
    salary,
  };

  useEffect(() => {
    form.resetFields();
  }, [displayName, form, role, job, salary]);

  const handleDelete = () =>
    dispatch(
      showModal({
        type: ModalType.RemoveEmployee,
        props: {
          name: displayName || '',
          id: userId,
        },
      })
    );

  return (
    <Form
      name={formName}
      form={form}
      initialValues={initialValues}
      colon={false}
      labelCol={{ span: 5 }}
      wrapperCol={{ span: 19 }}
      labelAlign="left"
    >
      <Space direction="vertical" style={{ width: '100%' }}>
        <DeleteEmployeeButton onClick={handleDelete} />
        <Card title={t('employees:detail.employee.title')} type="inner">
          <StyledFormItem
            label={t('employees:detail.employee.displayNameLabel')}
            name={FORM_FIELDS.DISPLAY_NAME}
          >
            <Input disabled />
          </StyledFormItem>
          <AccessController scope={AccessScope.EMPLOYEE} readOnly={<Job disabled />}>
            <Job />
          </AccessController>
          <AccessController scope={AccessScope.SALARY} readOnly={<Salary disabled />}>
            <Salary />
          </AccessController>
          <StyledFormItem label={t('employees:detail.employee.emailLabel')}>{email}</StyledFormItem>
          <>
            <AccessController scope={AccessScope.EMPLOYEE} readOnly={<RoleType disabled />}>
              <RoleType />
            </AccessController>

            <Row>
              <Col offset={5}>
                <EmployeeRolesButton />
              </Col>
            </Row>
          </>
          <StyledFormItem label={t('employees:detail.employee.statusLabel')}>
            <Space direction="horizontal">
              <Status value={isSuspended ? EmployeeStatus.SUSPENDED : EmployeeStatus.ACTIVE} />
              <SwitchStatus isSuspended={isSuspended} displayName={displayName || ''} />
            </Space>
          </StyledFormItem>
        </Card>
      </Space>
    </Form>
  );
};
