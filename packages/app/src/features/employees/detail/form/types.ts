import { RoleResource, SalaryResource } from '@cam/firebase/resource/Company';

export enum FORM_FIELDS {
  DISPLAY_NAME = 'displayName',
  ROLE = 'role',
  JOB = 'job',
  SALARY = 'salary',
}

export interface EmployeeProps {
  [FORM_FIELDS.DISPLAY_NAME]?: string;
  [FORM_FIELDS.ROLE]: RoleResource;
  [FORM_FIELDS.JOB]?: string;
  [FORM_FIELDS.SALARY]?: SalaryResource;
}
