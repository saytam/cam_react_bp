import { Input } from 'antd';
import { useTranslation } from 'react-i18next';

import { useRules } from '@cam/app/src/components/Form/rules';
import { Salary as SalaryComponent } from '@cam/app/src/components/Salary';
import { FORM_FIELDS } from '@cam/app/src/features/employees/detail/form/types';
import { StyledFormItem } from '@cam/app/src/features/employees/styles';

export const Job: React.FC<{ disabled?: boolean }> = ({ disabled = false }) => {
  const { t } = useTranslation();
  const rules = useRules();

  return (
    <StyledFormItem
      label={t('employees:detail.employee.job')}
      name={FORM_FIELDS.JOB}
      rules={[rules.max(40)]}
    >
      <Input disabled={disabled} name={FORM_FIELDS.JOB} />
    </StyledFormItem>
  );
};

export const Salary: React.FC<{ disabled?: boolean }> = ({ disabled = false }) => {
  const { t } = useTranslation();
  return (
    <StyledFormItem label={t('employees:detail.employee.salary')} name={FORM_FIELDS.SALARY}>
      <SalaryComponent disabled={disabled} />
    </StyledFormItem>
  );
};
