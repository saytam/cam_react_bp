import { UserDeleteOutlined } from '@ant-design/icons';
import { Row } from 'antd';
import { useTranslation } from 'react-i18next';

import { AccessController, AccessScope } from '@cam/app/src/components/AccessController';
import { Button } from '@cam/atomic';

export const DeleteEmployeeButton: React.FC<{ onClick: () => void }> = ({ onClick }) => {
  const { t } = useTranslation();

  return (
    <AccessController scope={AccessScope.EMPLOYEE}>
      <Row justify="end">
        <Button variant="text" size="small" onClick={onClick} icon={<UserDeleteOutlined />}>
          {t('employees:detail.employee.deleteButton')}
        </Button>
      </Row>
    </AccessController>
  );
};
