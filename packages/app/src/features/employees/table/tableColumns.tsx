import { TFunction } from 'i18next';

import { MenuComponent as MenuRenderer } from '@cam/app/src/features/employees/components/renderers/Menu';
import { Name as NameRenderer } from '@cam/app/src/features/employees/components/renderers/Name';
import { Role as RoleRenderer } from '@cam/app/src/features/employees/components/renderers/Role';
import { Status as StatusRenderer } from '@cam/app/src/features/employees/components/renderers/Status';
import { TypedColumnProps } from '@cam/app/src/utils/antd';
import { createActionsColumn, textSorter } from '@cam/app/src/utils/table';
import { Text } from '@cam/atomic';
import { Employee, EmployeeStatus } from '@cam/firebase/resource/Employee';

export const createColumns = (t: TFunction): TypedColumnProps<Employee>[] => [
  {
    title: t('employees:table.header.employee'),
    dataIndex: 'displayName',
    width: 140,
    render: (_, employee) => (
      <NameRenderer
        value={employee.displayName || employee.email || ''}
        userId={employee.userId}
        status={employee.status || EmployeeStatus.SUSPENDED}
      />
    ),
    sorter: textSorter('displayName'),
  },
  {
    title: t('employees:table.header.email'),
    dataIndex: 'email',
    width: 140,
    sorter: textSorter('email'),
  },
  {
    title: t('employees:table.header.employeeRole'),
    dataIndex: 'role',
    width: 120,
    render: (_, employee) => <RoleRenderer value={employee.role} />,
    sorter: textSorter('role'),
  },
  {
    title: t('employees:table.header.employeeJob'),
    dataIndex: 'job',
    width: 100,
    render: (_, employee) => employee.job && <Text>{employee.job}</Text>,
    sorter: textSorter('job'),
  },
  {
    title: t('employees:table.header.status'),
    dataIndex: 'status',
    width: 140,
    render: (_, employee) => <StatusRenderer value={employee.status} />,
    sorter: textSorter('status'),
  },
  createActionsColumn((_, employee) => (
    <MenuRenderer
      name={employee.displayName || employee.email || ''}
      id={employee.userId}
      status={employee.status}
    />
  )),
];
