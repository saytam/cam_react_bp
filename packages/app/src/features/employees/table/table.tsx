import { Row } from 'antd';
import { useMemo, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { ReloadButton } from '@cam/app/src/components/ReloadButton';
import { TABLE_SIZE } from '@cam/app/src/constants/tableSize';
import { createColumns } from '@cam/app/src/features/employees/table/tableColumns';
import { getSelectedCompanyId } from '@cam/app/src/redux/Company/selectors';
import { fetchEmployees } from '@cam/app/src/redux/Employees';
import { getEmployees, selectors } from '@cam/app/src/redux/Employees/selectors';
import { Table } from '@cam/atomic';

export const EmployeesTable: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const employees = useSelector(getEmployees);
  const { isLoading } = useSelector(selectors.getTableState);
  const companyId = useSelector(getSelectedCompanyId);

  useEffect(() => {
    dispatch(fetchEmployees());
  }, [dispatch, companyId]);

  return useMemo(
    () => (
      <>
        <Row justify="end" style={{ alignItems: 'baseline' }}>
          <ReloadButton
            fetchData={() => {
              dispatch(fetchEmployees());
            }}
            isLoading={isLoading}
          />
        </Row>
        <Table
          loading={isLoading}
          dataSource={employees}
          columns={createColumns(t)}
          rowKey={record => record.userId}
          size={TABLE_SIZE}
        />
      </>
    ),
    [isLoading, employees, t, dispatch]
  );
};
