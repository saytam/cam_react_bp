import { TFunction } from 'i18next';

import { createMessage } from '@cam/app/src/features/notifications/defaults';

export const messages = (t: TFunction) => ({
  createCustomer: createMessage({
    success: t('customers:messages.create.success') as string,
    failure: t('customers:messages.create.failure') as string,
  }),
  updateCustomer: createMessage({
    success: t('customers:messages.update.success') as string,
    failure: t('customers:messages.update.failure') as string,
  }),
  removeCustomer: createMessage({
    success: t('customers:messages.delete.success') as string,
    failure: t('customers:messages.delete.failure') as string,
  }),
});
