import { MessageArgsProps } from 'antd';

import { showFailure, showSuccess } from '@cam/app/src/features/notifications/defaults';

export type MessageProps = Omit<Partial<MessageArgsProps>, 'content'>;

export type SuccessMessage<SuccessContext> = { success: GetSuccessType<SuccessContext> };
export type FailureMessage<FailureContext> = { failure: GetFailureType<FailureContext> };

export type SuccessArgs<SuccessContext> =
  | React.ReactNode
  | ((context?: SuccessContext) => React.ReactNode | undefined);

export type FailureArgs<FailureContext> =
  | React.ReactNode
  | ((context?: FailureContext) => React.ReactNode | undefined);

/* TS hacks to infer return type of a generic function */
class SuccessContextWrapper<SuccessContext> {
  wrapper = () => showSuccess<SuccessContext>();
}
type GetSuccessType<T> = ReturnType<SuccessContextWrapper<T>['wrapper']>;

class FailureContextWrapper<FailureContext> {
  wrapper = () => showFailure<FailureContext>();
}
type GetFailureType<T> = ReturnType<FailureContextWrapper<T>['wrapper']>;
