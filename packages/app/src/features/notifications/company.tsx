import { TFunction } from 'i18next';

import { createMessage } from '@cam/app/src/features/notifications/defaults';

export const messages = (t: TFunction) => ({
  createCompany: createMessage({
    success: t('company:messages.create.success') as string,
    failure: t('company:messages.create.failure') as string,
  }),
  updateCompany: createMessage({
    success: t('company:messages.update.success') as string,
    failure: t('company:messages.update.failure') as string,
  }),
  deleteCompany: createMessage({
    success: t('company:messages.delete.success') as string,
    failure: t('company:messages.delete.failure') as string,
  }),
});
