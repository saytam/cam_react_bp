import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  CompanySelect as CompanySelectComponent,
  renderCompanySelectOption,
} from '@cam/app/src/components/CompanySelect';
import { Routes } from '@cam/app/src/constants/routes';
import { setSiderCollapsed } from '@cam/app/src/redux/App';
import { isSiderCollapsed } from '@cam/app/src/redux/App/selectors';
import { getUserId } from '@cam/app/src/redux/Auth/selectors';
import { fetchCompaniesList, setSelectedCompanyId } from '@cam/app/src/redux/Company';
import {
  getIsFetchingCompanies,
  getIsFetchingCompaniesError,
  getSelectedCompanyId,
  selectAll,
} from '@cam/app/src/redux/Company/selectors';

export const CompanySelect: React.FC<{ maxHeight?: number }> = ({ maxHeight }) => {
  const dispatch = useDispatch();
  const isCollapsed = useSelector(isSiderCollapsed);
  const isLoading = useSelector(getIsFetchingCompanies);
  const hasError = useSelector(getIsFetchingCompaniesError);
  const companyId = useSelector(getSelectedCompanyId);
  const userId = useSelector(getUserId);
  const companyList = useSelector(selectAll);

  const handleSetSelectedCompany = (selectedCompanyId: string) => {
    if (companyId === selectedCompanyId) return;
    dispatch(setSelectedCompanyId(selectedCompanyId));
  };

  const handleReload = useCallback(() => {
    dispatch(fetchCompaniesList(userId));
  }, [dispatch, userId]);

  const companyOptions = companyList.map(company =>
    renderCompanySelectOption({
      name: company.companyName,
      id: company.companyId,
      key: company.companyId,
    })
  );

  return (
    <CompanySelectComponent
      createCompanyRoute={[Routes.DASHBOARD.BASE, Routes.DASHBOARD.CREATE].join('/')}
      companyOptions={companyOptions}
      isCollapsed={isCollapsed}
      isFetchingError={hasError}
      isLoading={isLoading}
      onClickReload={handleReload}
      onCollapseChange={collapsed => dispatch(setSiderCollapsed(collapsed))}
      onSelect={handleSetSelectedCompany}
      selectedCompanyId={companyId}
      maxHeight={maxHeight}
    />
  );
};
