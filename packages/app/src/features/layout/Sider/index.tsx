import { useLocation, useNavigate } from '@reach/router';
import { useCallback } from 'react';
import Measure from 'react-measure';
import { useSelector, useDispatch } from 'react-redux';

import { SideMenu } from '@cam/app/src/components/SideMenu/SideMenu';
import { Routes } from '@cam/app/src/constants/routes';
import { CompanySelect } from '@cam/app/src/features/layout/Sider/CompanySelect';
import { useMenuItems } from '@cam/app/src/features/layout/Sider/menuItems';
import { toggleSiderCollapsed } from '@cam/app/src/redux/App';
import { isSiderCollapsed } from '@cam/app/src/redux/App/selectors';

export const Sider: React.FC = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const [, basePath] = pathname.split('/');
  const selectedKey = `/${basePath}`;

  const isCollapsed = useSelector(isSiderCollapsed);
  const menuItems = useMenuItems();

  const handleClickLogo = useCallback(() => {
    navigate(Routes.DASHBOARD.BASE);
  }, [navigate]);

  const handleCollapse = useCallback(() => {
    dispatch(toggleSiderCollapsed());
  }, [dispatch]);

  return (
    <Measure bounds>
      {({ measureRef, contentRect }) => (
        <SideMenu
          isCollapsed={isCollapsed}
          onCollapse={handleCollapse}
          onClickLogo={handleClickLogo}
          companySelect={<CompanySelect maxHeight={(contentRect?.bounds?.height || 0) - 176} />}
          menuItems={menuItems}
          selectedKey={selectedKey}
          {...{ ref: measureRef }}
        />
      )}
    </Measure>
  );
};
