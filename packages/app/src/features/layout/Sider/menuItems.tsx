import {
  GoldOutlined,
  HomeOutlined,
  OrderedListOutlined,
  SolutionOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { TFunction } from 'i18next';
import { useTranslation } from 'react-i18next';

import { MenuItemProps } from '@cam/app/src/components/SideMenu/SideMenu';
import { Routes } from '@cam/app/src/constants/routes';
import { Icon } from '@cam/atomic';

const getDefaultMenuItems = ({ t }: { t: TFunction }): MenuItemProps[] => [
  {
    icon: <Icon icon={HomeOutlined} />,
    label: t('common:dashboard.title'),
    link: Routes.DASHBOARD.BASE,
  },
  {
    icon: <Icon icon={OrderedListOutlined} />,
    label: t('common:events.title'),
    link: Routes.EVENTS.BASE,
  },
  {
    icon: <Icon icon={GoldOutlined} />,
    label: t('common:cashRegister.title'),
    link: Routes.CASH_REGISTER.BASE,
  },
  {
    icon: <Icon icon={UserOutlined} />,
    label: t('common:employees.title'),
    link: Routes.EMPLOYEES.BASE,
  },
  {
    icon: <SolutionOutlined />,
    label: t('common:company.title'),
    link: Routes.COMPANY.BASE,
  },
];

export const useMenuItems = () => {
  const { t } = useTranslation();
  return getDefaultMenuItems({
    t,
  }).filter(item => item.enabled !== false);
};
