import { Layout } from 'antd';
import { Suspense, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { Sider } from '@cam/app/src/features/layout/Sider';
import { Topbar } from '@cam/app/src/features/layout/Topbar';
import { UserProfile } from '@cam/app/src/features/users/profile';
import { getIsInitialized as getIsAppInitialized } from '@cam/app/src/redux/App/selectors';
import { getUserId } from '@cam/app/src/redux/Auth/selectors';
import { fetchAccessRights } from '@cam/app/src/redux/Company';
import { getSelectedCompanyId } from '@cam/app/src/redux/Company/selectors';
import { Loader } from '@cam/atomic';

const Content = styled(Layout.Content)`
  overflow-y: auto;
`;

const useCompanyData = () => {
  const dispatch = useDispatch();
  const userId = useSelector(getUserId);
  const companyId = useSelector(getSelectedCompanyId);

  useEffect(() => {
    if (companyId) {
      dispatch(fetchAccessRights(userId, companyId));
    }
  }, [dispatch, companyId, userId]);
};

const useAppInit = () => {
  const isInitialized = getIsAppInitialized;

  useCompanyData();
  return { isInitialized };
};

export const DefaultLayout: React.FC = ({ children }) => {
  const { isInitialized } = useAppInit();

  return (
    <Layout>
      <Sider />
      <Layout>
        <Topbar />
        <Content id="mainContent">
          <Suspense fallback={<Loader />}>{!isInitialized ? <Loader /> : children}</Suspense>
          <UserProfile />
        </Content>
      </Layout>
    </Layout>
  );
};
