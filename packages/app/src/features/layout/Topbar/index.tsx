import { Topbar as TopbarComponent } from '@cam/app/src/components/Topbar';
import { UserDropdown } from '@cam/app/src/features/layout/Topbar/UserDropdown';
import { Text } from '@cam/atomic';

// import { Breadcrumbs } from '@cam/app/src/features/layout/Topbar/Breadcrumbs';

export const Topbar: React.FC = () => {
  const menuItems = [<UserDropdown key="user-profile" />].filter(Boolean);

  return <TopbarComponent breadcrumbs={<Text theme="dark"></Text>} menuItems={menuItems} />;
};
