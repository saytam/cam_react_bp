import { ProfileOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { TopbarMenuItem, UserDropdown as UserDropdownUI } from '@cam/app/src/components/Topbar';
import { getUserInfo } from '@cam/app/src/redux/Auth/selectors';
import { getSelectedCompany } from '@cam/app/src/redux/Company/selectors';
import { setUserProfileVisible } from '@cam/app/src/redux/Users/profile';
import { getA11yHandlers } from '@cam/app/src/utils/a11y';
import { getRoleMap } from '@cam/app/src/utils/roles';
import { RoleResource } from '@cam/firebase/resource/Company';

export const UserDropdown: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userName = useSelector(getUserInfo).displayName || '';
  const companyRole = useSelector(getSelectedCompany)?.role || RoleResource.NO_ACCESS;

  return (
    <UserDropdownUI userName={userName} roleText={getRoleMap(t)[companyRole]}>
      <TopbarMenuItem
        icon={ProfileOutlined}
        key="userProfile"
        translation={t('common:userMenu.profile')}
        {...getA11yHandlers(() => dispatch(setUserProfileVisible(true)))}
      />
    </UserDropdownUI>
  );
};
