import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { ExternalSubmitButton } from '@cam/app/src/components/Form';
import {
  getIsUpdatingCompanyProfile,
  getSelectedCompanyId,
} from '@cam/app/src/redux/Company/selectors';
import { RootState } from '@cam/app/src/redux/reducer';
import { Button } from '@cam/atomic';

const StyledFooter = styled.div`
  align-items: center;
  border-top: 1px solid ${props => props.theme.colors.neutral.light.border};
  display: flex;
  justify-content: space-between;
  height: 55px;
  margin-bottom: -24px;
  margin-left: -24px;
  padding: 0 25px 0 15px;
  width: calc(100% + 48px);

  *:only-child {
    margin-left: auto;
  }
`;

export const CompanyProfileFooter: React.FC<{
  onClose?: () => void;
  onSubmit: () => Promise<void>;
}> = ({ onClose, onSubmit }) => {
  const { t } = useTranslation();
  const companyId = useSelector(getSelectedCompanyId);

  const isUpdatingCompanyProfile = useSelector((state: RootState) =>
    getIsUpdatingCompanyProfile(state, companyId)
  );

  return (
    <StyledFooter>
      {onClose && (
        <Button onClick={onClose} variant="text" danger>
          {t('company:companyProfile.delete')}
        </Button>
      )}

      <ExternalSubmitButton loading={isUpdatingCompanyProfile} onClick={onSubmit}>
        {t('company:cta.update')}
      </ExternalSubmitButton>
    </StyledFooter>
  );
};
