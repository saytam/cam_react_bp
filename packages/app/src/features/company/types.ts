import { Currency } from '@cam/firebase/resource/Common';
import { BusinessType } from '@cam/firebase/resource/Company';

export enum FORM_FIELDS {
  CATEGORY = 'category',
  COMPANY_NAME = 'companyName',
  CURRENCY = 'currency',
}

export interface CompanyProfileForm {
  [FORM_FIELDS.COMPANY_NAME]: string;
  [FORM_FIELDS.CATEGORY]?: BusinessType;
  [FORM_FIELDS.CURRENCY]: Currency;
}
