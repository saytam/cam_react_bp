import { Space, Row } from 'antd';
import { useTranslation } from 'react-i18next';

import { Body } from '@cam/app/src/components/Body';
import { PageTitle } from '@cam/app/src/components/PageTitle';
import { CompanyProfile } from '@cam/app/src/features/company/CompanyProfile';

export const Company: React.FC = ({ children }) => {
  const { t } = useTranslation();

  return (
    <>
      <Body
        header={
          <Space direction="vertical" size="large">
            <Row justify="space-between">
              <PageTitle>{t('company:companyProfile.title')}</PageTitle>
            </Row>
          </Space>
        }
      >
        <CompanyProfile />
      </Body>
      {children}
    </>
  );
};
