import { Form, Input, Select, Tooltip } from 'antd';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { FormProps } from '@cam/app/src/components/Form';
import { useRules } from '@cam/app/src/components/Form/rules';
import { CompanyProfileForm, FORM_FIELDS } from '@cam/app/src/features/company/types';
import { MAX_INPUT_WIDTH } from '@cam/app/src/features/events/components/form/utils';
import { categoriesTranslations } from '@cam/app/src/features/signup/translations';
import { fetchCompanyInfo } from '@cam/app/src/redux/Company';
import { getSelectedCompany } from '@cam/app/src/redux/Company/selectors';
import { Currency } from '@cam/firebase/resource/Common';
import { BusinessType } from '@cam/firebase/resource/Company';

export const StyledForm = styled(Form).attrs({
  labelCol: { span: 8, xxl: { offset: 1 } },
  wrapperCol: { span: 12, xxl: 8 },
  colon: false,
})`
  label {
    padding-right: 35px;
  }
`;

export const CompanyUpdateForm: React.FC<FormProps<CompanyProfileForm>> = ({
  form,
  name: formName,
  readonly,
}) => {
  const { t } = useTranslation(['company']);
  const dispatch = useDispatch();
  const company = useSelector(getSelectedCompany);
  const rules = useRules();
  const categories = categoriesTranslations(t);

  useEffect(() => {
    company?.companyId && dispatch(fetchCompanyInfo(company.companyId));
  }, [company, dispatch]);

  useEffect(() => {
    form.resetFields();
  }, [form, company]);

  return (
    <StyledForm
      name={formName}
      form={form}
      initialValues={{
        [FORM_FIELDS.COMPANY_NAME]: company?.companyName,
        [FORM_FIELDS.CATEGORY]: company?.businessType,
        [FORM_FIELDS.CURRENCY]: company?.currency,
      }}
    >
      <Form.Item
        label={t('company:credentials.name.label')}
        name="companyName"
        rules={[rules.required, rules.max()]}
      >
        <Input disabled={readonly} style={{ maxWidth: MAX_INPUT_WIDTH }} />
      </Form.Item>

      <Form.Item
        label={t('company:credentials.businessType.label')}
        name="category"
        rules={[rules.required]}
      >
        <Select
          filterOption
          showSearch
          optionFilterProp="children"
          placeholder={t('company:credentials.businessType.placeholder')}
          disabled={readonly}
          style={{ maxWidth: MAX_INPUT_WIDTH }}
        >
          {Object.values(BusinessType).map(category => (
            <Select.Option value={category} key={category}>
              {categories[category]}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item
        label={t('company:credentials.currency.label')}
        name="currency"
        rules={[rules.required]}
      >
        <Tooltip title={t('company:credentials.currency.tooltip')} placement="bottom">
          <Select
            disabled={true}
            style={{ maxWidth: MAX_INPUT_WIDTH }}
            value={company?.currency}
          ></Select>
        </Tooltip>
      </Form.Item>
    </StyledForm>
  );
};
