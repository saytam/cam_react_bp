import { Form, FormInstance } from 'antd';
import { useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { useRBAC, AccessScope } from '@cam/app/src/components/AccessController';
import { DirtProvider } from '@cam/app/src/components/Form';
import { CompanyUpdateForm } from '@cam/app/src/features/company/CompanyUpdateForm';
import { CompanyProfileFooter } from '@cam/app/src/features/company/components/Footer';
import { CompanyProfileForm } from '@cam/app/src/features/company/types';
import { updateCompany } from '@cam/app/src/redux/Company';
import { getSelectedCompany, getSelectedCompanyId } from '@cam/app/src/redux/Company/selectors';
import { showModal, ModalType } from '@cam/app/src/redux/Modals';
import { Card } from '@cam/atomic';

const useDeleteCompany = () => {
  const dispatch = useDispatch();
  const company = useSelector(getSelectedCompany);

  return useCallback(() => {
    dispatch(
      showModal({
        type: ModalType.DeleteCompany,
        props: {
          companyName: company?.companyName,
          companyId: company?.companyId || '',
        },
      })
    );
  }, [dispatch, company]);
};

const useSaveCompany = (form: FormInstance<CompanyProfileForm>) => {
  const dispatch = useDispatch();
  const companyId = useSelector(getSelectedCompanyId);
  const submit = async () => {
    try {
      const { companyName: name, category: businessType, currency } = await form.validateFields();
      dispatch(updateCompany(companyId, { name, businessType, currency }));
    } catch {}
  };
  return submit;
};

export const CompanyProfile: React.FC = () => {
  const { t } = useTranslation();
  const companyId = useSelector(getSelectedCompanyId);
  const { hasWriteAccess: canUpdateCompany } = useRBAC(AccessScope.COMPANY);

  const [form] = Form.useForm<CompanyProfileForm>();
  const handleClose = useDeleteCompany();
  const handleSubmit = useSaveCompany(form);

  return (
    <Card title={t('company:companyProfile.information')}>
      <DirtProvider key={companyId}>
        <CompanyUpdateForm form={form} name="companyForm" readonly={!canUpdateCompany} />
        {canUpdateCompany && <CompanyProfileFooter onClose={handleClose} onSubmit={handleSubmit} />}
      </DirtProvider>
    </Card>
  );
};
