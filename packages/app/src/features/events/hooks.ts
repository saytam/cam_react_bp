import { navigate, useParams } from '@reach/router';
import { message } from 'antd';
import { FormikProps } from 'formik';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { Routes } from '@cam/app/src/constants/routes';
import { initialWallet } from '@cam/app/src/features/events/detail/components/constants';
import {
  getDataFromObjects,
  removeEventIdFromLocalStorage,
} from '@cam/app/src/features/events/utils';
import { fetchEventDetail, updateEvent } from '@cam/app/src/redux/Events/eventDetail';
import {
  eventDetailSelectors,
  getHasEventDetailError,
  getIsEventDetailInitialized,
  getIsEventDetailLoading,
  getIsUpdatingEvent,
} from '@cam/app/src/redux/Events/selectors';
import { ModalType, showModal } from '@cam/app/src/redux/Modals';
import { RootState } from '@cam/app/src/redux/reducer';
import { Currency } from '@cam/firebase/resource/Common';
import { TimeUnit } from '@cam/firebase/resource/Company';
import { EventDetailResource } from '@cam/firebase/resource/Event';

type FormRef = React.MutableRefObject<FormikProps<EventDetailResource> | null>;

export const useHandleSubmit = (formRef: FormRef) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const handleNavigateBack = useNavigateBack();
  const { eventId } = useParams<{ eventId: string }>();
  const eventDetail = useSelector((state: RootState) =>
    eventDetailSelectors.selectById(state, eventId)
  );

  return () => {
    const { isChanged, error } = getDataFromObjects(
      t,
      eventDetail,
      formRef.current?.values,
      formRef.current?.errors
    );

    if (error) {
      message.open(error);
      return;
    }

    if (formRef.current && isChanged) {
      dispatch(updateEvent(formRef.current?.values));
    } else {
      handleNavigateBack();
    }
  };
};

export const useDiscardFormikChanges = (eventId: string, resetForm?: () => void) => {
  const dispatch = useDispatch();

  return {
    discardChanges: () => {
      removeEventIdFromLocalStorage(eventId);
      dispatch(fetchEventDetail(eventId));
      resetForm ? resetForm() : navigate(Routes.EVENTS.BASE);
    },
  };
};

export const useHandleClose = (formRef: FormRef) => {
  const dispatch = useDispatch();
  const { eventId } = useParams<{ eventId: string }>();

  return () => {
    formRef.current &&
      dispatch(
        showModal({
          type: ModalType.EventSave,
          props: { eventId, data: formRef.current },
        })
      );
  };
};

export const useEventDetailValues = () => {
  const { eventId } = useParams<{ eventId: string }>();

  const hasLoadingError = useSelector(getHasEventDetailError);
  const isLoading = useSelector(getIsEventDetailLoading);
  const isInitialized = useSelector(getIsEventDetailInitialized);
  const isUpdating = useSelector((state: RootState) => getIsUpdatingEvent(state, eventId));
  const eventData = useSelector((state: RootState) =>
    eventDetailSelectors.selectById(state, eventId)
  );

  // if (!!eventData) {
  //   if (!eventData?.walletBefore) {
  //     eventData = { ...eventData, walletBefore: initialWallet };
  //   }
  //   if (!eventData?.walletAfter) {
  //     eventData = { ...eventData, walletAfter: initialWallet };
  //   }

  //   if (!eventData?.middle) {
  //     eventData = {
  //       ...eventData,
  //       middle: {
  //         employeesSalaryCash: {
  //           ahaha: {
  //             salary: {
  //               amount: { amount: 200, currency: Currency.CZK },
  //               perTimeUnit: TimeUnit.HOUR,
  //             },
  //             amount: 2,
  //           },
  //         },
  //       },
  //     };
  //   }
  // }

  const isLoadingEvent = isLoading || !isInitialized || !eventData;

  return {
    isLoading,
    isLoadingEvent,
    hasLoadingError,
    isUpdating,
    eventData,
  };
};

export const useNavigateBack = () => {
  const { eventId } = useParams<{ eventId: string }>();
  return () => {
    removeEventIdFromLocalStorage(eventId);
    navigate(Routes.EVENTS.BASE);
  };
};
