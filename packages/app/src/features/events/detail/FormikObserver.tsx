import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { dataActions } from '@cam/app/src/redux/Events/eventDetail';
import { eventDetailSelectors } from '@cam/app/src/redux/Events/selectors';
import { RootState } from '@cam/app/src/redux/reducer';

export const FormikObserver: React.FC<{ eventId: string; dirty: boolean }> = ({
  eventId,
  dirty,
}) => {
  const dispatch = useDispatch();
  const event = useSelector((state: RootState) => eventDetailSelectors.selectById(state, eventId));
  const [editedState, setEditedState] = useState<boolean>(false);

  useEffect(() => {
    if (dirty && !editedState) {
      dispatch(dataActions.updateDataSuccess({ entityIds: [eventId], ...event }));
      event?.eventId && setEditedState(true);
    }
    if (!dirty && editedState) {
      dispatch(
        dataActions.updateDataSuccess({
          entityIds: [eventId],
          ...event,
        })
      );
      setEditedState(false);
    }
  }, [dirty, eventId, dispatch, event, editedState]);

  return null;
};
