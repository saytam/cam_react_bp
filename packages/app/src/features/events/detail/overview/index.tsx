import { Space } from 'antd';
import { Select as FormikSelect, DatePicker as FormikDatePicker } from 'formik-antd';
import { useTranslation } from 'react-i18next';

import { CustomerSelectFormik } from '@cam/app/src/features/events/components/form/CustomerSelectFormik';
import { DebouncedInput } from '@cam/app/src/features/events/components/form/DebouncedInput';
import { DebouncedTextArea } from '@cam/app/src/features/events/components/form/DebouncedTextArea';
import { EmployeeSelectFormik } from '@cam/app/src/features/events/components/form/EmployeeSelectFormik';
import { FormItem } from '@cam/app/src/features/events/components/form/FormItem';
import { FormItemWithBadge } from '@cam/app/src/features/events/components/form/FormItemWithBadge';
import { OverviewSection } from '@cam/app/src/features/events/components/form/OverviewSection';
import { RadioGroup } from '@cam/app/src/features/events/components/form/RadioGroup';
import {
  MAX_INPUT_WIDTH,
  MAX_SMALL_INPUT_WIDTH,
} from '@cam/app/src/features/events/components/form/utils';
import {
  useRequiredValidator,
  validateMaxLength,
} from '@cam/app/src/features/events/components/form/validationHelpers';
import { OVERVIEW_PATHS } from '@cam/app/src/features/events/detail/overview/formTypes';
import {
  getAgreementOptions,
  getEventTypeOptions,
} from '@cam/app/src/features/events/detail/overview/utils';

export const Overview: React.FC = () => {
  const { t } = useTranslation();
  const validateRequired = useRequiredValidator();

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <OverviewSection title={t('events:create.form.event.parts.essential')}>
          <DebouncedInput
            name={OVERVIEW_PATHS.NAME}
            title={t('events:create.form.event.name')}
            validate={value => validateRequired?.(value) || validateMaxLength(t, value)}
            required
          />
          <FormItemWithBadge
            name={OVERVIEW_PATHS.EVENT_TYPE}
            title={t('events:create.form.event.type')}
            validate={validateRequired}
            required
          >
            <FormikSelect
              name={OVERVIEW_PATHS.EVENT_TYPE}
              id={OVERVIEW_PATHS.EVENT_TYPE}
              filterOption
              showSearch
              optionFilterProp="label"
              options={getEventTypeOptions(t)}
              style={{ maxWidth: MAX_INPUT_WIDTH }}
            />
          </FormItemWithBadge>
          <FormItemWithBadge
            name={OVERVIEW_PATHS.DATE}
            title={t('events:create.form.event.date')}
            validate={validateRequired}
            required
          >
            <FormikDatePicker
              name={OVERVIEW_PATHS.DATE}
              picker="date"
              showTime
              format="MMM D, YYYY HH:mm"
              style={{ width: MAX_SMALL_INPUT_WIDTH }}
            />
          </FormItemWithBadge>
          <EmployeeSelectFormik
            name={OVERVIEW_PATHS.PRODUCTION_MANAGER}
            title={t('events:create.form.event.productionManager')}
            validate={validateRequired}
            required
          />
        </OverviewSection>

        <OverviewSection title={t('events:create.form.event.parts.additional')}>
          <EmployeeSelectFormik
            name={OVERVIEW_PATHS.PERSONNEL}
            title={t('events:create.form.event.personnel')}
            mode="multiple"
          />
          <DebouncedTextArea
            name={OVERVIEW_PATHS.DESCRIPTION}
            title={t('events:create.form.event.description')}
            validate={value => value && validateMaxLength(t, value, 1000)}
          />
          <DebouncedInput
            name={OVERVIEW_PATHS.HOSTS}
            title={t('events:create.form.event.hosts')}
            type="number"
            step={5}
            min={0}
          />
        </OverviewSection>
        <OverviewSection title={t('events:create.form.event.parts.administrative')}>
          <FormItem
            name={OVERVIEW_PATHS.AGREEMENT}
            title={t('events:create.form.event.agreement.title')}
          >
            <RadioGroup name={OVERVIEW_PATHS.AGREEMENT} options={getAgreementOptions(t)} />
          </FormItem>
          {/* <PaymentFormik
            name={OVERVIEW_PATHS.PAYMENT}
            title={t('events:create.form.event.payment.title')}
          /> */}
          <CustomerSelectFormik
            name={OVERVIEW_PATHS.CUSTOMER}
            title={t('events:create.form.event.customer.title')}
          />
        </OverviewSection>
      </Space>
    </>
  );
};
