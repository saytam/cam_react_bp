import { TFunction } from 'i18next';

import { getEventTypeTranslations } from '@cam/app/src/features/events/create/form/translation';
import { Agreement, EventType } from '@cam/firebase/resource/Event';

export const getAgreementOptions = (
  t: TFunction
): {
  description?: string;
  label: React.ReactNode;
  value: Agreement;
}[] => [
  {
    value: Agreement.WITHOUT,
    label: t('events:create.form.event.agreement.without'),
  },
  {
    value: Agreement.PENDING,
    label: t('events:create.form.event.agreement.pending'),
  },
  {
    value: Agreement.SIGNED,
    label: t('events:create.form.event.agreement.signed'),
  },
];

export const getEventTypeOptions = (t: TFunction) => {
  const eventTypeTranslationMap = getEventTypeTranslations(t);
  return Object.values(EventType).map(key => ({
    label: eventTypeTranslationMap[key],
    value: key as EventType,
  }));
};
