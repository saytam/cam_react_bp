export enum OVERVIEW_PATHS {
  NAME = 'name',
  DATE = 'date',
  EVENT_TYPE = 'eventType',
  DESCRIPTION = 'description',
  HOSTS = 'hosts',
  PRODUCTION_MANAGER = 'productionManager',
  PERSONNEL = 'personnel',
  PAYMENT = 'payment',
  AGREEMENT = 'agreement',
  CUSTOMER = 'customer',
}
