import { CalendarOutlined } from '@ant-design/icons';
import { Space } from 'antd';

import { Icon } from '@cam/atomic';

export const EventDetailTitle: React.FC<{ eventName?: string }> = ({ eventName }) => {
  return (
    <Space>
      <Icon icon={CalendarOutlined} />
      {eventName}
    </Space>
  );
};
