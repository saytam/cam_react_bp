import { Row } from 'antd';
import { Checkbox } from 'formik-antd';
import { get } from 'lodash';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { OverviewSection } from '@cam/app/src/features/events/components/form/OverviewSection';
import { AFTER_PATHS } from '@cam/app/src/features/events/detail/after/formTypes';
import { BEFORE_PATHS } from '@cam/app/src/features/events/detail/before/formTypes';
import { MIDDLE_PATHS } from '@cam/app/src/features/events/detail/middle/formTypes';
import { useFormikValues } from '@cam/app/src/features/events/utils';
import { getSelectedCompany } from '@cam/app/src/redux/Company/selectors';
import { HelpText, Text } from '@cam/atomic';
import { Amount } from '@cam/firebase/resource/CashRegister';
import { Currency } from '@cam/firebase/resource/Common';
import { Middle, Wallet } from '@cam/firebase/resource/Event';

const StyledRow = styled(Row)`
  border-top: 1px solid ${props => props.theme.colors.neutral.light.border};
`;

const EnumerationComponent: React.FC<{
  middle: Middle;
  walletBefore: Wallet;
  walletAfter: Wallet;
}> = ({ middle, walletBefore, walletAfter }) => {
  const { t } = useTranslation();
  const { setFieldValue } = useFormikValues();
  const currency = useSelector(getSelectedCompany)?.currency || Currency.CZK;
  const { employeesSalaryCash, rentSpaceCash, salesCash, rentSpaceCard, salesCard, invoice } =
    middle;

  const employeeSalariesSum = employeesSalaryCash
    ? Object.keys(employeesSalaryCash)
        .map(employeeId => {
          const amount = employeesSalaryCash?.[employeeId]?.amount || 0;
          const times = employeesSalaryCash?.[employeeId]?.salary?.amount?.amount || 0;
          return amount * times;
        })
        .reduce((a, b) => a + b, 0)
    : 0;

  const cashIncome = rentSpaceCash + salesCash - employeeSalariesSum;
  const cash = walletBefore.total.amount + cashIncome;
  const difference = walletAfter.total.amount - cash;
  const tuzer = difference;
  const result = rentSpaceCard + salesCard + invoice + cashIncome;
  const cashFinal = walletAfter.total.amount - tuzer;
  const cashRegisterIncome = cashFinal - walletBefore.total.amount;

  useEffect(() => {
    const amount = {
      amount: cashRegisterIncome,
      currency,
    } as Amount;
    setFieldValue(AFTER_PATHS.CASH_REGISTER_INCOME, amount);
  }, [setFieldValue, currency, cashRegisterIncome]);

  return (
    <OverviewSection title={t('events:detail.enumeration.title')}>
      <Row justify="space-between" align="middle">
        <HelpText help={t('events:detail.enumeration.incomeCashHint')}>
          {t('events:detail.enumeration.incomeCash')}
        </HelpText>
        <Text>{cashIncome}</Text>
      </Row>
      <Row justify="space-between" align="middle">
        <HelpText help={t('events:detail.enumeration.cashHint')}>
          {t('events:detail.enumeration.cash')}
        </HelpText>
        <Text>{cash}</Text>
      </Row>
      <Row justify="space-between" align="middle">
        <HelpText help={t('events:detail.enumeration.differenceHint')}>
          {t('events:detail.enumeration.difference')}
        </HelpText>
        <Text>{difference}</Text>
      </Row>
      <Row justify="space-between" align="middle">
        <HelpText help={t('events:detail.enumeration.tuzerHint')}>
          {t('events:detail.enumeration.tuzer')}
        </HelpText>
        <Text>{tuzer}</Text>
      </Row>
      <Row justify="space-between" align="middle">
        <HelpText help={t('events:detail.enumeration.resultHint')}>
          {t('events:detail.enumeration.result')}
        </HelpText>
        <Text>{result}</Text>
      </Row>
      <Row justify="space-between" align="middle">
        <HelpText help={t('events:detail.enumeration.cashFinalHint')}>
          {t('events:detail.enumeration.cashFinal')}
        </HelpText>
        <Text>{cashFinal}</Text>
      </Row>
      <StyledRow justify="space-between" align="middle">
        <Text type={cashRegisterIncome >= 0 ? 'success' : 'danger'}>
          {t('events:detail.enumeration.cashRegisterIncome')}
        </Text>
        <Text type={cashRegisterIncome >= 0 ? 'success' : 'danger'}>{cashRegisterIncome}</Text>
      </StyledRow>
      <Checkbox name={AFTER_PATHS.ENUMERATION_SUBMIT}>
        {t('events:detail.enumeration.createCashRegisterRecord')}
      </Checkbox>
    </OverviewSection>
  );
};

export const Enumeration: React.FC = () => {
  const { values } = useFormikValues();
  const { t } = useTranslation();
  const walletBefore = get(values, BEFORE_PATHS.WALLET_BEFORE);
  const walletAfter = get(values, AFTER_PATHS.WALLET_AFTER);
  const middle = get(values, MIDDLE_PATHS.MIDDLE);

  if (!middle || !walletBefore || !walletAfter) {
    return <div>{t('common:messages.failure')}</div>;
  }

  return (
    <EnumerationComponent walletAfter={walletAfter} walletBefore={walletBefore} middle={middle} />
  );
};
