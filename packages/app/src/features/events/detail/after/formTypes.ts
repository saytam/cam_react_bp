export enum AFTER_PATHS {
  WALLET_AFTER = 'walletAfter',
  ENUMERATION = 'enumeration',
  ENUMERATION_SUBMIT = 'enumeration.submit',
  INCOME_CASH = 'enumeration.incomeCash',
  CASH = 'enumeration.cash',
  CASH_DIFFERENCE = 'enumeration.cashDifference',
  TUZER = 'enumeration.tuzer',
  RESULT = 'enumeration.result',
  RESULT_FINAL = 'enumeration.resultFinal',
  CASH_REGISTER_INCOME = 'enumeration.cashRegisterIncome',
}
