import { Space } from 'antd';

import { Enumeration } from '@cam/app/src/features/events/detail/after/Enumeration';

export const After: React.FC = () => {
  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Enumeration />
    </Space>
  );
};
