import { FormikProps } from 'formik';
import { debounce } from 'lodash';
import { Component } from 'react';
import isEqual from 'react-fast-compare';
import { Translation } from 'react-i18next';

import {
  generateEventPersistId,
  removeEventIdFromLocalStorage,
  saveEventIntoLocalStorage,
} from '@cam/app/src/features/events/utils';
import { EventDetailResource } from '@cam/firebase/resource/Event';

const DEBOUNCE_TIME = 300;

interface PersistProps {
  eventId: string;
  formik: FormikProps<EventDetailResource>;
}

export class Persist extends Component<PersistProps> {
  saveForm = debounce((data: FormikProps<EventDetailResource>) => {
    saveEventIntoLocalStorage({
      errMessage: <Translation>{t => t('events:detail.save.maximumQuota.error')}</Translation>,
      eventId: this.props.eventId,
      data: JSON.stringify(data),
    });
  }, DEBOUNCE_TIME);

  componentDidUpdate(prevProps: PersistProps) {
    if (!isEqual(prevProps.formik, this.props.formik)) {
      this.saveForm(this.props.formik);
    }
  }

  componentDidMount() {
    const persistId = generateEventPersistId(this.props.eventId);
    const maybeState = localStorage.getItem(persistId);

    if (maybeState && maybeState !== null) {
      this.props.formik.setFormikState(JSON.parse(maybeState));
    }
  }

  componentWillUnmount() {
    const persistId = generateEventPersistId(this.props.eventId);
    const maybeState = localStorage.getItem(persistId);
    const parsed: FormikProps<EventDetailResource> = maybeState ? JSON.parse(maybeState) : {};
    if (!this.props.formik.dirty && !parsed.dirty) {
      removeEventIdFromLocalStorage(this.props.eventId);
    }
  }

  render() {
    return null;
  }
}
