import { Space } from 'antd';
import { useTranslation } from 'react-i18next';

import { AFTER_PATHS } from '@cam/app/src/features/events/detail/after/formTypes';
import { BEFORE_PATHS } from '@cam/app/src/features/events/detail/before/formTypes';
import { Wallet } from '@cam/app/src/features/events/detail/components/Wallet';

export const Before: React.FC = () => {
  const { t } = useTranslation();
  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Wallet
        walletPath={BEFORE_PATHS.WALLET_BEFORE}
        title={t('events:detail.wallet.before.title')}
        hint={t('events:detail.wallet.before.hint')}
      />
      <Wallet
        walletPath={AFTER_PATHS.WALLET_AFTER}
        title={t('events:detail.wallet.after.title')}
        hint={t('events:detail.wallet.after.hint')}
      />
    </Space>
  );
};
