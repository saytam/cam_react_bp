import {
  InfoCircleOutlined,
  CheckSquareOutlined,
  FileAddOutlined,
  FileDoneOutlined,
  DollarOutlined,
} from '@ant-design/icons';
import { useParams, navigate } from '@reach/router';
import { TFunction } from 'i18next';
import { memo } from 'react';
import { useTranslation } from 'react-i18next';

import { EventsDetailTabs } from '@cam/app/src/enums/events';
import { After } from '@cam/app/src/features/events/detail/after';
import { Before } from '@cam/app/src/features/events/detail/before';
import { Middle } from '@cam/app/src/features/events/detail/middle';
import { Overview } from '@cam/app/src/features/events/detail/overview';
import { Icon, Tabs, TabItem } from '@cam/atomic';

const getTabs = (t: TFunction): TabItem<EventsDetailTabs>[] => [
  {
    icon: <Icon icon={InfoCircleOutlined} size={16} />,
    title: t('events:detail.tabs.overview'),
    key: EventsDetailTabs.OVERVIEW,
    content: <Overview />,
  },
  {
    icon: <Icon icon={FileAddOutlined} size={16} />,
    title: t('events:detail.tabs.before'),
    key: EventsDetailTabs.BEFORE,
    content: <Before />,
  },
  {
    icon: <Icon icon={DollarOutlined} size={16} />,
    title: t('events:detail.tabs.middle'),
    key: EventsDetailTabs.MIDDLE,
    content: <Middle />,
  },
  {
    icon: <Icon icon={FileDoneOutlined} size={16} />,
    title: t('events:detail.tabs.after'),
    key: EventsDetailTabs.AFTER,
    content: <After />,
  },
  // TODO: add checklist
  // {
  //   icon: <Icon icon={CheckSquareOutlined} size={16} />,
  //   title: t('events:detail.tabs.checklist'),
  //   key: EventsDetailTabs.CHECKLIST,
  //   content: <div>Checklist</div>,
  // },
];

export const DetailTabs: React.FC = memo(() => {
  const { t } = useTranslation();
  const { tabId } = useParams<{ tabId: EventsDetailTabs }>();
  const tabs = getTabs(t);
  const activeKey = tabs.map(tab => tab.key).includes(tabId) ? tabId : EventsDetailTabs.OVERVIEW;

  return <Tabs.List activeKey={activeKey} onChange={navigate} tabs={tabs} />;
});
