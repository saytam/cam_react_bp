import { useParams } from '@reach/router';
import { Space } from 'antd';
import { Formik, FormikProps } from 'formik';
import { Form } from 'formik-antd';
import { useCallback, useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { AccessController, AccessScope } from '@cam/app/src/components/AccessController';
import { SidePanel } from '@cam/app/src/components/SidePanel';
import { EventsDetailTabs } from '@cam/app/src/enums/events';
import { ErrorOrLoader } from '@cam/app/src/features/events/components/ErrorOrLoader';
import { DeleteEventButton } from '@cam/app/src/features/events/detail/DeleteEventButton';
import { FormikObserver } from '@cam/app/src/features/events/detail/FormikObserver';
import { Persist } from '@cam/app/src/features/events/detail/FormikPersist';
import { DetailTabs } from '@cam/app/src/features/events/detail/Tabs';
import { EventDetailTitle } from '@cam/app/src/features/events/detail/Title';
import {
  useEventDetailValues,
  useHandleClose,
  useHandleSubmit,
  useNavigateBack,
} from '@cam/app/src/features/events/hooks';
import { fetchEventDetail } from '@cam/app/src/redux/Events/eventDetail';
import { ModalType, showModal } from '@cam/app/src/redux/Modals';
import { DateUtil } from '@cam/app/src/utils/date';
import { Button, Spin } from '@cam/atomic';
import { Agreement, EventDetailResource, EventType } from '@cam/firebase/resource/Event';

const initialValues = {
  eventId: '',
  companyId: '',
  name: '',
  eventType: EventType.OTHER,
  date: DateUtil.date().toISOString(),
  productionManager: '',
  personnel: [],
  agreement: Agreement.WITHOUT,
} as EventDetailResource;

export const EventDetail: React.FC = () => {
  const formRef = useRef<FormikProps<EventDetailResource> | null>(null);
  const dispatch = useDispatch();
  const { eventId } = useParams<{ eventId: string; tabId: EventsDetailTabs }>();
  const { t } = useTranslation();

  const isFormDirty = formRef.current?.dirty;
  const { isLoadingEvent, hasLoadingError, isUpdating, eventData } = useEventDetailValues();
  const handleNavigateBack = useNavigateBack();
  const handleSubmit = useHandleSubmit(formRef);
  const handleClose = useHandleClose(formRef);
  const handleFetchData = useCallback(() => {
    dispatch(fetchEventDetail(eventId));
  }, [eventId, dispatch]);

  useEffect(() => {
    handleFetchData();
  }, [dispatch, handleFetchData]);

  const handleDelete = () =>
    dispatch(
      showModal({
        type: ModalType.DeleteEvent,
        props: {
          eventId,
        },
      })
    );

  return (
    <SidePanel
      onClose={isFormDirty ? undefined : handleNavigateBack}
      customOnClose={isFormDirty ? handleClose : undefined}
      title={<EventDetailTitle eventName={eventData?.name} />}
      footer={
        <Space>
          <Button onClick={isFormDirty ? handleClose : handleNavigateBack}>
            {t('common:close')}
          </Button>
          <AccessController scope={AccessScope.EVENT}>
            <Button variant="primary" loading={isUpdating} onClick={handleSubmit}>
              {t('common:saveBtn')}
            </Button>
          </AccessController>
        </Space>
      }
    >
      <ErrorOrLoader
        isLoading={isLoadingEvent}
        hasError={hasLoadingError}
        refetch={handleFetchData}
      >
        <Spin spinning={isUpdating}>
          <Formik
            onSubmit={handleSubmit}
            initialValues={eventData || initialValues}
            innerRef={formRef}
            enableReinitialize
          >
            {props => (
              <Form>
                <DeleteEventButton onClick={handleDelete} />
                <DetailTabs />
                <Persist eventId={eventId} formik={props} />
                <FormikObserver dirty={props.dirty} eventId={eventId} />
              </Form>
            )}
          </Formik>
        </Spin>
      </ErrorOrLoader>
    </SidePanel>
  );
};
