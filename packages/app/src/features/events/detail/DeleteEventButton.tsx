import { FileExclamationOutlined } from '@ant-design/icons';
import { Row } from 'antd';
import { useTranslation } from 'react-i18next';

import { AccessController, AccessScope } from '@cam/app/src/components/AccessController';
import { Button } from '@cam/atomic';

export const DeleteEventButton: React.FC<{ onClick: () => void }> = ({ onClick }) => {
  const { t } = useTranslation();

  return (
    <AccessController scope={AccessScope.EVENT}>
      <Row justify="end">
        <Button variant="text" size="small" onClick={onClick} icon={<FileExclamationOutlined />}>
          {t('events:detail.event.deleteButton')}
        </Button>
      </Row>
    </AccessController>
  );
};
