export enum MIDDLE_PATHS {
  MIDDLE = 'middle',
  DEPOSIT = 'middle.deposit',
  RENT_SPACE_CASH = 'middle.rentSpaceCash',
  RENT_SPACE_CARD = 'middle.rentSpaceCard',
  SALES_CASH = 'middle.salesCash',
  SALES_CARD = 'middle.salesCard',
  INVOICE = 'middle.invoice',
  EMPLOYEES_SALARY_CASH = 'middle.employeesSalaryCash',
  OTHER_COSTS = 'middle.otherCosts',
}
