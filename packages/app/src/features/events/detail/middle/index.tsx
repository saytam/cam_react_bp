import { Col, Row, Space } from 'antd';
import { useTranslation } from 'react-i18next';

import { DebouncedInput } from '@cam/app/src/features/events/components/form/DebouncedInput';
import { OverviewSection } from '@cam/app/src/features/events/components/form/OverviewSection';
import { EmployeePayment } from '@cam/app/src/features/events/detail/middle/EmployeePayment';
import { MIDDLE_PATHS } from '@cam/app/src/features/events/detail/middle/formTypes';

export const Middle: React.FC = () => {
  const { t } = useTranslation();
  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Row justify="space-between" gutter={2}>
        <Col xs={12} md={12} lg={12}>
          <OverviewSection
            title={t('events:detail.rentSection.title')}
            titleColor="success"
            hint={t('events:detail.rentSection.hint')}
          >
            <DebouncedInput
              name={MIDDLE_PATHS.DEPOSIT}
              title={t('events:detail.rentSection.deposit')}
              type="number"
            />
            <DebouncedInput
              name={MIDDLE_PATHS.RENT_SPACE_CASH}
              title={t('events:detail.rentSection.rentSpaceCash')}
              type="number"
            />
            <DebouncedInput
              name={MIDDLE_PATHS.RENT_SPACE_CARD}
              title={t('events:detail.rentSection.rentSpaceCard')}
              type="number"
            />
          </OverviewSection>
        </Col>
        <Col xs={12} md={12} lg={12}>
          <OverviewSection
            title={t('events:detail.salesSection.title')}
            titleColor="success"
            hint={t('events:detail.salesSection.hint')}
          >
            <DebouncedInput
              name={MIDDLE_PATHS.INVOICE}
              title={t('events:detail.salesSection.invoice')}
              type="number"
            />
            <DebouncedInput
              name={MIDDLE_PATHS.SALES_CASH}
              title={t('events:detail.salesSection.salesCash')}
              type="number"
            />
            <DebouncedInput
              name={MIDDLE_PATHS.SALES_CARD}
              title={t('events:detail.salesSection.salesCard')}
              type="number"
            />
          </OverviewSection>
        </Col>
      </Row>
      <EmployeePayment />
      {/* TODO: other costs section */}
      {/* <OverviewSection title={t('events:detail.otherSalesSection.title')} titleColor="warning">
        <DebouncedInput
          name={MIDDLE_PATHS.OTHER_COSTS}
          title={t('events:create.form.event.name')}
          type="number"
        />
      </OverviewSection> */}
    </Space>
  );
};
