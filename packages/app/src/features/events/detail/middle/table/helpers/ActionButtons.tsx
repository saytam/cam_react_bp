import { SaveOutlined, EditOutlined, DeleteOutlined, CloseOutlined } from '@ant-design/icons';

import { Button } from '@cam/atomic';

interface Props {
  isEditable: boolean;
  isDisabled?: boolean;
  isLoading?: boolean;
  canDelete?: boolean;
  onSave: () => void;
  onEdit?: () => void;
  onRemove: () => void;
  onClose?: () => void;
}
export const ActionButtons: React.FC<Props> = ({
  isEditable,
  canDelete = false,
  isDisabled = false,
  isLoading = false,
  onEdit,
  onRemove,
  onSave,
  onClose,
}) => {
  if (isEditable) {
    return (
      <>
        <Button
          onClick={onSave}
          inline
          icon={<SaveOutlined aria-hidden="true" />}
          variant="link"
          disabled={isDisabled}
          loading={isLoading}
        />
        <Button
          onClick={onClose}
          inline
          icon={<CloseOutlined aria-hidden="true" />}
          variant="text"
          disabled={isLoading}
        />
      </>
    );
  } else {
    return (
      <>
        {onEdit && (
          <Button
            onClick={onEdit}
            inline
            icon={<EditOutlined aria-hidden="true" />}
            variant="text"
          />
        )}
        {canDelete && (
          <Button
            onClick={onRemove}
            inline
            icon={<DeleteOutlined aria-hidden="true" />}
            danger
            variant="text"
          />
        )}
      </>
    );
  }
};
