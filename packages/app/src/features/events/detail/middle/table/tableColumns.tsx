import { ColumnProps } from 'antd/es/table';

import { BadgeComponent } from '@cam/app/src/features/events/components/form/Badge';
import { ActionButtons } from '@cam/app/src/features/events/detail/middle/table/helpers/ActionButtons';
import { EmployeeName } from '@cam/app/src/features/events/detail/middle/table/helpers/EmployeeName';
import { SalaryValue } from '@cam/app/src/features/events/detail/middle/table/helpers/SalaryValue';
import { Total } from '@cam/app/src/features/events/detail/middle/table/helpers/Total';
import { Value } from '@cam/app/src/features/events/detail/middle/table/helpers/Value';
import {
  ColumnTypes,
  EmployeePaymentKeys,
  ItemProps,
  SharedColumnProps,
} from '@cam/app/src/features/events/detail/middle/table/types';

export const getTableColumns = ({
  name,
  editingKey,
  t,
  columnValueTitle,
  onSave,
  onRemove,
  onEdit,
  onClose,
  isLoading,
}: SharedColumnProps): (ColumnProps<ItemProps> & {
  dataIndex: ColumnTypes;
  editable?: boolean;
})[] => {
  const isEditing = (key: string) => editingKey === key;

  return [
    {
      title: columnValueTitle || t('events:detail.employeesSalarySection.table.displayName'),
      dataIndex: ColumnTypes.EMPLOYEE_ID,
      width: 100,
      align: 'center',
      render: (_, record) => <EmployeeName employeeId={record.key} />,
    },
    {
      title: columnValueTitle || t('events:detail.employeesSalarySection.table.salary'),
      dataIndex: ColumnTypes.SALARY,
      editable: true,
      width: 100,
      align: 'center',
      render: (_, record, index) => (
        <SalaryValue
          index={index}
          editable={isEditing(record.key)}
          employeePaymentKey={EmployeePaymentKeys.SALARY}
          recordValue={record.salary}
        />
      ),
    },
    {
      title: columnValueTitle || t('events:detail.employeesSalarySection.table.amount'),
      dataIndex: ColumnTypes.AMOUNT,
      editable: true,
      width: 100,
      align: 'center',
      render: (_, record, index) => (
        <Value
          index={index}
          editable={isEditing(record.key)}
          employeePaymentKey={EmployeePaymentKeys.AMOUNT}
          recordValue={record.amount}
        />
      ),
    },
    {
      title: columnValueTitle || t('events:detail.employeesSalarySection.table.total'),
      dataIndex: ColumnTypes.TOTAL,
      width: 100,
      align: 'center',
      render: (_, record) =>
        record.salary && record.amount && <Total salary={record.salary} amount={record.amount} />,
    },
    {
      title: t('events:detail.employeesSalarySection.table.actions'),
      dataIndex: ColumnTypes.ACTIONS,
      fixed: 'right',
      align: 'center',
      width: 100,
      render: (_, record, index) => {
        return (
          <>
            <ActionButtons
              isEditable={isEditing(record.key)}
              isLoading={isLoading}
              onEdit={() => onEdit(record.key)}
              onRemove={() => onRemove(record.key)}
              onSave={() => onSave(record.key, index)}
              onClose={onClose}
            />
            <BadgeComponent path={`${name}.${record.key}`} />
          </>
        );
      },
    },
  ];
};
