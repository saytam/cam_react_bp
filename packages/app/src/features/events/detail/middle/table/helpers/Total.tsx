import { useSelector } from 'react-redux';

import { getSelectedCompany } from '@cam/app/src/redux/Company/selectors';
import { numberWithCommas } from '@cam/app/src/utils/helpers';
import { Text } from '@cam/atomic';
import { Salary } from '@cam/firebase/resource/Company';

export const Total: React.FC<{
  salary: Salary;
  amount: number;
}> = ({ salary, amount }) => {
  const companyInfo = useSelector(getSelectedCompany);

  return (
    <Text>{`${numberWithCommas(salary.amount.amount * amount)} ${companyInfo?.currency}`}</Text>
  );
};
