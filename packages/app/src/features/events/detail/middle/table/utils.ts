import { debounce } from 'lodash';
import { useState } from 'react';

export enum DELAY_SAVE_TIME {
  DELAY_ROW = 0,
  DELAY_MODAL = 200,
}

export const useSaveWithLoading = (
  delay: DELAY_SAVE_TIME
): [isLoading: boolean, debouncedSave: (callback: () => void) => void] => {
  const [isLoading, setLoading] = useState(false);

  const debouncedSave = (callback: () => void) => {
    setLoading(true);

    debounce(() => {
      callback();
      setLoading(false);
    }, delay)();
  };

  return [isLoading, debouncedSave];
};
