import { useTranslation } from 'react-i18next';

import { getTimePerUnitTranslations } from '@cam/app/src/components/Salary/translations';
import { Salary } from '@cam/app/src/features/events/detail/middle/table/inputs/Salary';
import { EmployeePaymentKeys } from '@cam/app/src/features/events/detail/middle/table/types';
import { Salary as SalaryType } from '@cam/firebase/resource/Company';

export const SalaryValue: React.FC<{
  editable: boolean;
  index: number;
  employeePaymentKey: EmployeePaymentKeys;
  recordValue?: SalaryType;
}> = ({ editable, recordValue, index, employeePaymentKey }) => {
  const { t } = useTranslation();
  const translations = getTimePerUnitTranslations(t);
  return editable ? (
    <Salary name={['tableData', index, employeePaymentKey]} />
  ) : (
    <div>
      {recordValue?.amount?.amount +
        '/' +
        translations[recordValue ? recordValue?.perTimeUnit : '']}
    </div>
  );
};
