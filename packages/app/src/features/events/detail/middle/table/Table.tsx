import { Form } from 'antd';
import { TableLocale } from 'antd/es/table/interface';
import { get, omit } from 'lodash';
import { useState, useEffect, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { getTableColumns } from '@cam/app/src/features/events/detail/middle/table/tableColumns';
import { ColumnTypes } from '@cam/app/src/features/events/detail/middle/table/types';
import {
  DELAY_SAVE_TIME,
  useSaveWithLoading,
} from '@cam/app/src/features/events/detail/middle/table/utils';
import { useFormikValues } from '@cam/app/src/features/events/utils';
import { Table } from '@cam/atomic';

interface TableProps {
  name: string;
  columns: ColumnTypes[];
  columnValueTitle?: string;
  bordered?: boolean;
  locale?: TableLocale;
}

const TABLE_HEIGHT = 480;

export const CostsTable: React.FC<TableProps> = ({
  name,
  columns,
  columnValueTitle,
  children,
  bordered = true,
  locale,
}) => {
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const [editingKey, setEditingKey] = useState('');
  const [isLoading, debouncedSave] = useSaveWithLoading(DELAY_SAVE_TIME.DELAY_ROW);
  const [isRemoving, debouncedRemove] = useSaveWithLoading(DELAY_SAVE_TIME.DELAY_ROW);
  const { values, setFieldValue } = useFormikValues();
  const tableData = get(values, name, {});

  const transformedData = useMemo(
    () => (tableData ? Object.keys(tableData).map(key => ({ key, ...tableData[key] })) : []),
    [tableData]
  );

  useEffect(() => {
    form.setFieldsValue({ tableData: transformedData });
  }, [transformedData, form]);

  const handleSave = async (key: string, index: number) => {
    try {
      const isFormTouched = form.isFieldsTouched();
      if (isFormTouched) {
        const { tableData: submitTableData } = await form.validateFields();
        debouncedSave(() => {
          setEditingKey('');
          setFieldValue(`${name}.${key}`, {
            ...submitTableData[index],
          });
        });
      } else {
        setEditingKey('');
      }
    } catch {}
  };

  const handleClose = () => {
    form.resetFields();
    setEditingKey('');
  };

  const handleRemoveRow = (key: string) =>
    debouncedRemove(() => setFieldValue(name, omit(tableData, [key])));

  return (
    <div>
      <Form
        form={form}
        component="div"
        initialValues={{ tableData: transformedData }}
        onKeyDown={e => e.key === 'Escape' && handleClose()}
      >
        <Table
          style={{ maxWidth: 900 }}
          locale={locale}
          height={TABLE_HEIGHT}
          loading={isRemoving}
          size="small"
          bordered={bordered}
          dataSource={transformedData}
          columns={getTableColumns({
            name,
            editingKey,
            t,
            onSave: handleSave,
            onRemove: handleRemoveRow,
            onEdit: setEditingKey,
            onClose: handleClose,
            columnValueTitle,
            isLoading,
          }).filter(
            item => columns.includes(item.dataIndex) || item.dataIndex === ColumnTypes.TOTAL
          )}
          pagination={false}
        />
      </Form>

      {children}
    </div>
  );
};
