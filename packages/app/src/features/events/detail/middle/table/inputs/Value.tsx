import { InputNumber } from 'antd';
import { NamePath } from 'antd/es/form/interface';

import { StyledFormItem } from '@cam/app/src/features/employees/styles';
import { isActionKey } from '@cam/app/src/utils/a11y';

const preventEnterSubmit = (evt: React.KeyboardEvent<HTMLInputElement>) => {
  if (isActionKey(evt, true)) {
    evt.preventDefault();
  }
};

export const ValueInput: React.FC<{
  name: NamePath;
  label?: string;
}> = ({ name, label }) => {
  return (
    <StyledFormItem name={name} labelAlign="left" labelCol={{ xs: 8 }} label={label}>
      <InputNumber
        onKeyDown={preventEnterSubmit}
        size="small"
        autoFocus
        min={0}
        step={1}
        type="number"
      />
    </StyledFormItem>
  );
};
