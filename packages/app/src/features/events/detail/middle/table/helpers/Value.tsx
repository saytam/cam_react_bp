import { ValueInput } from '@cam/app/src/features/events/detail/middle/table/inputs/Value';
import { EmployeePaymentKeys } from '@cam/app/src/features/events/detail/middle/table/types';

export const Value: React.FC<{
  editable: boolean;
  index: number;
  employeePaymentKey: EmployeePaymentKeys;
  recordValue?: number;
}> = ({ editable, recordValue, index, employeePaymentKey }) => {
  return editable ? (
    <ValueInput name={['tableData', index, employeePaymentKey]} />
  ) : (
    <div>{recordValue}</div>
  );
};
