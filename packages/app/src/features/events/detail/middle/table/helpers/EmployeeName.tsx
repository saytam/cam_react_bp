import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { fetchEmployeeDetail } from '@cam/app/src/redux/Employees/detail';
import { getEmployeeDetail } from '@cam/app/src/redux/Employees/selectors';
import { RootState } from '@cam/app/src/redux/reducer';
import { Text } from '@cam/atomic';

export const EmployeeName: React.FC<{
  employeeId: string;
}> = ({ employeeId }) => {
  const dispatch = useDispatch();
  const employee = useSelector((state: RootState) => getEmployeeDetail(state, employeeId));

  useEffect(() => {
    dispatch(fetchEmployeeDetail(employeeId));
  }, [dispatch, employeeId]);

  return <Text style={{ maxWidth: '300px' }}>{employee?.displayName}</Text>;
};
