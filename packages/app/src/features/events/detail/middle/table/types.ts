import { TFunction } from 'i18next';

import { EmployeePaymentRow } from '@cam/firebase/resource/Event';

type ValueProps = EmployeePaymentRow;

export type ItemProps = ValueProps & {
  key: string;
};

export enum ColumnTypes {
  EMPLOYEE_ID = 'employeeId',
  SALARY = 'salary',
  AMOUNT = 'amount',
  TOTAL = 'total',
  ACTIONS = 'actions',
}

export enum EmployeePaymentKeys {
  SALARY = 'salary',
  AMOUNT = 'amount',
}

export interface SharedColumnProps {
  name: string;
  editingKey: string;
  t: TFunction;
  columnValueTitle?: string;
  onSave: (key: string, index: number) => void;
  onRemove: (key: string) => void;
  onEdit: (key: string) => void;
  onClose: () => void;
  isLoading?: boolean;
}

export interface SharedColumnProps {
  name: string;
  editingKey: string;
  t: TFunction;
  columnValueTitle?: string;
  onSave: (key: string, index: number) => void;
  onRemove: (key: string) => void;
  onEdit: (key: string) => void;
  onClose: () => void;
  isLoading?: boolean;
}
