import { InputNumber, Select, Space } from 'antd';
import { NamePath } from 'antd/es/form/interface';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { getTimePerUnitTranslations } from '@cam/app/src/components/Salary/translations';
import { StyledFormItem } from '@cam/app/src/features/employees/styles';
import { Currency } from '@cam/firebase/resource/Common';
import { Salary as SalaryType, TimeUnit } from '@cam/firebase/resource/Company';

interface SalaryProps {
  value?: SalaryType;
  onChange?: (value: SalaryType) => void;
}

const SalaryComponent: React.FC<SalaryProps> = ({
  value = {
    amount: { amount: 0, currency: Currency.CZK },
    perTimeUnit: TimeUnit.HOUR,
  } as SalaryType,
  onChange,
}) => {
  const { t } = useTranslation();
  const timePerUnitTranslations = getTimePerUnitTranslations(t);
  const [salary, setSalary] = useState<SalaryType>(value);

  const triggerChange = (changedPrice: SalaryType) => {
    onChange?.(changedPrice || value);
  };

  const onAmountChange = (amount: number) => {
    const newAmount = { ...value, amount: { amount, currency: value.amount.currency } };
    if (amount < 0) {
      return;
    }

    if (!('amount' in value)) {
      setSalary(newAmount);
    }

    triggerChange(newAmount);
  };

  const onTimePerUnitChange = (newPerTimeUnit: TimeUnit) => {
    if (!Object.keys(TimeUnit).includes(newPerTimeUnit)) {
      return;
    }
    if (!('perTimeUnit' in value)) {
      setSalary({ ...value, perTimeUnit: newPerTimeUnit });
    }

    triggerChange({ ...value, perTimeUnit: newPerTimeUnit });
  };

  return (
    <Space direction="horizontal">
      <InputNumber
        min={0}
        step={100}
        type="number"
        onChange={onAmountChange}
        value={value.amount.amount || salary.amount.amount}
        size="small"
      />
      <Select
        onChange={onTimePerUnitChange}
        value={value.perTimeUnit || salary.amount}
        size="small"
        style={{ minWidth: 100 }}
      >
        {Object.keys(TimeUnit)?.map(timeUnit => (
          <Select.Option value={timeUnit} key={timeUnit}>
            {timePerUnitTranslations[timeUnit]}
          </Select.Option>
        ))}
      </Select>
    </Space>
  );
};

export const Salary: React.FC<{
  name: NamePath;
  label?: string;
}> = ({ name, label }) => {
  return (
    <StyledFormItem name={name} labelAlign="left" labelCol={{ xs: 8 }} label={label}>
      <SalaryComponent />
    </StyledFormItem>
  );
};
