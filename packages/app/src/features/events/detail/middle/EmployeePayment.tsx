import { useParams } from '@reach/router';
import { Row, Space } from 'antd';
import { Trans, useTranslation } from 'react-i18next';

import { Routes } from '@cam/app/src/constants/routes';
import { EventsDetailTabs } from '@cam/app/src/enums/events';
import { OverviewSection } from '@cam/app/src/features/events/components/form/OverviewSection';
import { MIDDLE_PATHS } from '@cam/app/src/features/events/detail/middle/formTypes';
import { CostsTable } from '@cam/app/src/features/events/detail/middle/table/Table';
import { ColumnTypes } from '@cam/app/src/features/events/detail/middle/table/types';
import { OVERVIEW_PATHS } from '@cam/app/src/features/events/detail/overview/formTypes';
import { isValueChanged, useFormikValues } from '@cam/app/src/features/events/utils';
import { NavLink, Spin, Text } from '@cam/atomic';

export const EmployeePayment: React.FC = () => {
  const { t } = useTranslation();
  const { values, initialValues } = useFormikValues();
  const { eventId } = useParams<{ eventId: string }>();
  const isPersonnelChanged = isValueChanged(initialValues, values, OVERVIEW_PATHS.PERSONNEL);
  return (
    <OverviewSection
      title={t('events:detail.employeesSalarySection.title')}
      titleColor="warning"
      hint={t('events:detail.employeesSalarySection.hint')}
    >
      {!isPersonnelChanged ? (
        <CostsTable
          name={MIDDLE_PATHS.EMPLOYEES_SALARY_CASH}
          columns={[
            ColumnTypes.EMPLOYEE_ID,
            ColumnTypes.SALARY,
            ColumnTypes.AMOUNT,
            ColumnTypes.TOTAL,
            ColumnTypes.ACTIONS,
          ]}
          locale={{
            emptyText: (
              <Trans t={t} i18nKey="events:detail.employeesSalarySection.emptyTable">
                To set up salaries, first add workers on{' '}
                <NavLink to={[Routes.EVENTS.BASE, eventId, EventsDetailTabs.OVERVIEW].join('/')}>
                  Overview
                </NavLink>{' '}
                tab
              </Trans>
            ),
          }}
        />
      ) : (
        <Space direction="vertical">
          <Row justify="center">
            <Text>{t('events:detail.employeesSalarySection.changedPersonnelHint')}</Text>
          </Row>
          <Row justify="center">
            <Spin spinning={true} />
          </Row>
        </Space>
      )}
    </OverviewSection>
  );
};
