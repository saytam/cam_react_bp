import { get } from 'lodash';

import { OverviewSection } from '@cam/app/src/features/events/components/form/OverviewSection';
import { SumUpRow } from '@cam/app/src/features/events/detail/components/SumUpRow';
import { WalletBillInput } from '@cam/app/src/features/events/detail/components/WalletBillInput';
import { useFormikValues } from '@cam/app/src/features/events/utils';

export const Wallet: React.FC<{ walletPath: string; title: string; hint?: string }> = ({
  walletPath,
  title,
  hint,
}) => {
  const { values } = useFormikValues();
  const wallet = get(values, walletPath);

  if (!wallet) {
    return <div />;
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { total, ...restWallet } = wallet;

  return (
    <OverviewSection title={title} hint={hint}>
      {!!wallet &&
        Object.keys(restWallet)?.map((billAmount, i) => (
          <div key={i}>
            <WalletBillInput walletPath={walletPath} walletBill={wallet[billAmount]} />
          </div>
        ))}
      <SumUpRow walletPath={walletPath} />
    </OverviewSection>
  );
};
