import { Col, Row } from 'antd';
import { get } from 'lodash';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { useFormikValues } from '@cam/app/src/features/events/utils';
import { getSelectedCompany } from '@cam/app/src/redux/Company/selectors';
import { numberWithCommas } from '@cam/app/src/utils/helpers';
import { Text } from '@cam/atomic';
import { Amount } from '@cam/firebase/resource/CashRegister';

const StyledSumUpRow = styled(Row)`
  border-top: 1px solid ${props => props.theme.colors.neutral.light.border};
  height: 55px;
  margin-bottom: -18px;
`;

const useWallet = (walletPath: string) => {
  const { values, setFieldValue } = useFormikValues();
  const wallet = get(values, walletPath);
  const total: Amount = wallet.total;

  useEffect(() => {
    let sum = 0;
    Object.keys(wallet).forEach(key => {
      if (key !== 'total') {
        sum += wallet[key].bill.amount * wallet[key].quantity;
      }
    });
    setFieldValue(`${walletPath}.total.amount`, sum);
  }, [wallet, setFieldValue, walletPath]);

  return { total };
};

export const SumUpRow: React.FC<{ walletPath: string }> = ({ walletPath }) => {
  const companyInfo = useSelector(getSelectedCompany);
  const { t } = useTranslation();
  const { total } = useWallet(walletPath);

  return (
    <StyledSumUpRow gutter={2}>
      <Col xs={18} md={18} lg={18}>
        <Row align="middle" justify="end" style={{ height: '100%' }}>
          <Text>{t('events:detail.wallet.total')}</Text>
        </Row>
      </Col>
      <Col xs={6} md={6} lg={6}>
        <Row align="middle" justify="center" style={{ height: '100%' }}>
          <Text>{numberWithCommas(total.amount) + ' ' + companyInfo?.currency}</Text>
        </Row>
      </Col>
    </StyledSumUpRow>
  );
};
