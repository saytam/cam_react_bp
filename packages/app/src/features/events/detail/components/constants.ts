import { Currency } from '@cam/firebase/resource/Common';
import { Wallet } from '@cam/firebase/resource/Event';

export const initialWallet: Wallet = {
  total: { amount: 0, currency: Currency.CZK },
  5: { bill: { amount: 5, currency: Currency.CZK }, quantity: 0 },
  10: { bill: { amount: 10, currency: Currency.CZK }, quantity: 0 },
  20: { bill: { amount: 20, currency: Currency.CZK }, quantity: 0 },
  50: { bill: { amount: 50, currency: Currency.CZK }, quantity: 0 },
  100: { bill: { amount: 100, currency: Currency.CZK }, quantity: 0 },
  200: { bill: { amount: 200, currency: Currency.CZK }, quantity: 0 },
  500: { bill: { amount: 500, currency: Currency.CZK }, quantity: 0 },
  1000: { bill: { amount: 1000, currency: Currency.CZK }, quantity: 0 },
  2000: { bill: { amount: 2000, currency: Currency.CZK }, quantity: 0 },
  5000: { bill: { amount: 5000, currency: Currency.CZK }, quantity: 0 },
};
