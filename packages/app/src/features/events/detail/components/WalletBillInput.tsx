import { Col, Row } from 'antd';
import { default as InputNumber, InputNumberProps } from 'antd/es/input-number';
import { debounce, get } from 'lodash';
import { useState, useEffect, useCallback } from 'react';

import { FormItemWithBadge } from '@cam/app/src/features/events/components/form/FormItemWithBadge';
import { MAX_SMALL_INPUT_WIDTH } from '@cam/app/src/features/events/components/form/utils';
import { useFormikValues } from '@cam/app/src/features/events/utils';
import { numberWithCommas } from '@cam/app/src/utils/helpers';
import { Text } from '@cam/atomic';
import { WalletBill } from '@cam/firebase/resource/Event';

const useWalletBillFormValues = (name: string) => {
  const { values, setFieldValue, errors } = useFormikValues();
  const inputValue = get(values, name, '');
  const inputError = get(errors, name, '');
  const [value, setValue] = useState(inputValue);

  useEffect(() => {
    setValue(inputValue);
  }, [setValue, inputValue]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const debouncedSave = useCallback(
    debounce((nextValue: WalletBill) => {
      setFieldValue(name, nextValue);
    }, 200),
    []
  );

  return {
    value,
    setValue,
    inputValue,
    inputError,
    debouncedSave,
  };
};

export const WalletBillInput: React.FC<
  { walletBill: WalletBill; walletPath: string } & InputNumberProps
> = ({ walletBill, walletPath, ...rest }) => {
  const name = `${walletPath}.${walletBill.bill.amount}]`;
  const { value, setValue, debouncedSave } = useWalletBillFormValues(name);

  const handleChange = (fieldValue: string | number) => {
    const valueToBeSaved = { ...walletBill, quantity: fieldValue } as WalletBill;
    setValue(valueToBeSaved);
    debouncedSave(valueToBeSaved);
  };

  return (
    <Row gutter={2}>
      <Col xs={18} md={18} lg={18}>
        <FormItemWithBadge
          name={name}
          title={walletBill?.bill?.amount + ' ' + walletBill.bill.currency}
        >
          <InputNumber
            value={value.quantity || 0}
            onChange={handleChange}
            id={name}
            min={0}
            max={1000000}
            style={{ maxWidth: MAX_SMALL_INPUT_WIDTH, width: '100%' }}
            {...rest}
          />
        </FormItemWithBadge>
      </Col>
      <Col xs={6} md={6} lg={6}>
        <Row align="middle" justify="center" style={{ height: '100%' }}>
          <Text>
            {numberWithCommas(walletBill.bill.amount * walletBill.quantity) +
              ' ' +
              walletBill.bill.currency}
          </Text>
        </Row>
      </Col>
    </Row>
  );
};
