import { CalendarOutlined } from '@ant-design/icons';
import { Space } from 'antd';
import { useTranslation } from 'react-i18next';

import { Icon } from '@cam/atomic';

export const CreateEventTitle: React.FC = () => {
  const { t } = useTranslation();

  return (
    <Space>
      <Icon icon={CalendarOutlined} />
      {t('events:create.title')}
    </Space>
  );
};
