import { useNavigate } from '@reach/router';
import { Form } from 'antd';
import { useDispatch, useSelector } from 'react-redux';

import { DirtProvider } from '@cam/app/src/components/Form';
import { SidePanel } from '@cam/app/src/components/SidePanel';
import { Footer } from '@cam/app/src/features/events/create/Footer';
import { CreateEventForm } from '@cam/app/src/features/events/create/form';
import { EventFormProps } from '@cam/app/src/features/events/create/form/types';
import { CreateEventTitle } from '@cam/app/src/features/events/create/Title';
import { initialWallet } from '@cam/app/src/features/events/detail/components/constants';
import {
  getIsCustomersInitialized,
  getIsCustomersLoading,
} from '@cam/app/src/redux/Customers/selectors';
import {
  getIsEmployeesInitialized,
  getIsEmployeesLoading,
} from '@cam/app/src/redux/Employees/selectors';
import { createEvent, getIsCreating } from '@cam/app/src/redux/Events/eventList';
import { Spin } from '@cam/atomic';

export const CreateEvent: React.FC<{
  basePath?: string;
}> = ({ basePath = './' }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [form] = Form.useForm<EventFormProps>();
  const isEmployeesInitialized = useSelector(getIsEmployeesInitialized);
  const isCustomersInitialized = useSelector(getIsCustomersInitialized);
  const isEmployeesLoading = useSelector(getIsEmployeesLoading);
  const isCustomersLoading = useSelector(getIsCustomersLoading);
  const isLoading = isEmployeesLoading || isCustomersLoading;

  const isCreatingEvent = useSelector(getIsCreating);
  const handleCreateEvent = async () => {
    try {
      const values = await form.validateFields();
      // Constant value wallet
      dispatch(createEvent({ ...values, walletBefore: initialWallet, walletAfter: initialWallet }));
    } catch {}
  };

  return (
    <DirtProvider>
      <SidePanel
        onClose={() => navigate(basePath)}
        title={<CreateEventTitle />}
        footer={<Footer onClick={handleCreateEvent} isLoading={isCreatingEvent} />}
      >
        <Spin spinning={isLoading}>
          {isEmployeesInitialized && isCustomersInitialized && (
            <CreateEventForm name="createEventForm" form={form} />
          )}
        </Spin>
      </SidePanel>
    </DirtProvider>
  );
};
