import { TFunction } from 'i18next';

import { EventType } from '@cam/firebase/resource/Event';

export const getEventTypeTranslations = (t: TFunction): Record<EventType, string> => ({
  BIRTHDAY: t('events:eventType.birthday'),
  CHILL: t('events:eventType.chill'),
  CONCERT: t('events:eventType.concert'),
  FESTIVAL: t('events:eventType.festival'),
  OTHER: t('events:eventType.other'),
});
