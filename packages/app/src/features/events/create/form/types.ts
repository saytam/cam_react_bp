import { DateIoType } from '@cam/app/src/utils/date';
import { EventDetailResource } from '@cam/firebase/resource/Event';

export enum FORM_FIELDS {
  NAME = 'name',
  DATE = 'date',
  EVENT_TYPE = 'eventType',
  DESCRIPTION = 'description',
  HOSTS = 'hosts',
  PRODUCTION_MANAGER = 'productionManager',
  PERSONNEL = 'personnel',
  PAYMENT = 'payment',
  AGREEMENT = 'agreement',
  CUSTOMER = 'customer',
}

export type EventFormProps = Omit<EventDetailResource, 'eventId' | 'companyId' | 'date'> & {
  date: DateIoType;
};
