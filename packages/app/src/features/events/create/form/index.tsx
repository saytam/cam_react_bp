import { Space, Form, Select, Input, InputNumber } from 'antd';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { CustomerSelect } from '@cam/app/src/components/CustomerSelect';
import { EmployeeSelect } from '@cam/app/src/components/EmployeeSelect';
import { FormProps } from '@cam/app/src/components/Form';
import { useRules } from '@cam/app/src/components/Form/rules';
import { MAX_SMALL_INPUT_WIDTH } from '@cam/app/src/features/events/components/form/utils';
import { getEventTypeTranslations } from '@cam/app/src/features/events/create/form/translation';
import { FORM_FIELDS, EventFormProps } from '@cam/app/src/features/events/create/form/types';
import { getUserId } from '@cam/app/src/redux/Auth/selectors';
import { getSelectedCompany } from '@cam/app/src/redux/Company/selectors';
import { DateUtil } from '@cam/app/src/utils/date';
import { Card, DatePicker, Radio } from '@cam/atomic';
import { Agreement, EventType } from '@cam/firebase/resource/Event';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

export const CreateEventForm: React.FC<FormProps<EventFormProps>> = ({ form, name: formName }) => {
  const { t } = useTranslation();
  const rules = useRules();
  const companyInfo = useSelector(getSelectedCompany);
  const currentUserId = useSelector(getUserId);
  const eventTypeTranslationMap = getEventTypeTranslations(t);

  const initialValues: EventFormProps = {
    name: `New event - ${companyInfo?.companyName}`,
    eventType: EventType.OTHER,
    date: DateUtil.date(),
    productionManager: currentUserId,
    personnel: [],
    agreement: Agreement.WITHOUT,
  };

  return (
    <Form
      initialValues={initialValues}
      colon={false}
      form={form}
      {...layout}
      labelAlign="left"
      name={formName}
    >
      <Space direction="vertical" style={{ width: '100%' }}>
        <Card title={t('events:create.form.event.parts.essential')} type="inner">
          <Form.Item
            name={FORM_FIELDS.NAME}
            label={t('events:create.form.event.name')}
            rules={[rules.required]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={FORM_FIELDS.EVENT_TYPE}
            label={t('events:create.form.event.type')}
            rules={[rules.required]}
          >
            <Select filterOption showSearch optionFilterProp="label">
              {Object.keys(EventType)?.map(key => (
                <Select.Option value={key} key={key} label={eventTypeTranslationMap[key]}>
                  {eventTypeTranslationMap[key]}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={FORM_FIELDS.DATE}
            label={t('events:create.form.event.date')}
            rules={[{ ...rules.required, type: 'object' }]}
          >
            <DatePicker
              showTime
              showSecond={false}
              format="MMM D, YYYY HH:mm"
              style={{ width: MAX_SMALL_INPUT_WIDTH }}
            />
          </Form.Item>
          <Form.Item
            name={FORM_FIELDS.PRODUCTION_MANAGER}
            label={t('events:create.form.event.productionManager')}
            rules={[rules.required]}
          >
            <EmployeeSelect />
          </Form.Item>
        </Card>
        <Card title={t('events:create.form.event.parts.additional')} type="inner">
          <Form.Item name={FORM_FIELDS.PERSONNEL} label={t('events:create.form.event.personnel')}>
            <EmployeeSelect mode="multiple" />
          </Form.Item>
          <Form.Item
            name={FORM_FIELDS.DESCRIPTION}
            label={t('events:create.form.event.description')}
            rules={[rules.max(1000)]}
          >
            <Input.TextArea />
          </Form.Item>
          <Form.Item
            name={FORM_FIELDS.HOSTS}
            label={t('events:create.form.event.hosts')}
            rules={[{ type: 'number' }]}
          >
            <InputNumber min={0} step={5} type="number" style={{ width: MAX_SMALL_INPUT_WIDTH }} />
          </Form.Item>
        </Card>
        <Card title={t('events:create.form.event.parts.administrative')} type="inner">
          <Form.Item
            name={FORM_FIELDS.AGREEMENT}
            label={t('events:create.form.event.agreement.title')}
          >
            <Radio.Group name={FORM_FIELDS.AGREEMENT}>
              <Radio.Vertical value={Agreement.WITHOUT}>
                {t('events:create.form.event.agreement.without')}
              </Radio.Vertical>
              <Radio.Vertical value={Agreement.PENDING}>
                {t('events:create.form.event.agreement.pending')}
              </Radio.Vertical>
              <Radio.Vertical value={Agreement.SIGNED}>
                {t('events:create.form.event.agreement.signed')}
              </Radio.Vertical>
            </Radio.Group>
          </Form.Item>
          <Form.Item
            name={FORM_FIELDS.CUSTOMER}
            label={t('events:create.form.event.customer.title')}
            rules={[{ type: 'string' }]}
          >
            <CustomerSelect />
          </Form.Item>
        </Card>
      </Space>
    </Form>
  );
};
