import { useNavigate } from '@reach/router';
import { Space } from 'antd';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { ExternalSubmitButton } from '@cam/app/src/components/Form';
import { getHasVerifiedEmail } from '@cam/app/src/redux/Auth/selectors';
import { Button } from '@cam/atomic';

export const Footer: React.FC<{ isLoading: boolean; onClick: () => void }> = ({
  isLoading,
  onClick,
}) => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const isValidEmail = useSelector(getHasVerifiedEmail);

  const handleClose = () => navigate('./');

  return (
    <Space>
      <Button onClick={handleClose} disabled={isLoading}>
        {t('events:create.ctaCancel')}
      </Button>
      <ExternalSubmitButton
        onClick={onClick}
        disabled={!isValidEmail}
        tooltipTitle={isValidEmail ? '' : t('common:tooltip.notVerifiedEmail')}
        loading={isLoading}
      >
        {t('events:create.ctaCreate')}
      </ExternalSubmitButton>
    </Space>
  );
};
