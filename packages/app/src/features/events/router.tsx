import { Router, Redirect } from '@reach/router';

import { Route } from '@cam/app/src/components/Route';
import { Routes } from '@cam/app/src/constants/routes';
import { EventsDetailTabs } from '@cam/app/src/enums/events';
import { Events } from '@cam/app/src/features/events';
import { CreateEvent } from '@cam/app/src/features/events/create/CreateEvent';
import { EventDetail } from '@cam/app/src/features/events/detail/Detail';

const EventsRouter: React.FC = () => {
  return (
    <Router primary={false}>
      <Route path="/" component={<Events />}>
        <Route path={Routes.EVENTS.CREATE} component={<CreateEvent />} />
        <Route
          path={[Routes.EVENTS.DETAIL, Routes.EVENTS.TAB].join('/')}
          component={<EventDetail />}
        />
        <Redirect
          from={Routes.EVENTS.DETAIL}
          to={[Routes.EVENTS.BASE, Routes.EVENTS.DETAIL, EventsDetailTabs.OVERVIEW].join('/')}
          noThrow
        />
      </Route>
    </Router>
  );
};

export default EventsRouter;
