import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Body } from '@cam/app/src/components/Body';
import { EventsHeader } from '@cam/app/src/features/events/components/Header';
import { EventsTable } from '@cam/app/src/features/events/table/table';
import { getSelectedCompanyId } from '@cam/app/src/redux/Company/selectors';
import { fetchCustomers } from '@cam/app/src/redux/Customers';
import { getIsCustomersInitialized } from '@cam/app/src/redux/Customers/selectors';
import { fetchEmployees } from '@cam/app/src/redux/Employees';
import { getIsEmployeesInitialized } from '@cam/app/src/redux/Employees/selectors';
import {
  eventListSelectors,
  fetchEventList,
  fetchAdditionalEventList,
  getDateRange,
} from '@cam/app/src/redux/Events/eventList';
import { Loader } from '@cam/atomic';

const useEventsData = () => {
  const { isInitialized, isLoading, hasError } = useSelector(eventListSelectors.rawDataSelectors);
  const { data: events } = useSelector(eventListSelectors.getTableState);
  const hasFilter = useSelector(eventListSelectors.filterSelectors.isActive);
  const isEmpty = events.length === 0 && !hasError && !isLoading && !hasFilter;

  return { isInitialized, isLoading, isEmpty, events };
};

export const Events: React.FC = ({ children }) => {
  const dispatch = useDispatch();
  const eventsData = useEventsData();
  const companyId = useSelector(getSelectedCompanyId);
  const { isInitialized, isLoading, events } = eventsData;
  const dateRange = useSelector(getDateRange);

  const isEmployeesInitialized = useSelector(getIsEmployeesInitialized);
  const isCustomersInitialized = useSelector(getIsCustomersInitialized);

  useEffect(() => {
    !isEmployeesInitialized && dispatch(fetchEmployees());
    !isCustomersInitialized && dispatch(fetchCustomers());
  }, [dispatch, isEmployeesInitialized, isCustomersInitialized]);

  const fetchData = useCallback(() => {
    if (!companyId || !dateRange) return;
    dispatch(fetchEventList(companyId));
  }, [dispatch, companyId, dateRange]);

  const fetchAdditionalData = useCallback(() => {
    companyId && dispatch(fetchAdditionalEventList(companyId));
  }, [dispatch, companyId]);

  useEffect(fetchData, [fetchData]);

  if (!isInitialized) {
    return <Loader />;
  }

  return (
    <>
      <Body header={<EventsHeader />}>
        <EventsTable
          loading={isLoading}
          data={events}
          fetchData={fetchData}
          fetchAdditionalData={fetchAdditionalData}
        />
      </Body>
      {children}
    </>
  );
};
