import { TFunction } from 'i18next';
import { pipe } from 'lodash/fp';
import { useTranslation } from 'react-i18next';

import { CustomerRow } from '@cam/app/src/components/CustomerSelect';
import { searchFilterProps } from '@cam/app/src/components/Table/filters/SearchFilter';
import { Name } from '@cam/app/src/features/events/components/Name';
import { MenuComponent as MenuRenderer } from '@cam/app/src/features/events/table/renderer/Menu';
import { sortMap } from '@cam/app/src/features/events/table/util';
import { TypedColumnProps } from '@cam/app/src/utils/antd';
import { DateUtil, formatDateTime } from '@cam/app/src/utils/date';
import {
  applyFilter,
  applySorter,
  createActionsColumn,
  TableColumnsHook,
} from '@cam/app/src/utils/table';
import { Tag, Text } from '@cam/atomic';
import { EventList } from '@cam/firebase/resource/Event';

export const createColumns = (t: TFunction): TypedColumnProps<EventList>[] => [
  {
    title: t('events:eventsTable.event'),
    dataIndex: 'name',
    render: (_, record) => <Name name={record.name} eventId={record.eventId} />,
    ...searchFilterProps,
  },
  {
    title: t('events:eventsTable.date'),
    dataIndex: 'date',
    render: (_, record) => <Text>{formatDateTime(DateUtil.date(record.date))}</Text>,
  },
  {
    title: t('events:eventsTable.productionManager'),
    dataIndex: 'productionManager',
    render: (_, record) => <Tag variant="default">{record.productionManager.displayName}</Tag>,
    ...searchFilterProps,
  },
  {
    title: t('events:eventsTable.hosts'),
    dataIndex: 'hosts',
    render: (_, record) => <Text>{record.hosts}</Text>,
  },
  {
    title: t('events:eventsTable.customer'),
    dataIndex: 'customer',
    render: (_, record) => <CustomerRow customer={record.customer} />,
    ...searchFilterProps,
  },
  createActionsColumn((_, record) => <MenuRenderer id={record.eventId} />),
];

export const useTableColumns: TableColumnsHook<EventList> = (filter, sorter) => {
  const { t } = useTranslation();
  const columns = createColumns(t);
  return pipe(applySorter(sorter, sortMap), applyFilter(filter))(columns);
};
