import { Space } from 'antd';
import { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { LoadingError } from '@cam/app/src/components/LoadingError';
import { RangePicker } from '@cam/app/src/components/RangePicker';
import { DateRange } from '@cam/app/src/components/RangePicker/util';
import { useTableColumns } from '@cam/app/src/features/events/table/tableColumns';
import {
  eventListActions,
  eventListSelectors,
  firebasePaginationActions,
  getDateRange,
  getFirebasePagination,
  getIsFetchingAdditionalEventList,
  setDateRange,
} from '@cam/app/src/redux/Events/eventList';
import { useTable } from '@cam/app/src/utils/table';
import { Button, Table } from '@cam/atomic';
import { EventList } from '@cam/firebase/resource/Event';

export const EventsTable: React.FC<{
  loading: boolean;
  data: EventList[];
  fetchData: () => void;
  fetchAdditionalData?: () => void;
}> = ({ loading, data, fetchData, fetchAdditionalData }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { filter, sorter, pagination } = useSelector(eventListSelectors.getTableState);
  const hasFilter = useSelector(eventListSelectors.filterSelectors.isActive);
  const isFetchingAdditionalEventList = useSelector(getIsFetchingAdditionalEventList);
  const isPaginationOnLastPage = useSelector(getFirebasePagination)?.data?.lastPage;
  const dateRange = useSelector(getDateRange);

  const resetFirebasePagination = () => {
    dispatch(firebasePaginationActions.reset());
  };

  const handleFetchAdditionalEventList = () => {
    const { current, pageSize } = pagination;
    dispatch(eventListActions.paginationActions.setPagination({ current: current + 1, pageSize }));
    fetchAdditionalData?.();
  };

  const { onTableChange } = useTable({
    fetchData,
    tableActions: eventListActions,
    resetFirebasePagination,
  });

  const handleDateChange = useCallback(
    (range: DateRange) => {
      dispatch(setDateRange(range));
    },
    [dispatch]
  );

  return (
    <Space direction="vertical" size="middle">
      <Space>
        {t('events:rangePicker.title')}
        <RangePicker value={dateRange} onChange={handleDateChange} />
      </Space>
      <Table
        loading={loading}
        pagination={false}
        dataSource={data}
        columns={useTableColumns(filter, sorter)}
        locale={{
          emptyText: !hasFilter && !loading && (
            <LoadingError
              onReload={fetchData}
              text={t('events:eventsTable.loadingError')}
              bg="transparent"
            />
          ),
        }}
        rowKey={record => record.eventId}
        rowLabel={record => record.name}
        onChange={onTableChange}
      />
      {!isPaginationOnLastPage && !loading && (
        <Button
          variant="primary"
          loading={isFetchingAdditionalEventList}
          onClick={handleFetchAdditionalEventList}
        >
          {t('events:eventsTable.extra.fetchAdditionalEvents')}
        </Button>
      )}
    </Space>
  );
};
