import { Menu } from 'antd';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { AccessController, AccessScope, useRBAC } from '@cam/app/src/components/AccessController';
import { MoreButton } from '@cam/app/src/components/TableIconButtons';
import { fetchEventDetail } from '@cam/app/src/redux/Events/eventDetail';
import { showModal, ModalType } from '@cam/app/src/redux/Modals';
import { getA11yHandlers } from '@cam/app/src/utils/a11y';
import { Dropdown, Text } from '@cam/atomic';

interface Props {
  id: string;
}

export const MenuComponent: React.FC<Props> = ({ id }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { hasWriteAccess } = useRBAC(AccessScope.EVENT);

  const handleDelete = () => {
    dispatch(fetchEventDetail(id));
    dispatch(
      showModal({
        type: ModalType.DeleteEvent,
        props: {
          eventId: id,
        },
      })
    );
  };

  const menu = () => (
    <Menu>
      <Menu.Item disabled={!hasWriteAccess} key="delete" {...getA11yHandlers(handleDelete)}>
        <Text type="danger" disabled={!hasWriteAccess}>
          {t('events:table.menu.delete')}
        </Text>
      </Menu.Item>
    </Menu>
  );

  return (
    <AccessController scope={AccessScope.EVENT}>
      <Dropdown overlay={menu()} trigger={['click']}>
        <MoreButton />
      </Dropdown>
    </AccessController>
  );
};
