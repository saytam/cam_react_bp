import { Space } from 'antd';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { Routes } from '@cam/app/src/constants/routes';
import { getUserInfo } from '@cam/app/src/redux/Auth/selectors';
import { Button, Text, Title } from '@cam/atomic';

const Wrapper = styled.div`
  padding: 220px 0 20px 60px;

  @media only screen and (max-width: 768px) {
    padding-top: 60px;
  }
`;

export const EmptyEvents: React.FC = () => {
  const { t } = useTranslation(['events']);
  const { displayName } = useSelector(getUserInfo);

  return (
    <Wrapper>
      <Title level={1} size="xxl">
        {t('emptyEvents.title', { name: displayName })}
      </Title>
      <Space direction="vertical" size={64}>
        <Space direction="vertical" size="middle">
          <Text size="xl" type="secondary">
            {t('emptyEvents.subTitle')}
          </Text>

          <Button variant="primary" router={{ to: Routes.EVENTS.CREATE }}>
            {t('emptyEvents.cta')}
          </Button>
        </Space>
      </Space>
    </Wrapper>
  );
};
