import { useTranslation } from 'react-i18next';

import { LoadingError } from '@cam/app/src/components/LoadingError';
import { Loader } from '@cam/atomic';

export const ErrorOrLoader: React.FC<{
  isLoading: boolean;
  hasError: boolean;
  refetch: () => void;
}> = ({ isLoading, hasError, refetch, children }) => {
  const { t } = useTranslation();

  if (hasError) {
    return (
      <LoadingError
        text={t('common:notification.loadingError.description')}
        onReload={refetch}
        size="lg"
      />
    );
  }

  if (isLoading) return <Loader />;

  return <>{children}</>;
};
