import { NavLink } from '@cam/atomic';

export const Name: React.FC<{
  name: string;
  eventId: string;
  route?: string;
}> = ({ name, eventId, route }) => {
  return (
    <>
      <NavLink to={route || eventId} style={{ display: 'flex', alignItems: 'center' }}>
        {name}
      </NavLink>
    </>
  );
};
