import { FieldValidator } from 'formik';
import { TFunction } from 'i18next';
import { isEmpty, isInteger } from 'lodash';
import { useTranslation } from 'react-i18next';

export const useRequiredValidator = (skipCondition?: boolean): FieldValidator | undefined => {
  const { t } = useTranslation();
  if (skipCondition) {
    return undefined;
  }
  const errorMessage = t('common:validation.requiredField');
  return value => {
    if (isEmpty(value) && !isInteger(value)) {
      return errorMessage;
    }
  };
};

export const validateMaxLength = (t: TFunction, value: string, maxLength = 100) => {
  return value.length > maxLength ? t('common:validation.error.maxLength', { maxLength }) : '';
};
