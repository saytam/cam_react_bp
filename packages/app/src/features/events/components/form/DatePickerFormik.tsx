import { Dayjs } from 'dayjs';
import { FieldValidator } from 'formik';
import { get } from 'lodash';

import { FormItemWithBadge } from '@cam/app/src/features/events/components/form/FormItemWithBadge';
import { MAX_SMALL_INPUT_WIDTH } from '@cam/app/src/features/events/components/form/utils';
import { useFormikValues } from '@cam/app/src/features/events/utils';
import { DatePicker } from '@cam/atomic';

export const DatePickerFormik: React.FC<{
  name: string;
  validate?: FieldValidator;
  title?: string;
  help?: React.ReactNode;
}> = ({ name, title, validate, help, ...rest }) => {
  const { values, setFieldValue, errors } = useFormikValues();
  const inputValue = get(values, name);
  const inputError = get(errors, name, '');

  const handleChange = (fieldValue: Dayjs | null) => {
    const newDate = fieldValue?.toISOString();
    if (!newDate) {
      return;
    }
    setFieldValue(name, fieldValue);
  };

  return (
    <FormItemWithBadge
      name={name}
      validate={validate}
      validateStatus={!!inputError ? 'error' : undefined}
      help={!!inputError ? inputError : help}
      title={title}
    >
      <DatePicker
        showTime
        showSecond={false}
        format="MMM D, YYYY HH:mm"
        style={{ width: MAX_SMALL_INPUT_WIDTH }}
        value={inputValue}
        onChange={handleChange}
        {...rest}
      />
    </FormItemWithBadge>
  );
};
