import { Radio as FormikRadio } from 'formik-antd';
import { get } from 'lodash';
import { Fragment } from 'react';

import { BadgeComponent as Badge } from '@cam/app/src/features/events/components/form/Badge';
import { useFormikValues } from '@cam/app/src/features/events/utils';
import { Text, Radio } from '@cam/atomic';

interface OptionsProps {
  value: React.ReactText;
  label: React.ReactNode;
  description?: string;
  extra?: React.ReactNode;
}

export const RadioGroup: React.FC<{
  name: string;
  options: OptionsProps[];
  disabled?: boolean;
}> = ({ name, options, disabled }) => {
  const { values } = useFormikValues();
  const actualValue = get(values, name);

  return (
    <>
      <FormikRadio.Group name={name} disabled={disabled} aria-labelledby={name}>
        {options.map(({ value, label, description, extra }) => (
          <Fragment key={value}>
            <Radio.Vertical value={value}>
              {label} {value === actualValue && <Badge path={name} />}
              {description && (
                <div>
                  <Text type="secondary">{description}</Text>
                </div>
              )}
            </Radio.Vertical>
            {extra}
          </Fragment>
        ))}
      </FormikRadio.Group>
    </>
  );
};
