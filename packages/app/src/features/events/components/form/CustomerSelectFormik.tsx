import { FieldValidator } from 'formik';
import { get } from 'lodash';

import { CustomerSelect } from '@cam/app/src/components/CustomerSelect';
import { FormItemWithBadge } from '@cam/app/src/features/events/components/form/FormItemWithBadge';
import { useFormikValues } from '@cam/app/src/features/events/utils';

export const CustomerSelectFormik: React.FC<{
  name: string;
  validate?: FieldValidator;
  title?: string;
  help?: React.ReactNode;
}> = ({ name, title, validate, help, ...rest }) => {
  const { values, setFieldValue, errors } = useFormikValues();
  const inputValue = get(values, name, '');
  const inputError = get(errors, name, '');

  const handleChange = (fieldValue: string) => {
    setFieldValue(name, fieldValue);
  };

  return (
    <FormItemWithBadge
      name={name}
      validate={validate}
      validateStatus={!!inputError ? 'error' : undefined}
      help={!!inputError ? inputError : help}
      title={title}
    >
      <CustomerSelect onChange={handleChange} value={inputValue} {...rest} />
    </FormItemWithBadge>
  );
};
