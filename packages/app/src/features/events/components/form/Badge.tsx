import { useParams } from '@reach/router';
import { Badge, Tooltip, Space } from 'antd';
import { get } from 'lodash';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { isValueChanged, useFormikValues } from '@cam/app/src/features/events/utils';
import { eventDetailSelectors } from '@cam/app/src/redux/Events/selectors';
import { RootState } from '@cam/app/src/redux/reducer';
import { TextLink } from '@cam/atomic';

interface BadgeProps {
  readonly $visible: boolean;
}

const StyledBadge = styled(Badge)<BadgeProps>`
  .ant-badge-status-dot {
    visibility: ${props => (props.$visible ? 'visible' : 'hidden')};
    margin-left: 5px;
    margin-right: 5px;
    cursor: pointer;
  }
  .ant-badge-status-text {
    display: none;
  }
`;

export const BadgeComponent: React.FC<{ path: string; order?: number; discard?: boolean }> = ({
  path,
  order,
  discard = true,
}) => {
  const { t } = useTranslation();
  const { eventId } = useParams<{ eventId: string }>();
  const { values, initialValues, setFieldValue } = useFormikValues();
  const event = useSelector((state: RootState) => eventDetailSelectors.selectById(state, eventId));

  const originalValue = get(event, path);
  const hasOrder = !!order || order === 0;
  const isVisible = isValueChanged(initialValues, values, path, hasOrder);

  const valueToSet = hasOrder && originalValue ? { ...originalValue, order } : originalValue;

  const content = discard && (
    <TextLink onClick={() => setFieldValue(path, valueToSet)}>
      {t('common:value.discard.message')}
    </TextLink>
  );

  return (
    <Tooltip
      title={
        isVisible && (
          <Space>
            {t('common:value.changed.message')}
            {content}
          </Space>
        )
      }
    >
      <span>
        <StyledBadge status="warning" $visible={isVisible} />
      </span>
    </Tooltip>
  );
};
