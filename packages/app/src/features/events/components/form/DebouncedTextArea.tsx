import { default as Input, TextAreaProps } from 'antd/es/input';
import { FieldValidator } from 'formik';

import { useInputFormValues } from '@cam/app/src/features/events/components/form/DebouncedInput';
import { FormItemWithBadge } from '@cam/app/src/features/events/components/form/FormItemWithBadge';
import { MAX_INPUT_WIDTH } from '@cam/app/src/features/events/components/form/utils';

const inputStyle = { width: '100%', maxWidth: MAX_INPUT_WIDTH };

export const DebouncedTextArea: React.FC<
  {
    name: string;
    validate?: FieldValidator;
    title?: string;
    help?: React.ReactNode;
  } & TextAreaProps
> = ({ name, validate, title, help, ...rest }) => {
  const { value, setValue, inputError, debouncedSave } = useInputFormValues(name);

  const handleChange = (fieldValue: string | number) => {
    setValue(fieldValue);
    debouncedSave(fieldValue);
  };

  return (
    <FormItemWithBadge
      name={name}
      validate={validate}
      validateStatus={!!inputError ? 'error' : undefined}
      help={!!inputError ? inputError : help}
      title={title}
    >
      <Input.TextArea
        value={value}
        onChange={e => handleChange(e.target.value)}
        style={inputStyle}
        id={name}
        {...rest}
      />
    </FormItemWithBadge>
  );
};
