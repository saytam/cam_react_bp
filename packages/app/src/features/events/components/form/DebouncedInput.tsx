import { default as Input, InputProps } from 'antd/es/input';
import { default as InputNumber, InputNumberProps } from 'antd/es/input-number';
import { FieldValidator } from 'formik';
import { debounce, get } from 'lodash';
import { useState, useEffect, useCallback } from 'react';

import { FormItemWithBadge } from '@cam/app/src/features/events/components/form/FormItemWithBadge';
import {
  MAX_INPUT_WIDTH,
  MAX_SMALL_INPUT_WIDTH,
} from '@cam/app/src/features/events/components/form/utils';
import { useFormikValues } from '@cam/app/src/features/events/utils';

const inputStyle = { width: '100%', maxWidth: MAX_INPUT_WIDTH };

export const useInputFormValues = (name: string) => {
  const { values, setFieldValue, errors } = useFormikValues();
  const inputValue = get(values, name, '');
  const inputError = get(errors, name, '');
  const [value, setValue] = useState(inputValue);

  useEffect(() => {
    setValue(inputValue);
  }, [setValue, inputValue]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const debouncedSave = useCallback(
    debounce((nextValue: string | number) => setFieldValue(name, nextValue), 200),
    []
  );

  return {
    value,
    setValue,
    inputValue,
    inputError,
    debouncedSave,
  };
};

export const DebouncedInput: React.FC<
  {
    name: string;
    validate?: FieldValidator;
    title?: string;
    help?: React.ReactNode;
    required?: boolean;
  } & InputProps &
    InputNumberProps
> = ({ name, type, validate, title, help, required, ...rest }) => {
  const InputComponent = type === 'password' ? Input.Password : Input;
  const { value, setValue, inputError, debouncedSave } = useInputFormValues(name);

  const handleChange = (fieldValue: string | number) => {
    setValue(fieldValue);
    debouncedSave(fieldValue);
  };

  return (
    <FormItemWithBadge
      name={name}
      validate={validate}
      validateStatus={!!inputError ? 'error' : undefined}
      help={!!inputError ? inputError : help}
      title={title}
      required={required}
    >
      {type === 'number' ? (
        <InputNumber
          value={value}
          onChange={handleChange}
          style={{ ...inputStyle, width: MAX_SMALL_INPUT_WIDTH }}
          id={name}
          {...rest}
        />
      ) : (
        <InputComponent
          value={value}
          onChange={({ target }) => handleChange(target.value)}
          type={type}
          style={inputStyle}
          id={name}
          {...rest}
        />
      )}
    </FormItemWithBadge>
  );
};
