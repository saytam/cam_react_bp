import { Space } from 'antd';
import { SpaceSize } from 'antd/es/space';

import { Card, HelpText } from '@cam/atomic';
import { Colors } from '@cam/theme/types';

export const OverviewSection: React.FC<{
  title: string;
  size?: SpaceSize | [SpaceSize, SpaceSize];
  titleColor?: Colors;
  hint?: string;
}> = ({ title, size = 'small', titleColor, hint, children }) => {
  return (
    <Card
      title={hint ? <HelpText help={hint}>{title}</HelpText> : title}
      type="inner"
      titleColor={titleColor}
    >
      <Space direction="vertical" size={size}>
        {children}
      </Space>
    </Card>
  );
};
