import { SelectProps } from 'antd';
import { FieldValidator } from 'formik';
import { get } from 'lodash';

import { EmployeeSelect } from '@cam/app/src/components/EmployeeSelect';
import { FormItemWithBadge } from '@cam/app/src/features/events/components/form/FormItemWithBadge';
import { useFormikValues } from '@cam/app/src/features/events/utils';

export const EmployeeSelectFormik: React.FC<
  {
    name: string;
    validate?: FieldValidator;
    title?: string;
    help?: React.ReactNode;
    required?: boolean;
  } & SelectProps
> = ({ name, title, validate, help, required, ...rest }) => {
  const { values, setFieldValue, errors } = useFormikValues();
  const inputValue = get(values, name, '');
  const inputError = get(errors, name, '');

  const handleChange = (fieldValue: string | string[]) => {
    setFieldValue(name, fieldValue);
  };

  return (
    <FormItemWithBadge
      name={name}
      validate={validate}
      validateStatus={!!inputError ? 'error' : undefined}
      help={!!inputError ? inputError : help}
      title={title}
      required={required}
    >
      <EmployeeSelect {...rest} onChange={handleChange} value={inputValue} />
    </FormItemWithBadge>
  );
};
