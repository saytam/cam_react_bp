import { FieldValidator } from 'formik';
import { get } from 'lodash';

import { Payment } from '@cam/app/src/components/Payment';
import { FormItemWithBadge } from '@cam/app/src/features/events/components/form/FormItemWithBadge';
import { useFormikValues } from '@cam/app/src/features/events/utils';
import { Payment as PaymentType } from '@cam/firebase/resource/Payment';

export const PaymentFormik: React.FC<{
  name: string;
  validate?: FieldValidator;
  title?: string;
  help?: React.ReactNode;
}> = ({ name, title, validate, help, ...rest }) => {
  const { values, setFieldValue, errors } = useFormikValues();
  const inputValue = get(values, name);
  const inputError = get(errors, name, '');

  const handleChange = (fieldValue: PaymentType) => {
    setFieldValue(name, fieldValue);
  };

  return (
    <FormItemWithBadge
      name={name}
      validate={validate}
      validateStatus={!!inputError ? 'error' : undefined}
      help={!!inputError ? inputError : help}
      title={title}
    >
      <Payment onChange={handleChange} value={inputValue} {...rest} />
    </FormItemWithBadge>
  );
};
