import { Form } from 'formik-antd';
import { FormItemProps } from 'formik-antd/es/form-item';

export const FormItem: React.FC<FormItemProps & { title?: React.ReactNode }> = ({
  title,
  children,
  name,
  ...rest
}) => (
  <Form.Item
    label={title}
    style={{ margin: 0 }}
    labelAlign="left"
    htmlFor={name}
    name={name}
    labelCol={{ xs: 6 }}
    {...rest}
    hasFeedback={false}
  >
    <div style={{ display: 'flex', alignItems: 'center' }}>{children}</div>
  </Form.Item>
);
