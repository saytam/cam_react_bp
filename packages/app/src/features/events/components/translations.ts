import { TFunction } from 'i18next';
import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { TranslationHook } from '@cam/app/src/components/Table/Header/Left';
import { StringKeys } from '@cam/app/src/utils/typescript';
import { EventList } from '@cam/firebase/resource/Event';

export const getColumnTranslations = (t: TFunction): Record<StringKeys<EventList>, string> => ({
  eventId: '',
  customer: t('events:eventsTable.customer'),
  date: t('events:eventsTable.date'),
  hosts: t('events:eventsTable.hosts'),
  name: t('events:eventsTable.event'),
  productionManager: t('events:eventsTable.productionManager'),
  eventType: t('events:eventsTable.eventType'),
  visibleColumns: '',
});

export const useColumnsTranslations = () => {
  const { t } = useTranslation();
  return useMemo(() => getColumnTranslations(t), [t]);
};

export const useFilterTranslations: TranslationHook<StringKeys<EventList>> = () => {
  const columnTranslations = useColumnsTranslations();

  return [
    column => columnTranslations[column] || column,
    (_, filter) => {
      return `"${filter}"`;
    },
  ];
};
