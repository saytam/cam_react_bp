import { PlusOutlined } from '@ant-design/icons';
import { Row, Space } from 'antd';
import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { AccessController } from '@cam/app/src/components/AccessController';
import { PageTitle } from '@cam/app/src/components/PageTitle';
import { TableActionButtons, TableHeader } from '@cam/app/src/components/Table';
import { LeftHeader } from '@cam/app/src/components/Table/Header/Left';
import { Routes } from '@cam/app/src/constants/routes';
import { useFilterTranslations } from '@cam/app/src/features/events/components/translations';
import { getSelectedCompanyId } from '@cam/app/src/redux/Company/selectors';
import {
  eventListActions,
  eventListSelectors,
  fetchEventList,
  firebasePaginationActions,
} from '@cam/app/src/redux/Events/eventList';
import { Button } from '@cam/atomic';
import { Scope } from '@cam/firebase/resource/Company';

const useHeaderData = () => {
  const dispatch = useDispatch();
  const companyId = useSelector(getSelectedCompanyId);

  const reloadEvents = () => {
    dispatch(firebasePaginationActions.reset());
    dispatch(fetchEventList(companyId));
  };

  return {
    reloadEvents,
  };
};

export const EventsHeader: React.FC = memo(() => {
  const { t } = useTranslation(['events']);

  const headerData = useHeaderData();
  const { reloadEvents } = headerData;

  return (
    <Space direction="vertical" size="small" style={{ width: '100%' }}>
      <Row align="middle" justify="space-between">
        <PageTitle>{t('common:events.title')}</PageTitle>

        <Space size="middle">
          <AccessController scope={Scope.EVENT}>
            <Button
              size="small"
              icon={<PlusOutlined />}
              router={{ to: [Routes.EVENTS.BASE, Routes.EVENTS.CREATE].join('/') }}
            >
              {t('createEvent.create')}
            </Button>
          </AccessController>
        </Space>
      </Row>

      <TableHeader
        filters={<CustomersFilters fetchData={reloadEvents} />}
        actions={<CustomersActions fetchData={reloadEvents} />}
      />
    </Space>
  );
});

const CustomersFilters: React.FC<{ fetchData: () => void }> = ({ fetchData }) => {
  return (
    <LeftHeader
      fetchData={fetchData}
      useFilterTranslations={useFilterTranslations}
      {...eventListActions}
      {...eventListSelectors}
    />
  );
};

const CustomersActions: React.FC<{ fetchData: () => void }> = ({ fetchData }) => {
  return <TableActionButtons fetchData={fetchData} />;
};
