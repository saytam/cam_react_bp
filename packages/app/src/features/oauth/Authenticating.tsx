import { useTranslation } from 'react-i18next';

import { LoadingScreen } from '@cam/app/src/components/LoadingScreen';

export const Authenticating = () => {
  const { t } = useTranslation();
  return <LoadingScreen title={t('common:login.authenticating')} isLoading />;
};
