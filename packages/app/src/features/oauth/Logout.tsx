import { useTranslation } from 'react-i18next';

import { LoadingScreen } from '@cam/app/src/components/LoadingScreen';

export const Logout = () => {
  const { t } = useTranslation();
  return <LoadingScreen title={t('common:login.loggingOut')} isLoading />;
};
