import { Authenticating } from '@cam/app/src/features/oauth/Authenticating';
import { Secure } from '@cam/app/src/features/oauth/Secure';

export const SecuredApp: React.FC = ({ children }) => {
  return <Secure authenticating={Authenticating}>{children}</Secure>;
};
