import { Router } from '@reach/router';

import { Route } from '@cam/app/src/components/Route';
import { Routes } from '@cam/app/src/constants/routes';
import { CashRegister } from '@cam/app/src/features/cashRegister';
import { ManageCategories } from '@cam/app/src/features/cashRegister/categories/ManageCategories';

const CashRegisterRouter: React.FC = () => {
  return (
    <Router primary={false}>
      <Route path="/" component={<CashRegister />}>
        <Route path={Routes.CASH_REGISTER.CREATE} component={<div>HAHAHA</div>} />
        <Route path={Routes.CASH_REGISTER.CATEGORIES} component={<ManageCategories />} />
        <Route path={Routes.CASH_REGISTER.DETAIL} component={<div>SHASHAHSD</div>} />
      </Route>
    </Router>
  );
};

export default CashRegisterRouter;
