import { DatabaseOutlined } from '@ant-design/icons';
import { Space } from 'antd';
import { useTranslation } from 'react-i18next';

import { Icon } from '@cam/atomic';

export const ManageCategoriesTitle: React.FC = () => {
  const { t } = useTranslation();

  return (
    <Space>
      <Icon icon={DatabaseOutlined} />
      {t('cashRegister:categories.title')}
    </Space>
  );
};
