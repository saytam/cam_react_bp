import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { ExternalSubmitButton } from '@cam/app/src/components/Form';
import { getIsCreating } from '@cam/app/src/redux/CashRegister/categories';

const StyledFooter = styled.div`
  align-items: center;
  border-top: 1px solid ${props => props.theme.colors.neutral.light.border};
  display: flex;
  justify-content: space-between;
  height: 55px;
  margin-bottom: -18px;
  margin-left: -24px;
  padding: 0 25px 0 15px;
  width: calc(100% + 48px);

  *:only-child {
    margin-left: auto;
  }
`;

export const CategoryFooter: React.FC<{
  onSubmit?: () => Promise<void>;
}> = ({ onSubmit }) => {
  const { t } = useTranslation();
  const isCreating = useSelector(getIsCreating);

  return (
    <StyledFooter>
      <div />

      {onSubmit && (
        <ExternalSubmitButton onClick={onSubmit} loading={isCreating}>
          {t('company:cta.update')}
        </ExternalSubmitButton>
      )}
    </StyledFooter>
  );
};
