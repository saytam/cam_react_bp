import { useNavigate } from '@reach/router';
import { Form, Space } from 'antd';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { AccessScope, useRBAC } from '@cam/app/src/components/AccessController';
import { DirtProvider } from '@cam/app/src/components/Form';
import { SidePanel } from '@cam/app/src/components/SidePanel';
import { Category } from '@cam/app/src/features/cashRegister/categories/Category';
import { CategoryFooter } from '@cam/app/src/features/cashRegister/categories/Footer';
import { CategoryForm } from '@cam/app/src/features/cashRegister/categories/form';
import { CategoryFormProps } from '@cam/app/src/features/cashRegister/categories/form/types';
import { ManageCategoriesTitle } from '@cam/app/src/features/cashRegister/categories/Title';
import {
  createCategory,
  fetchCategories,
  getCategories,
  getIsLoading,
} from '@cam/app/src/redux/CashRegister/categories';
import { Card, Spin } from '@cam/atomic';

export const ManageCategories: React.FC<{
  basePath?: string;
}> = ({ basePath = './' }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const [form] = Form.useForm<CategoryFormProps>();
  const { hasWriteAccess: canModifyCashRegister } = useRBAC(AccessScope.CASH_REGISTER);

  const isLoading = useSelector(getIsLoading);
  const categories = useSelector(getCategories);

  useEffect(() => {
    dispatch(fetchCategories());
  }, [dispatch]);

  const handleCreateCategory = async () => {
    try {
      const values = await form.validateFields();
      dispatch(createCategory(values));
    } catch {}
  };

  return (
    <SidePanel onClose={() => navigate(basePath)} title={<ManageCategoriesTitle />}>
      <Spin spinning={isLoading}>
        <Space direction="vertical">
          {categories.map(category => (
            <div key={category.key}>
              <Category category={category} />
            </div>
          ))}
          <Card title={t('cashRegister:categories.form.title')} type="inner">
            <DirtProvider>
              <CategoryForm name="createCategoryForm" form={form} />
              {canModifyCashRegister && <CategoryFooter onSubmit={handleCreateCategory} />}
            </DirtProvider>
          </Card>
        </Space>
      </Spin>
    </SidePanel>
  );
};
