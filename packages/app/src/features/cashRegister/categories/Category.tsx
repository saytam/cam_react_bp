import { Row, Tooltip } from 'antd';
import { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';

import { useRBAC, AccessScope } from '@cam/app/src/components/AccessController';
import { showModal, ModalType } from '@cam/app/src/redux/Modals';
import { Button, Card, Text } from '@cam/atomic';
import { Category as CategoryType, DefaultCategories } from '@cam/firebase/resource/CashRegister';

export const Category: React.FC<{ category: CategoryType }> = ({ category }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { hasWriteAccess: canModifyCashRegister } = useRBAC(AccessScope.CASH_REGISTER);
  const notDefault = !Object.keys(DefaultCategories).includes(category.key);

  const handleDelete = useCallback(() => {
    dispatch(
      showModal({
        type: ModalType.DeleteCategory,
        props: {
          id: category.key,
          name: category.name,
        },
      })
    );
  }, [dispatch, category]);

  return (
    <Card>
      <Row justify="space-between" align="middle">
        <Text>{`${category.key} - ${category.name}`}</Text>
        {canModifyCashRegister && (
          <Tooltip title={notDefault ? '' : t('cashRegister:category.deleteDefault.tooltip')}>
            <Button disabled={!notDefault} onClick={handleDelete} variant="text" danger>
              {t('cashRegister:category.delete')}
            </Button>
          </Tooltip>
        )}
      </Row>
    </Card>
  );
};
