import { Category } from '@cam/firebase/resource/CashRegister';

export enum FORM_FIELDS {
  KEY = 'key',
  NAME = 'name',
}

export type CategoryFormProps = Category;
