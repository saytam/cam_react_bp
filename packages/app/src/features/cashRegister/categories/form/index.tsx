import { Form, Input } from 'antd';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { FormProps } from '@cam/app/src/components/Form';
import { useRules } from '@cam/app/src/components/Form/rules';
import {
  CategoryFormProps,
  FORM_FIELDS,
} from '@cam/app/src/features/cashRegister/categories/form/types';
import { getCategoriesKeys } from '@cam/app/src/redux/CashRegister/categories';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

export const CategoryForm: React.FC<FormProps<CategoryFormProps>> = ({ form, name: formName }) => {
  const { t } = useTranslation();
  const rules = useRules();
  const categoriesKeys = useSelector(getCategoriesKeys);

  useEffect(() => {
    form.resetFields();
  }, [form]);

  return (
    <Form colon={false} form={form} {...layout} labelAlign="left" name={formName}>
      <Form.Item
        name={FORM_FIELDS.KEY}
        label={t('cashRegister:categories.form.category.key')}
        rules={[
          rules.required,
          rules.max(10),
          {
            pattern: /[A-Z0-9]/,
            message: t('common:validation.key'),
          },
          {
            validator: (_, value: string) => {
              const isValid = !categoriesKeys.includes(value);
              if (!isValid) {
                return Promise.reject(t('cashRegister:validation.key.taken', { key: value }));
              }
              return Promise.resolve();
            },
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name={FORM_FIELDS.NAME}
        label={t('cashRegister:categories.form.category.name')}
        rules={[rules.required, rules.max(20)]}
      >
        <Input />
      </Form.Item>
    </Form>
  );
};
