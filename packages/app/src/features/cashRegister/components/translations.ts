import { TFunction } from 'i18next';
import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { TranslationHook } from '@cam/app/src/components/Table/Header/Left';
import { StringKeys } from '@cam/app/src/utils/typescript';
import { CashRegisterList } from '@cam/firebase/resource/CashRegister';

export const getColumnTranslations = (
  t: TFunction
): Record<StringKeys<CashRegisterList>, string> => ({
  amount: t('cashRegister:cashRegisterTable.amount'),
  amountWithoutDph: t('cashRegister:cashRegisterTable.amountWithoutDph'),
  cashRegisterBalance: t('cashRegister:cashRegisterTable.cashRegisterBalance'),
  cashRegisterCorrection: t('cashRegister:cashRegisterTable.cashRegisterCorrection'),
  cashRegisterRecordId: '',
  categoryKey: t('cashRegister:cashRegisterTable.categoryKey'),
  date: t('cashRegister:cashRegisterTable.date'),
  description: t('cashRegister:cashRegisterTable.description'),
  dph: t('cashRegister:cashRegisterTable.dph'),
  visibleColumns: '',
});

export const useColumnsTranslations = () => {
  const { t } = useTranslation();
  return useMemo(() => getColumnTranslations(t), [t]);
};

export const useFilterTranslations: TranslationHook<StringKeys<CashRegisterList>> = () => {
  const columnTranslations = useColumnsTranslations();

  return [
    column => columnTranslations[column] || column,
    (_, filter) => {
      return `"${filter}"`;
    },
  ];
};
