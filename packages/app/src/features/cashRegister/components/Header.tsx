import { DatabaseOutlined, PlusOutlined } from '@ant-design/icons';
import { Row, Space } from 'antd';
import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { AccessController } from '@cam/app/src/components/AccessController';
import { PageTitle } from '@cam/app/src/components/PageTitle';
import { TableActionButtons, TableHeader } from '@cam/app/src/components/Table';
import { LeftHeader } from '@cam/app/src/components/Table/Header/Left';
import { Routes } from '@cam/app/src/constants/routes';
import { useFilterTranslations } from '@cam/app/src/features/cashRegister/components/translations';
import {
  cashRegisterActions,
  cashRegisterListSelectors,
  fetchCashRegister,
  firebasePaginationActions,
} from '@cam/app/src/redux/CashRegister/cashRegister';
import { getSelectedCompanyId } from '@cam/app/src/redux/Company/selectors';
import { Button } from '@cam/atomic';
import { Scope } from '@cam/firebase/resource/Company';

const useHeaderData = () => {
  const dispatch = useDispatch();
  const companyId = useSelector(getSelectedCompanyId);

  const reloadCashRegister = () => {
    dispatch(firebasePaginationActions.reset());
    dispatch(fetchCashRegister(companyId));
  };

  return {
    reloadCashRegister,
  };
};

export const CashRegisterHeader: React.FC = memo(() => {
  const { t } = useTranslation(['cashRegister']);

  const headerData = useHeaderData();
  const { reloadCashRegister } = headerData;

  return (
    <Space direction="vertical" size="small" style={{ width: '100%' }}>
      <Row align="middle" justify="space-between">
        <PageTitle>{t('common:cashRegister.title')}</PageTitle>

        <Space size="middle">
          <AccessController scope={Scope.CASH_REGISTER}>
            <Button
              size="small"
              icon={<DatabaseOutlined />}
              router={{
                to: [Routes.CASH_REGISTER.BASE, Routes.CASH_REGISTER.CATEGORIES].join('/'),
              }}
            >
              {t('categories.title')}
            </Button>
          </AccessController>
          <AccessController scope={Scope.CASH_REGISTER}>
            <Button
              size="small"
              icon={<PlusOutlined />}
              router={{ to: [Routes.CASH_REGISTER.BASE, Routes.CASH_REGISTER.CREATE].join('/') }}
            >
              {t('createCashRegisterRecord.create')}
            </Button>
          </AccessController>
        </Space>
      </Row>

      <TableHeader
        filters={<CustomersFilters fetchData={reloadCashRegister} />}
        actions={<CustomersActions fetchData={reloadCashRegister} />}
      />
    </Space>
  );
});

const CustomersFilters: React.FC<{ fetchData: () => void }> = ({ fetchData }) => {
  return (
    <LeftHeader
      fetchData={fetchData}
      useFilterTranslations={useFilterTranslations}
      {...cashRegisterActions}
      {...cashRegisterListSelectors}
    />
  );
};

const CustomersActions: React.FC<{ fetchData: () => void }> = ({ fetchData }) => {
  return <TableActionButtons fetchData={fetchData} />;
};
