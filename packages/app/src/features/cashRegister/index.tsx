import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Body } from '@cam/app/src/components/Body';
import { CashRegisterHeader } from '@cam/app/src/features/cashRegister/components/Header';
import { CashRegisterRecordsTable } from '@cam/app/src/features/cashRegister/table/table';
import {
  cashRegisterListSelectors,
  fetchAdditionalCashRegisterList,
  fetchCashRegister,
  getDateRange,
} from '@cam/app/src/redux/CashRegister/cashRegister';
import { getSelectedCompanyId } from '@cam/app/src/redux/Company/selectors';
import { Loader } from '@cam/atomic';

const useCashRegisterData = () => {
  const { isInitialized, isLoading, hasError } = useSelector(
    cashRegisterListSelectors.rawDataSelectors
  );
  const { data: cashRegisterRecords } = useSelector(cashRegisterListSelectors.getTableState);
  const hasFilter = useSelector(cashRegisterListSelectors.filterSelectors.isActive);
  const isEmpty = cashRegisterRecords.length === 0 && !hasError && !isLoading && !hasFilter;

  return { isInitialized, isLoading, isEmpty, cashRegisterRecords };
};

export const CashRegister: React.FC = ({ children }) => {
  const dispatch = useDispatch();
  const cashRegisterData = useCashRegisterData();
  const companyId = useSelector(getSelectedCompanyId);
  const { isInitialized, isLoading, cashRegisterRecords } = cashRegisterData;
  const dateRange = useSelector(getDateRange);

  const fetchData = useCallback(() => {
    if (!companyId || !dateRange) return;
    dispatch(fetchCashRegister(companyId));
  }, [dispatch, companyId, dateRange]);

  const fetchAdditionalData = useCallback(() => {
    companyId && dispatch(fetchAdditionalCashRegisterList(companyId));
  }, [dispatch, companyId]);

  useEffect(fetchData, [fetchData]);

  if (!isInitialized) {
    return <Loader />;
  }

  return (
    <>
      <Body header={<CashRegisterHeader />}>
        <CashRegisterRecordsTable
          loading={isLoading}
          data={cashRegisterRecords}
          fetchData={fetchData}
          fetchAdditionalData={fetchAdditionalData}
        />
      </Body>
      {children}
    </>
  );
};
