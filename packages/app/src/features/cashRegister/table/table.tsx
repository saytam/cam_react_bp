import { Space } from 'antd';
import { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { LoadingError } from '@cam/app/src/components/LoadingError';
import { RangePicker } from '@cam/app/src/components/RangePicker';
import { DateRange } from '@cam/app/src/components/RangePicker/util';
import { useTableColumns } from '@cam/app/src/features/cashRegister/table/tableColumns';
import {
  cashRegisterActions,
  cashRegisterListSelectors,
  firebasePaginationActions,
  getDateRange,
  getFirebasePagination,
  getIsFetchingAdditionalCashRegisterList,
  setDateRange,
} from '@cam/app/src/redux/CashRegister/cashRegister';
import { useTable } from '@cam/app/src/utils/table';
import { Button, Table } from '@cam/atomic';
import { CashRegisterList } from '@cam/firebase/resource/CashRegister';

export const CashRegisterRecordsTable: React.FC<{
  loading: boolean;
  data: CashRegisterList[];
  fetchData: () => void;
  fetchAdditionalData?: () => void;
}> = ({ loading, data, fetchData, fetchAdditionalData }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { filter, sorter, pagination } = useSelector(cashRegisterListSelectors.getTableState);
  const hasFilter = useSelector(cashRegisterListSelectors.filterSelectors.isActive);
  const isFetchingAdditionalCashRegisterList = useSelector(getIsFetchingAdditionalCashRegisterList);
  const isPaginationOnLastPage = useSelector(getFirebasePagination)?.data?.lastPage;
  const dateRange = useSelector(getDateRange);

  const resetFirebasePagination = () => {
    dispatch(firebasePaginationActions.reset());
  };

  const handleFetchAdditionalCashRegisterList = () => {
    const { current, pageSize } = pagination;
    dispatch(
      cashRegisterActions.paginationActions.setPagination({ current: current + 1, pageSize })
    );
    fetchAdditionalData?.();
  };

  const { onTableChange } = useTable({
    fetchData,
    tableActions: cashRegisterActions,
    resetFirebasePagination,
  });

  const handleDateChange = useCallback(
    (range: DateRange) => {
      dispatch(setDateRange(range));
    },
    [dispatch]
  );

  return (
    <Space direction="vertical" size="middle">
      <Space>
        {t('cashRegister:rangePicker.title')}
        <RangePicker value={dateRange} onChange={handleDateChange} past />
      </Space>
      <Table
        loading={loading}
        pagination={false}
        dataSource={data}
        columns={useTableColumns(filter, sorter)}
        locale={{
          emptyText: !hasFilter && !loading && (
            <LoadingError
              onReload={fetchData}
              text={t('cashRegister:cashRegisterTable.loadingError')}
              bg="transparent"
            />
          ),
        }}
        rowKey={record => record.cashRegisterRecordId}
        rowLabel={record => record.description || ''}
        onChange={onTableChange}
      />
      {!isPaginationOnLastPage && !loading && (
        <Button
          variant="primary"
          loading={isFetchingAdditionalCashRegisterList}
          onClick={handleFetchAdditionalCashRegisterList}
        >
          {t('cashRegister:cashRegisterTable.extra.fetchAdditionalCashRegisterRecords')}
        </Button>
      )}
    </Space>
  );
};
