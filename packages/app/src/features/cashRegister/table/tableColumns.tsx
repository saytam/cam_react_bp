import { TFunction } from 'i18next';
import { pipe } from 'lodash/fp';
import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { useColumnsTranslations } from '@cam/app/src/features/cashRegister/components/translations';
import { sortMap } from '@cam/app/src/features/cashRegister/table/util';
import { TypedColumnProps } from '@cam/app/src/utils/antd';
import { DateUtil, formatDateTime } from '@cam/app/src/utils/date';
import {
  applyFilter,
  applySorter,
  createActionsColumn,
  TableColumnsHook,
  translateTitle,
} from '@cam/app/src/utils/table';
import { Text } from '@cam/atomic';
import { CashRegisterList } from '@cam/firebase/resource/CashRegister';

export const createColumns = (t: TFunction): TypedColumnProps<CashRegisterList>[] => [
  {
    dataIndex: 'date',
    render: (_, record) => <Text>{formatDateTime(DateUtil.date(record.date))}</Text>,
  },
  {
    dataIndex: 'categoryKey',
    render: (_, record) => record.categoryKey,
  },
  {
    dataIndex: 'description',
    render: (_, record) => record.description,
  },
  {
    dataIndex: 'amount',
    render: (_, record) => record.amount.amount + ' ' + record.amount.currency,
  },
  {
    dataIndex: 'dph',
    render: (_, record) => record.dph.amount + ' ' + record.dph.currency,
  },
  {
    dataIndex: 'amountWithoutDph',
    render: (_, record) => record.amountWithoutDph.amount + ' ' + record.amountWithoutDph.currency,
  },
  {
    dataIndex: 'cashRegisterBalance',
    render: (_, record) =>
      record.cashRegisterBalance.amount + ' ' + record.cashRegisterBalance.currency,
  },
  {
    dataIndex: 'cashRegisterCorrection',
    render: (_, record) =>
      record.cashRegisterCorrection.amount + ' ' + record.cashRegisterCorrection.currency,
  },
  // createActionsColumn((_, record) => <MenuRenderer id={record.cashRegisterRecordId} />),
];

export const useTableColumns: TableColumnsHook<CashRegisterList> = (filter, sorter) => {
  const { t } = useTranslation();
  const columnsTranslations = useColumnsTranslations();
  return useMemo(() => {
    const columns = createColumns(t);
    const translatedColumns = translateTitle(columns, columnsTranslations);

    return pipe(applySorter(sorter, sortMap), applyFilter(filter))([...translatedColumns]);
  }, [t, filter, sorter, columnsTranslations]);
};
