export enum EventsDetailTabs {
  OVERVIEW = 'overview',
  BEFORE = 'before',
  MIDDLE = 'middle',
  AFTER = 'after',
  CHECKLIST = 'checklist',
}
