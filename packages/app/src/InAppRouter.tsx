import { Redirect, Router } from '@reach/router';
import { notification } from 'antd';
import { lazy, Suspense, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useTheme } from 'styled-components';

import 'chartjs-adapter-dayjs';
import 'chart.js/auto';
import { LoadingModal } from '@cam/app/src/components/LoadingModal';
import { Route } from '@cam/app/src/components/Route';
import { Routes } from '@cam/app/src/constants/routes';
import { Company } from '@cam/app/src/features/company';
import { DashboardRouter } from '@cam/app/src/features/dashboard/router';
import { EmployeeDetail } from '@cam/app/src/features/employees/detail/Employee';
import { Employees } from '@cam/app/src/features/employees/Employees';
import { EmployeeInvite } from '@cam/app/src/features/employees/Invite';
import { DefaultLayout as Layout } from '@cam/app/src/features/layout';
import { getModal } from '@cam/app/src/redux/Modals/selectors';

const Modals = lazy(
  () =>
    import(/* webpackChunkName: "modals", webpackPrefetch: true */ '@cam/app/src/features/modals')
);

const EventsRouter = lazy(
  () =>
    import(
      /* webpackChunkName: "events", webpackPrefetch: true */ '@cam/app/src/features/events/router'
    )
);

const CashRegisterRouter = lazy(
  () =>
    import(
      /* webpackChunkName: "cashRegister", webpackPrefetch: true */ '@cam/app/src/features/cashRegister/router'
    )
);

const useDefaults = () => {
  const theme = useTheme();
  useEffect(() => {
    notification.config({
      top: 64 + 24, //header + margin
    });
  }, []);
};

const InAppRouter: React.FC = () => {
  const modal = useSelector(getModal);
  // TODO @psenicka: role based access
  //   const { hasWriteAccess } = useRBAC(AccessScope.COMPANY);
  useDefaults();

  return (
    <>
      <Router>
        <Route path="/" component={<Layout />}>
          <Route path={[Routes.DASHBOARD.BASE, '*'].join('/')} component={<DashboardRouter />} />
          <Route path={Routes.COMPANY.BASE} component={<Company />} />
          <Route path={[Routes.EVENTS.BASE, '*'].join('/')} component={<EventsRouter />} />
          <Route
            path={[Routes.CASH_REGISTER.BASE, '*'].join('/')}
            component={<CashRegisterRouter />}
          />
          <Route path={Routes.EMPLOYEES.BASE} component={<Employees />}>
            <Route path={Routes.EMPLOYEES.INVITE} component={<EmployeeInvite />} />
            <Route path={Routes.EMPLOYEES.DETAIL} component={<EmployeeDetail />} />
          </Route>
          <Redirect from="*" to={Routes.DASHBOARD.BASE} noThrow />
        </Route>
      </Router>
      <Suspense fallback={<LoadingModal />}>{modal && <Modals />}</Suspense>
    </>
  );
};

// eslint-disable-next-line import/no-default-export
export default InAppRouter;
