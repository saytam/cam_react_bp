import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { Lang } from '@cam/app/src/locales';
import { changeDayjsLocale } from '@cam/app/src/locales/vendor';
import { getLocale } from '@cam/app/src/redux/Auth/selectors';

export const useChangeLocale = () => {
  const { i18n } = useTranslation();
  const locale = useSelector(getLocale);

  useEffect(() => {
    i18n.changeLanguage(locale);
    changeDayjsLocale(locale as Lang);
    document.documentElement.lang = locale;
  }, [locale, i18n]);
};
