# Club Action Manager

- monorepo using **yarn workspaces**
- prefix for all workspaces is `@cam`, the main app is in `@cam/app`
- always use **absolute imports** even when referencing the same project
- you can run commands inside workspaces from the root folder, e.g. `yarn workspace @cam/app build`
- common dependencies should be installed in the root folder, using `yarn add -W [package_name]`
- install dependencies from the root folder, just run `yarn` or `yarn install`

## Code formatting

- predefined rules to automatically format code during development (= no pointless style-related discussions in pull requests)
- **ESLint**, **Prettier** and pre-commit hooks using **husky**
- set up prettier in your IDE to load configuration from the local project
- format on save & before git commit

## Requirements

- node v16
- yarn v1

## Packages

All packages use **TypeScript** and can extend the base `tsconfig.json`.

### Main app [(README)](packages/app/README.md)

#### First run

```bash
yarn
yarn workspace @cam/app start
```

## Available scripts

### `yarn i18n`

Automatically add new translation keys and remove unused ones using [a babel plugin](https://i18next-extract.netlify.com).

### `yarn prettier`

Automatically format code in all files.

### `yarn lint`

Automatically apply eslint rules in all files.
